
# Some shell benchmarks.
# ======================

# % uname -a                                                                                                                              0
# Linux tyrant 5.15.19 #1 SMP PREEMPT Wed Feb 2 01:50:51 CST 2022 x86_64 AMD
# Ryzen 7 3700X 8-Core Processor AuthenticAMD GNU/Linux

# `shellbench'
# ~~~~~~~~~~~~

# % ./shellbench -c -s ksh,zsh,bash,dash -t 12 -w 2 sample/assign.sh
# sample/cmp.sh sample/count.sh sample/eval.sh sample/func.sh sample/null.sh
# sample/output.sh sample/subshell.sh
# --------------------------------------------------------------------------
# name                                  ksh        zsh       bash       dash
# --------------------------------------------------------------------------
# [null loop]                       722,235 >1,062,389    397,894>>1,687,238
# assign.sh: positional params   >2,560,459    773,832    484,970>>3,331,408
# assign.sh: variable             4,860,804 >6,435,936  1,260,997>>6,925,381
# assign.sh: local var                error >6,097,498  1,127,416>>7,599,489
# assign.sh: local var (typeset) >2,708,121>>6,293,963  1,122,436      error
# cmp.sh: [ ]                   >>3,444,418    442,328    322,624 >1,445,344
# cmp.sh: [[ ]]                >>10,127,965   >914,810    598,986      error
# cmp.sh: case                 >>13,588,813  1,119,219  1,021,745 >5,162,352
# count.sh: posix                 1,034,338 >1,831,763    605,182>>2,844,416
# count.sh: typeset -i           >1,110,569>>1,903,199    542,598      error
# count.sh: increment           >>5,734,219 >4,599,505    936,897      error
# eval.sh: direct assign           >893,103    241,663    258,644>>2,051,813
# eval.sh: eval assign             >449,333    182,692    136,933>>1,014,860
# eval.sh: command subs           >>211,802      3,009      3,093     >6,038
# func.sh: no func               >5,273,298  1,338,082    979,349>>6,555,817
# func.sh: func                  >1,064,868    240,224    296,974>>3,231,267
# null.sh: blank                      error>84,481,827      error      error
# null.sh: assign variable       >5,860,511  5,556,214  1,387,679>>6,746,367
# null.sh: define function       >6,072,567  4,315,432  1,215,669>>9,692,567
# null.sh: undefined variable    >2,429,587  1,190,234    772,987>>7,885,830
# null.sh: : command             >5,878,336  1,331,834  1,014,387>>7,660,968
# output.sh: echo               >>3,173,238    832,472    484,793 >2,592,521
# output.sh: printf                >995,433    818,483    471,272>>2,411,797
# output.sh: print              >>1,534,058   >835,295      error      error
# subshell.sh: no subshell      >>5,820,137  1,353,362    850,142 >5,195,124
# subshell.sh: brace            >>6,284,782    931,703    803,654 >5,823,043
# subshell.sh: subshell           >>530,095      3,299      5,351    >12,078
# subshell.sh: command subs       >>304,412      4,838      4,682    >11,505
# subshell.sh: external command      >5,775      1,907      3,258    >>6,312
# --------------------------------------------------------------------------
#                                        35         11          0         38
# * count: number of executions per second
# * To skip null loop measurement, set the environment variable below
# export SHELLBENCH_NULLLOOP_COUNT=ksh:722235,zsh:1062389,bash:397894,dash:1687238

# `sh-benchmark.zsh'
# ~~~~~~~~~~~~~~~~~~

# % ./sh-benchmarK.zsh
#  ksh   |   zsh  |  bash   | Test
#  100.0 | ->85.0 |  205.0  | Parameter Expansion 1: "$PARAMETER"
#  100.0 | ->71.4 |  810.0  | Parameter Expansion 2: $PARAMETER
#->100.0 |  151.6 |  224.0  | Parameter Expansion 3: "${PARAMETER##*/}" (modifier)
#->100.0 | 1135.0 | 2857.5  | Array Parameter Expansion 1: "${ARRAY[1]}" (one element)
#  100.0 | ->86.7 | 32935.1 | Array Parameter Expansion 2: "${ARRAY[@]}" (all elements)
#  100.0 | ->94.5 |  171.7  | Arithmetic Evaluation 1: let EXPRESSION
#  100.0 | ->77.5 |  245.0  | Arithmetic Evaluation 2: ((EXPRESSION))
#  100.0 | ->61.1 |  194.4  | Arithmetic Expansion 1: $((EXPRESSION))
#  100.0 | ->57.6 |  182.8  | Arithmetic Expansion 2: $(($PARAMETER+EXPRESSION))
#->100.0 |  103.8 |  291.0  | Test 1: [[ EXPRESSION ]]
#->100.0 |  314.7 |  411.0  | Test 2: [ EXPRESSION ]
#  100.0 | ->55.4 |  143.8  | Fork
#->100.0 |  429.5 | 54278.4 | Iterate Parameters 1; for
#->100.0 | 4130.0 |  380.0  | Iterate Parameters 2: while shift

# Bottom line.
# ~~~~~~~~~~~~

# Rule of thumb: use `dash' for piping programs without any data wrangling in
# between until script not reached 50 LOC, but if there is urge for extended
# functionality then use `ksh' instead. For more complicated tasks one should
# use Perl.


export PATH="${PATH}:${HOME}/.local/bin:"
export PATH="${PATH}:${HOME}/.local/bin/my_scripts:"
export PATH="${PATH}:${HOME}/.local/bin/my_scripts/wm:"
export PATH="${PATH}:${HOME}/.local/bin:"


_loadGoods() {
    . my_functions
    . my_aliases
}


_loadGoods


# Beware of that not all shells support escape characters in the same way.
export NORM_BLACK="\e[0;30m"
export NORM_RED="\e[0;31m"
export NORM_GREEN="\e[0;32m"
export NORM_BROWN="\e[0;33m"
export NORM_BLUE="\e[0;34m"
export NORM_PURPLE="\e[0;35m"
export NORM_CYAN="\e[0;36m"
export NORM_LGRAY="\e[0;37m"
export BOLD_BLACK="\e[1;30m"
export BOLD_RED="\e[1;31m"
export BOLD_GREEN="\e[1;32m"
export BOLD_BROWN="\e[1;33m"    # Yellow.
export BOLD_BLUE="\e[1;34m"
export BOLD_PURPLE="\e[1;35m"   # Magenta.
export BOLD_CYAN="\e[1;36m"
export BOLD_LGRAY="\e[1;37m"
export EXIT_COLOR="\e[0m"


# Requires `dircolors'. List of file type codes:
#    - Directory: di
#    - File: fi
#    - Symbolic Link: ln
#    - Named Pipe (FIFO): pi
#    - Socket: so
#    - Block Device: bd
#    - Character Device: cd
#    - Orphan Symbolic Link (points to a file that no longer exists): or
#    - Missing File (a missing file that an orphan symbolic link points to): mi
#    - Executable File (has the “x” permission): ex
#    - *.extension: Any file ending with an extension you specify. For example,
#    use *.txt for files ending in .txt, *.mp3 for files ending in .mp3,
#    *.desktop for files ending in .desktop, or anything else you like. You can
#    specify as many different file extensions as you like.
export LS_COLORS="*.hs=0;36:*.js=0;36:*.py=0;36:*.vim=0;33:*.txt=0;32:*.doc=0;32:*.pdf=0;32:${LS_COLORS}"


export PAGER="less"
export READER="zathura"
export BROWSER="lynx"
export EMACS_CLIENT="emacsclient -c -a 'emacs'"
export EDITOR="$(command -v nvim 2>/dev/null \
                    || command -v vim 2>/dev/null \
                    || command -v vi)"
if [ -n "${SSH_CLIENT}" ] || [ -n "${SSH_TTY}" ]; then
    VISUAL="${EDITOR}"
else
    export VISUAL="$(command -v ${EMACS_CLIENT} 2>/dev/null \
                    || command -v ${EDITOR} -)"
fi


#export GOPATH="/usr/share/gocode"
export GOPATH="${GOPATH}:${HOME}/go"
export PATH="${PATH}:${GOPATH}/bin:"


# Disable Bluetooth if it have not done in BIOS.
_checkDep bluetooth && bluetooth off
_checkDep bluetoothctl && bluetoothctl power off


/usr/bin/startx
#/usr/bin/startx 2>&1 | tee mylog   # Additionaly to /var/log/Xorg.n.log
