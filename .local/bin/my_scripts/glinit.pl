#!/usr/bin/env perl


#use v5.10;
use v5.34;
use utf8;
use warnings;
use diagnostics;    # One should better use `perl -Wdiagnostics' for debugging.
use strict;
use autodie;
use experimental qw(signatures); #use feature 'signatures';
no warnings "experimental::signatures";
#use Cpanel::JSON::XS;   # This package may be absent on your system by default.
use JSON::XS;
use Sys::Hostname;


# Setting defaults.
my $SCRIPT_NAME     = $0 =~ s/^\/*.*\///r;
my $SCRIPT_DIR      = $0 =~ s/\/${SCRIPT_NAME}$//r;
my $DEBUG;
my $VERBOSE;
my $LOGIN           = "git";
my $DOMAIN          = "gitlab.com";
my $NAME_SPACE      = "enkonto";
my $PROJECT_NAME    = "newPojectChangeThisName";
my $BRANCH_NAME     = "master";
my $ORIGIN          = "${LOGIN}\@${DOMAIN}:${NAME_SPACE}/${PROJECT_NAME}.git";
my $CURRENT_PATH    = `pwd`; chomp (${CURRENT_PATH});


sub HelpMsg {
    print STDERR << "	EOF";
	GitLabInit
	Options are:
	  -l:       Set login;
	  -d:       Set domain;
	  -n:       Set name space;
	  -p:       Set project name;
	  -b:       Set branch name.
	  -v:       Run in verbose mode;
	  -D[1-3]:  Run in debug mode;
	  -h:       Show this message;
	EOF
    exit;
};
#HelpMsg;


sub ErrorMsg {
    # One should use `strftime' if milliseconds are have to be shown.
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
    $year += 1900;
    ++$mon;
    print STDERR "[${year}-${mon}-${mday}T${hour}:${min}:${sec}+timezone],
        ${SCRIPT_NAME}:\n";

    foreach my $line ( @_ ) {
        print STDERR "\t$line\n";
    };
}
# ErrorMsg "error1" . "error2", "error3";


sub VerboseMsg( $verboseMessage ) {
    if ( ${VERBOSE} )   { say "$${verboseMessage}"; sleep 0.1 };
}


sub DebugMsg( $debugMessage ) {
    if ( ${DEBUG} )     { say "$${debugMessage}"; sleep 1 };
}


sub Init {
    VerboseMsg( \"Initiating tracker in a desired local folder." );
    DebugMsg( \"git init" );
    # `git init`;

    VerboseMsg( \"Adding all files to commit." );
    DebugMsg( \"git add -A" );
    # `git add -A`;

    VerboseMsg( \"Committing all." );
    DebugMsg( \"git commit -m \"Initial version\"" );
    # `git commit -m \"Initial version\"`;

    VerboseMsg( \"Adding an alias origin to master branch." );
    DebugMsg( \"git remote add origin ${ORIGIN}" );
    # `git remote add origin ${ORIGIN}`;

    VerboseMsg( \"Pushing change to remote repository the way supposed if your project uses the standard port 22." );
    DebugMsg( \"git push --set-upstream ${ORIGIN} ${BRANCH_NAME}" );
    # `git push --set-upstream ${ORIGIN} ${BRANCH_NAME}`;
}


sub Main {
    say "Ready to initiate new repository ${ORIGIN} at ${BRANCH_NAME} branch sourced from ${CURRENT_PATH}, [Yy]es/[Nn]o:";

    while ( <STDIN> ) {
        if      ( /^[Yy]$/ )    { Init; exit }
        elsif   ( /^[Nn]$/ )    { exit }
        else                    { say "Wrong answer." };
    };
}


# Processing incoming arguments.
while ( @ARGV ) {
    if ( $ARGV[0] =~ /^-/ ) {
        $_ = shift;
        last if /^--$/;
        if ( /^-h$/ )           { HelpMsg };                      # Will exit.
        if ( /^-D\s?([1-3])$/ ) { $DEBUG = $1; say "Debug level: $DEBUG"; next };
        if ( /^-v$/ )           { $VERBOSE++; say "Verbose mode is on."; next };
        if ( /^-l$/ )           { if ( defined($ARGV[0]) ) { $LOGIN        = $ARGV[0] }; next };
        if ( /^-d$/ )           { if ( defined($ARGV[0]) ) { $DOMAIN         = $ARGV[0] }; next };
        if ( /^-n$/ )           { if ( defined($ARGV[0]) ) { $NAME_SPACE   = $ARGV[0] }; next };
        if ( /^-p$/ )           { if ( defined($ARGV[0]) ) { $PROJECT_NAME = $ARGV[0] }; next };
        if ( /^-b$/ )           { if ( defined($ARGV[0]) ) { $BRANCH_NAME  = $ARGV[0] }; next };
        #...                    # other switches
        if ( /^-.*$/ ) { die "Wrong switch: '$_'. '-h' for help.\n"; }; # Last one.
    }
    else { $_ = shift; };
}


$ORIGIN = "${LOGIN}\@${DOMAIN}:${NAME_SPACE}/${PROJECT_NAME}.git";
Main;
