#!/usr/bin/env dash


# This script changes function's name according to the list.


funcList="$(cat <<- EOF
			_killemall _killEmAll
			_kill_zombies _killZombies
			_check_dep _checkDep
			_check_load _checkLoad
			_uberuser _uberUser
			_dynamic_menu _dynamicMenu
			_git_clean_branches _gitCleanBranches
			_make_rdm _makeRdm
			_spellme _spellMe
			_list_actions _listActions
			_check_systemd _checkSystemd
			_reboot_machine _rebootMachine
			_poweroff_machine _poweroffMachine
			apply_patch _applyPatch
			prepair_sources _prepairSources
			EOF
			)"

exeptionList="$(cat <<- EOF
			new_func.sh
			my_functions
			.bak
			.orig
			EOF
			)"


_skipVal() {
    printf "%s\n" "${exeptionList}" \
        | while read -r fileName; do
            printf "%s\n" "$@" \
            | grep \
                -q \
                -e "${fileName}" \
                && echo "True"
    done
}


_mkBackUp() {
    ! [ -f "$@.orig" ] \
        && {
            echo "Making backup of $@ to $@.orig."
            cp "$@" "$@.orig"
        }
}


printf "%s\n" "${funcList}" \
    | while read -r oldFunc newFunc; do
        echo "Substitute ${oldFunc} with ${newFunc}."

        for path in $(grep -rw . -e "${oldFunc}" | sed 's/:.*$//'); do


        skipFlag="$(_skipVal "${path}")"; skipFlag="${skipFlag:-False}"

        if [ "${skipFlag}" != "True" ]; then
            #echo "skipFlag=$skipFlag"

            # Suffix for `sed' works with issues. Workaround is below.
            # Beware of that BSD have different ``-i'' positional parameter
            # implementation.
            #sed -i.bak 's/'"${oldFunc}"'/'"${newFunc}"'/g' "${path}"
            _mkBackUp "${path}"

            echo "Modifying $path. Changing ${oldFunc} to ${newFunc}."
            sed -i 's/'"${oldFunc}"'/'"${newFunc}"'/g' "${path}"
        else
            #echo "skipFlag=$skipFlag"
            echo "Skipping $path."
        fi

        done
    done
