#!/usr/bin/env dash

# This script should be launched with root user previledges to be able to edit
# `asound.conf' and to restart alsa daemon.

# How should look like `asound.conf'.
# defaults.ctl.card 2;
# defaults.pcm.card 2;
# defaults.ctl.device 0;
# defaults.pcm.device 0;
#
# pcm.dsp {
#     type plug
#     slave.pcm "dmix"
# }

# TODO: Wrong switching to the next hardware.

_checkNonNum() {
    # Output "non number" if first argument isn't a number.
    echo $1 | awk -c '$0 ~/[^0-9]/ { print "non number" }'
}

_listPlayback() {
    # List of playback hardware devices.
    aplay -l | awk -c 'NR==1 || $1=="card" { print $0 }'
}

_helpMsg() {
    printf "%s" "Usage: asound.sh _no_args_" \
        " | list | [card number] [device number]"; printf "\n"
    printf "\t%s\n" "[card number] [device number] should be less then 100."
}

_switchPlayback() {
    # Define config file name and path to it
    # conf_file="/etc/asound.conf"
    conf_file="${HOME}/.local/bin/my_scripts/asound.conf"

    card_list="defaults.ctl.card defaults.pcm.card"
    device_list="defaults.ctl.device defaults.pcm.device"
    awk_source="${HOME}/.local/bin/my_scripts/asound.awk"

    aplay_pipe="/tmp/_switchPlayback_aplay.pipe"
    mkfifo $aplay_pipe
    aplay -l > $aplay_pipe &

    awk -c -v conf_file="$conf_file" -v new_card="$1" -v new_device="$2" \
        -v card_list="$card_list" -v device_list="$device_list" \
        -v aplay_pipe="$aplay_pipe" -f $awk_source $aplay_pipe $conf_file

    rm $aplay_pipe

    # Restart alsa daemon.
    #sv restart alsa
    doas /etc/rc.d/rc.alsa restart
}

case $# in
    2)
        # Check if correct arguements was given.
        if [ "$(_checkNonNum $1)" != "non number" ] && \
            [ $1 -lt 100 ] && \
            [ "$(_checkNonNum $2 )" != "non number" ] && \
            [ $2 -lt 100 ]; then
            _switchPlayback $1 $2
        else
            _helpMsg
        fi
        ;;
    1)
        if [ "$1"=="list" ]; then
            _listPlayback
        else
            _helpMsg
        fi
        ;;
    0)
        _switchPlayback
        ;;
    *)
        _helpMsg
        ;;
esac
