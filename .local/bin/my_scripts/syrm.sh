#!/usr/bin/env dash


# System Repository Manager
# =========================
# TODO: Rewrite in Perl.


_loadGoods() {
    . my_functions
    . my_aliases
}


_HelpMsg() {
    cat <<- EOF
		System Repository Manager.
		SyRM is mostly aimed to manage user's and system configurations. But don't have
		to. Treat your repositories as any other Git project using \`git' independently
		from working tree placement. Specify an alias for \`mgrm.sh' and use it with
		commands like \`push', \`add', \`commit', etc., specified after "-c" option:
		    alias _MyGitCommandAlias='syrm.sh -p [gitFolderPath] \\
		                                      -n [.uniqueGitFolderName] \\
		                                      -t [workingTreePath] \\
		                                      -o [git@gitlab.com:userName/repoName.git]'
		    _MyGitCommandAlias -c "commit -m 'message'"
		Mandatory options:
		  -p: Specify the git folder path where git folder supposed to be stored at.
		  -n: Specify the git folder name beginning with a dot.
		  -t: Specify the working tree path.
		  -o: Specify the repo origin for building or fetching,
		      i.e. git@gitlab.com:userName/repoName.git
		      It's supposed to set origin in SSH format for convenience reasons.
		Other options:
		  -c: Run usual git commands, i.e. status, push, pull, commit, etc..
		  -b: build repo from scratch.
		  -f: fetch repository to the specified path.
		  -h: Show this message.
	EOF
}


_CurrentDate() {
    date --iso-8601=seconds
}


_ErrorMsg() {
    {
        printf '%s\n\t%s' "[$(_CurrentDate)], ${_SCRIPT_NAME}:" "ERROR: "
        for arg in "$*";do
            printf '\t%s\n' "${arg}" >&2
        done
    } >&2

    _ErrorExit
}


_SetGitIgnore() {
    _gitDirPath="$1"
    _gitDirName="$2"


    grep -q -e "^${_gitDirName}$" "${_gitDirPath}/.gitignore" 1>/dev/null 2>&1 \
        || {
            _Verbose "Adding ${_gitDirName} to ${_gitDirPath}/.gitignore"

            echo "${_gitDirName}" >> "${_gitDirPath}/.gitignore"
        }

    _CheckFile "$1/.gitignore"
}


_CheckVars() {
    while [ $# -gt 0 ]; do
        _Verbose "Checking value for argument:" "$1=$2" # name=value

        [ -n "$2" ] || _ErrorMsg "Variable wasn't specified."

        _Verbose "PASSED"

        shift 2
    done
}


_CheckFile() {
    _Verbose "Checking file: $@"

    [ -f "$@" ] || _ErrorMsg "File $@ doesn't exist."

    _Verbose "PASSED"
}


_CheckPaths() {
    for path in $@; do
        _Verbose "Checking path: ${path}"

        [ -d "${path}" ] || _ErrorMsg "Path ${path} doesn't exist."

        _Verbose "PASSED"
    done
}


_BuildRepoFromScratch() {
    _Verbose "Building repository from scratch." "Origin: ${_ORIGIN}"

    _SetGitIgnore "${_GIT_DIR_PATH}" "${_GIT_DIR_NAME}"

    _Verbose "Initiating bare repository at ${_GIT_DIR_PATH}/${_GIT_DIR_NAME}."
    git init --bare "${_GIT_DIR_PATH}/${_GIT_DIR_NAME}"

    _Verbose "Setting 'status.showUntrackedFiles' to 'no'"
    _MyGitCmd config --local status.showUntrackedFiles no

    _Verbose "Adding origin ${_ORIGIN}"
    _MyGitCmd remote add origin "${_ORIGIN}"

    _Verbose "Setting branch to master."
    _MyGitCmd branch -M master

    _Verbose "Pushing to master."
    _MyGitCmd push -u origin master
}


_FetchFromOrigin() {
    _Verbose "Fetching repository from ${_ORIGIN}"

    _SetGitIgnore "${_GIT_DIR_PATH}" "${_GIT_DIR_NAME}"

    _Verbose "Initiating repository at ${_GIT_DIR_PATH}/${_GIT_DIR_NAME}"
    env GIT_DIR="${_GIT_DIR_NAME}" git init "${_GIT_DIR_PATH}"

    _Verbose "Setting 'status.showUntrackedFiles' to 'no'"
    _MyGitCmd config --local status.showUntrackedFiles no

    _Verbose "Adding origin ${_ORIGIN}"
    _MyGitCmd remote add origin "${_ORIGIN}"

    _Verbose "Fetching from origin."
    _MyGitCmd fetch origin

    _Verbose "Checking out from master branch."
    _MyGitCmd checkout -b master -t origin/master

    _Verbose "Resetting master branch."
    _MyGitCmd reset origin/master
}


_Verbose() {
    [ "${_VERBOSE_FLAG}" = "1" ] \
        && {
            # printf '%s\n' "[$(_CurrentDate)], ${_SCRIPT_NAME}:"
            for arg in "$@"; do
                printf "\t%s\n" "${arg}"
            done
    }
}


_Exit() {
    _Verbose "Exitting..."

    exit 0
}


_ErrorExit() {
    _Verbose "Exitting with errors..."

    exit 1
}


_MyGitCmd() {
    # Be aware of that aliases are only available in a local scope.
    # alias _MyGitCmd="/usr/bin/git --git-dir=${_GIT_DIR_PATH}/${_GIT_DIR_NAME}/ --work-tree=${_WORK_TREE_PATH}"

    _Verbose \
        "_MyGitCmd function:" \
        "/usr/bin/git --git-dir=${_GIT_DIR_PATH}/${_GIT_DIR_NAME}/ --work-tree=${_WORK_TREE_PATH} $*"

    /usr/bin/git --git-dir=${_GIT_DIR_PATH}/${_GIT_DIR_NAME}/ --work-tree=${_WORK_TREE_PATH} $*
}


_Main() {
    _Verbose "_Main function:" "_GIT_ACTION=${_GIT_ACTION}"

    _CheckPaths "${_WORK_TREE_PATH}" "${_GIT_DIR_PATH}"
    _CheckVars "_GIT_DIR_NAME" "${_GIT_DIR_NAME}" "_ORIGIN" "${_ORIGIN}"


    case "${_GIT_ACTION}" in
        "b") _BuildRepoFromScratch; _Exit ;;
        "f") _FetchFromOrigin;      _Exit ;;
        "c") _MyGitCmd $_CMD_STR;   _Exit ;;
        *)   _ErrorMsg "Wrong action was specified." ;;
    esac
}


_SCRIPT_NAME="${0##*/}"      # $(basename $0)
_SCRIPT_DIR="${0%/*}"        # $(dirname $0)

_GIT_DIR_PATH=""
_GIT_DIR_NAME=""
_WORK_TREE_PATH=""
_ORIGIN=""
_CMD_STR=""
_GIT_ACTION=""
_VERBOSE_FLAG=""


# Setting some arguments.
while getopts "p:n:t:o:c:bfvh" options; do
    case "${options}" in
        p) _GIT_DIR_PATH="$OPTARG";;
        n) _GIT_DIR_NAME="$OPTARG" ;;
        t) _WORK_TREE_PATH="$OPTARG";;
        o) _ORIGIN="$OPTARG" ;;
        c) _GIT_ACTION="c"; _CMD_STR="$OPTARG" ;;
        b) _GIT_ACTION="b" ;;
        f) _GIT_ACTION="f" ;;
        v) _VERBOSE_FLAG=1 ;;
        h) _HelpMsg; _Exit ;;
        *) _HelpMsg; _ErrorExit ;;
    esac
done


_Main
