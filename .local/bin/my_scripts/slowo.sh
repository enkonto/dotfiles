#!/usr/bin/env dash


languages="ru uk be cs pl sk sl sr mk bg de fr hi la el fa ar he"
playList="hi la el fa ar he"    # List codes for languages to play translation.
# Only Google works fine out of the box.
engine="google" # aspell google bing spell hunspell apertium yandex


for lang in ${languages}; do
    playOption=""

    echo "${playList}" \
        | grep -i -q "${lang}" \
        && playOption="-p"

    printf "%s:\t" "${lang}"
    trans \
        -b \
        -j \
        -e "${engine}" \
        ${playOption} \
        :${lang} \
        "$@"
done
