#!/usr/bin/env dash


# NOTE: Put a backslash right before a command to turn off the alias for that
#       command.  For instance `\ls' will execute `/usr/bin/ls' regardless of
#       existing aliases.

# Convenient `ls' commands. No need in `exa'. Apply `dircolors' settings in
# ~/.profile.
# List in concise format without dotfiles:
alias lxc='/usr/bin/ls \
    --group-directories-first \
    --color=always'
# List in long format with all dot files and folders:
alias lx='/usr/bin/ls \
    -A \
    --group-directories-first \
    --show-control-chars \
    --color=always'
# List in long extended format:
alias lxe='/usr/bin/ls \
    -lAh \
    --time-style long-iso \
    --group-directories-first \
    --show-control-chars \
    --color=always'
# List all directories in the CWD:
alias lxd='/usr/bin/ls \
    -lAh \
    --time-style long-iso \
    --group-directories-first \
    --show-control-chars \
    --color=always \
    | grep -i "^d"'

alias dir='/usr/bin/dir --color=auto'
alias vdir='/usr/bin/vdir --color=auto'
alias grep='/usr/bin/grep --color=auto'
alias egrep='/usr/bin/egrep --color=auto'
alias fgrep='/usr/bin/fgrep --color=auto'

alias ..='cd ..'
alias ...='cd ../..'
alias v='/usr/bin/nvim'
alias vR='/usr/bin/nvim -R'
alias emacs="/usr/bin/emacsclient -c -a 'emacs'"
alias emacs_launch_my_server="/usr/bin/emacs --daemon &"
alias cp='cp -i'        # confirm before overwriting something
alias rm='rm -i'        # confirm before deleting something
alias df='df -h'        # show human-readable output
alias du='du -hs'       # show human-readable output
alias free='free -h -t' # show human-readable output
alias mkdir='mkdir -pv' # automatically create parent directories
alias ping='ping -c 5'  # sends only 5 ICMP messages
alias clear='clear; echo Currently logged in on $TTY, as $USER in directory $PWD.'
# FIX:  lynx need it's config directory!
alias lynx='lynx \
    -cfg=~/.config/lynx/lynx.cfg \
    -lss=~/.config/lynx/lynx.lss \
    -vikeys'


alias _ruu='_uberuser'      # run as root
alias _suu='_uberuser su -' # switch to root
# Displays $PATH with each entry on a separate line.
alias _path='echo "$PATH" | tr -t ":" "\n"'
# Display the processes using the most memory.
alias _memhog='ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%mem | head'
alias _psmem='ps -e -orss=,args= | sort -b -k1,1n'
# Display the top 10 processes using the most memory.
alias _psmem10='ps -e -orss=,args= | sort -b -k1,1n | head -10'
# Display the top processes using the most CPU.
alias _cpuhog='ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%cpu | head'
alias _pscpu='ps -e -o pcpu,cpu,nice,state,cputime,args |sort -k1 -nr'
# Display the top 10 processes using the most CPU.
alias _pscpu10='ps -e -o pcpu,cpu,nice,state,cputime,args |sort -k1 -nr | head -10'
# Display the top 10 most used commands in the history.
alias _histop='print -l ${(o)history%% *} | uniq -c | sort -nr | head -n 10'
# Convenient rsync's commands.
alias _rsync-copy='rsync -avz --progress -h'
alias _rsync-move='rsync -avz --progress -h --remove-source-files'
alias _rsync-update='rsync -avzu --progress -h'
alias _rsync-update-no-compress='rsync -avu --progress -h'
alias _rsync-synchronize='rsync -avzu --delete --progress -h'
alias _rsync-cpv='rsync -pogbr -hhh --backup-dir=/tmp/rsync -e /dev/null --progress'
# Convenient docker's commands.
alias _Dcomp='docker-compose '
alias _Dimages='docker-compose images'
alias _Dps='docker ps'
alias _Dbuild='docker-compose down && docker-compose up --build'
alias _Dstop='docker stop $(docker ps -a -q)'
# Concise translator's commands.
alias trans='trans -no-ansi -pager less'
alias _Tru='trans -no-ansi -pager less :ru'
alias _Ten='trans -no-ansi -pager less :en'
alias _Tde='trans -no-ansi -pager less :de'
alias _Tpl='trans -no-ansi -pager less :pl'
alias ytd='yt-dlp'  # YouTube Downloader
alias hwmon='inxi -Fxz'  # comprehensive hardware data
# Bare git repo aliases for system configs.
alias _gitdots='/usr/bin/git --git-dir=${HOME}/.gitdots/ --work-tree=${HOME}'
alias _gitetc='/usr/bin/git --git-dir=${HOME}/.gitetc/ --work-tree=/etc/'
