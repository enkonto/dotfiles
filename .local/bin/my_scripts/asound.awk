#!/bin/awk

# Get hardware (card of device) number of the current record/string line.
function get_settings(input_line,hw_type) { 
    split(input_line,input_line_array); 
    current_hw_num=input_line_array[2]; 
    delete input_line_array; 
    if (current_hw_num ~/[1-9][0-9];$|[0-9];$|;$/) 
        gsub(";","",current_hw_num); 
    # print current_hw_num; 
    # Put current numbers in array.
    if (hw_type=="card") current_hardware[1]=current_hw_num; 
    if (hw_type=="device") current_hardware[2]=current_hw_num; 
} 

# Change hardware number of the current record/string line.
function change_settings(input_line,new_number) { 
    # Mark the line as line with settings.
    settings_line++; 
    get_settings(input_line) 
    # printf "%s\n", "\t\t\tDEBUGGING: current hw num= "current_hw_num; 
    # printf "%s\n", "\t\t\tDEBUGGING: new number= "new_number; 
    if (current_hw_num!=new_number) 
        gsub(/[1-9][0-9];$|[0-9];$|;$/,new_number";",input_line); 
        # printf "%s\n", "\t\t\tDEBUGGING: new number= "new_number; 
    print input_line #> conf_file; 
} 

# Write changeable settings to the configuration file.
function write_settings(current_input_line,new_card,new_device) {
    # printf "%s", "\t\t\tDEBUGGING: new card= ";
    # print new_card"\tnew device= "new_device;
    settings_line=0; 
    split(current_input_line,current_input_line_array); 
    # printf "%s", "\t\t\tDEBUGGING: current input line array= ";
    # print current_input_line_array[1];
    # Prints card setting.
    for ( i in card_array ) 
        if ( current_input_line_array[1]==card_array[i] )
            change_settings(current_input_line,new_card); 
    # Prints device setting.
    for ( i in device_array ) 
        if ( current_input_line_array[1]==device_array[i] )
            change_settings(current_input_line,new_device); 
    delete current_input_line_array; 
    # Prints line without card/device setting. Set variable settings_line 
    # to zero if the line does not contain any setting to change.
    if (settings_line==0) print current_input_line #> conf_file; 
    }

# Find index number of the current settings in the list of plugged cards and 
# devices.
function find_position() { 
    # print "\t\t\tDEBUGGING: FIND POSITION FUNCTION"
    # print "\t\t\tDEBUGGING: length of hw array= " length(hw_array);
    # printf "%s", "\t\t\tDEBUGGING: current hw= ";
    # print current_hardware[1]" "current_hardware[2]; 
    for (i in hw_array) {
        split(hw_array[i],current_hw,"FLDSPRTR"); 
        if ( current_hw[1]==current_hardware[1] &&
            current_hw[2]==current_hardware[2] ) hw_index=i; 
        # print "\t\t\tDEBUGGING: current i of hw array= "i;
        # print "\t\t\tDEBUGGING: current hw array index= "hw_index;
# TODO: Debug this.
        if ( current_hw[1]!=current_hardware[1] ||
            current_hw[2]!=current_hardware[2] ) missed_hw_mark=1;
    }
    delete current_hw;
    hw_index==length(hw_array) ? next_index=1 : next_index=++hw_index; 
    # print "\t\t\tDEBUGGING: next hw array index= "next_index;
    # split(hw_array[next_index],next_hw,"FLDSPRTR"); 
    # printf "%s", "\t\t\tDEBUGGING: next= ";
    # print next_hw[1]" "next_hw[2]" "next_hw[3]; # delete next_hw 
} 

# Show thins message if current card or device wasn't found in the list of
# plugged cards and devices.
function missed_hw_msg() {
    printf "%s", "Current card ("current_hardware[1]") and current device ("; 
    printf "%s", current_hardware[2]") missed in ALSA's hardware playback "; 
    printf "%s\n", "list."; 
    printf "%s", "Please, enter manualy desired card number and device number "; 
    printf "%s\n\n", "form the list below:"; 
    print "Cards:\tDevices:\tNames:"
    for (i in hw_array) {
        split(hw_array[i],current_hw,"FLDSPRTR"); 
        print current_hw[1]"\t"current_hw[2]"\t\t"current_hw[3]; 
    }
}

# Convert card list and device list variables to arrays.
BEGIN { 
    # Converting strings to arrays.
    split(card_list,card_array); 
    # for ( i in card_array ) print card_array[i]; 
    split(device_list,device_array); 
    # for ( i in device_array ) print device_array[i]; 
} 

# Collect all data in to array hw_array.
FILENAME==aplay_pipe && $1=="card" && new_card=="" && new_device=="" { 
    split($0,current_line); 
    # Get card number.
    for (i in current_line) 
        if (current_line[i]=="card") 
        card=current_line[i+1]; 
    gsub(":","",card); 
    # Get device number.
    for (i in current_line) 
        if (current_line[i]=="device") 
        device=current_line[i+1]; 
    delete current_line; 
    gsub(":","",device); 
    # Get hardware name.
    split($0,name_aplay,/\[|\]/); 
    name=name_aplay[2]" - "name_aplay[4]; delete name_aplay; 
    # Put card, device, name to array.
    ++hw_dev; 
    hw_array[hw_dev]=card"FLDSPRTR"device"FLDSPRTR"name; 
    # split(hw_array[hw_dev],current_hw,"FLDSPRTR"); 
    # printf "%s", "\t\t\tDEBUGGING: collected hw= ";
    # print current_hw[1]" "current_hw[2]" "current_hw[3]; 
    # delete current_hw 
} 

# If both arguements was inputted, then try to set those card and device.
FILENAME==conf_file && new_card ~/[0-9]/ && new_device ~/[0-9]/ { 
    write_settings($0,new_card,new_device); 
    } 

# Write current record to configuration file, if the record don't contain any 
# settings to change.
FILENAME==conf_file && new_card=="" && new_device=="" { 
    for ( i in card_array ) 
        if ( $1==card_array[i] ) get_settings($0,"card"); 
    for ( i in device_array ) 
        if ( $1==device_array[i] ) get_settings($0,"device"); 
    ++file_array_index;
    whole_file_array[file_array_index]=$0;
    } 
    
# If both card and device number wasn't inputted, then try to change to the 
# next card and device pair.
END { 
    # print "\t\t\tDEBUGGING: END FUNCTION"
    if ( new_card=="" && new_device=="" ) find_position(); 
    if ( missed_hw_mark==1 ) missed_hw_msg();
    else
        for ( i in whole_file_array ) {
            # print "\t\t\tDEBUGGING: whole file array= "whole_file_array[i];
            write_settings(whole_file_array[i],next_hw[1],next_hw[2]);
        }
} 
