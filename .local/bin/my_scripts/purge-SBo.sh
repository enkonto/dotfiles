#!/usr/bin/env dash


# This script is aimed to purge in the interactive mode all program's old
# versions made by `sbopkg' at the current directory.


CWD="$(pwd)"


list_files(){
    /usr/bin/ls -1 "$@"
}

remove_files(){
    /usr/bin/rm -i "$@"
}


for line in $(list_files ${CWD}/*SBo.tgz); do
    name="${line%%-[0-9]*}"
    doub_list=$(list_files ${name}-[0-9]*SBo.tgz)
    doub_counter="$(printf "%s\n" "${doub_list}" | wc -l)"

    if [ "${doub_counter}" -gt 1 ]; then
        purge_list="$(printf "%s\n" "${doub_list}" \
            | head -n $((${doub_counter} - 1)))"
        for file in ${purge_list}; do
            remove_files "${file}"
        done
    fi

done
