#!/usr/bin/env perl


#use v5.10;
use v5.34;
use utf8;
use warnings;
use diagnostics;    # One should better use `perl -Wdiagnostics' for debugging.
use strict;
use autodie;
no warnings "experimental::signatures";
use experimental qw(signatures); #use feature 'signatures';
#use Cpanel::JSON::XS;   # May be absent on your system by default, unfortunately.
use JSON::XS;
use Sys::Hostname;


my $SCRIPT_NAME    = $0 =~ s/^\/*.*\///r;
my $SCRIPT_DIR     = $0 =~ s/\/$SCRIPT_NAME$//r;
my $DEBUG;
my $VERBOSE;


sub HelpMsg {
    print STDERR << "	EOF";
	Options are:
	  -h:       Show this message;
	  -v:       Run in verbose mode;
	  -D[1-3]:  Run in debug mode;
	  -l:       list hardware;
	  -u:       list local user settings;
	  -mX,Y:    specify relatively card and device ID pair manualy,
	            neither can't be greater then 99.
	EOF
    exit;
};
#HelpMsg;


sub ErrorMsg {
    # One should use `strftime' if milliseconds are have to be shown.
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
    $year += 1900;
    ++$mon;
    print STDERR "[${year}-${mon}-${mday}T${hour}:${min}:${sec}+timezone], $SCRIPT_NAME:\n";

    foreach my $line ( @_ ) {
        print STDERR "\t$line\n";
    };
}
ErrorMsg "error1" . "error2", "error3";


sub StoreHardwareData {
    my @hardwareLoL;
    my $lineCounter = 0;


    foreach ( `aplay -l` ) {
        if ( /^card/ ) {
            chomp;
            push ( @hardwareLoL, [ split ( /: |, /, $_ ) ] );
            $hardwareLoL[ $lineCounter ]->[0] =~ s/^card //;
            $hardwareLoL[ $lineCounter ]->[2] =~ s/^device //;
            $lineCounter++;
        };
    }

    \@hardwareLoL;
}


sub ListPlayback ( $hwLoLRef ) {
    # List of playback hardware devices.
    foreach ( @$hwLoLRef ) {
        print $_->[0] . "\t";
        print $_->[1] . "\t";
        print $_->[2] . "\t";
        print $_->[3] . "\n";
    }
    exit;
}


sub SetPlayback ( $hwLoLRef, $cardId, $deviceId ) {
    my $newCardId;
    my $newDeviceId;
    my $countMatches = 0;


    foreach ( @$hwLoLRef ) {
        if ( $cardId == $_->[0] && $deviceId == $_->[2] ) {
            $newCardId = $cardId;
            $newDeviceId = $deviceId;
            $countMatches++;
        }
    }

    if ( $countMatches == 1 )   { say $newCardId . " " . $newDeviceId }
    else                        { die "Too much or too few ID pairs matched.\n" };
    #doas /etc/rc.d/rc.alsa restart
    exit;
}


sub ReadLocalSettings ( $cardPatternListRef, $devicePatternListRef, $localConfFh ) {
    sub ShowPattern ( $patterListRef ) {
        for my $item ( @$patterListRef ) {
            if ( /$item/ ) {
                say $item . ":\t" . $_ =~ s/^$item\s+(\d)\s*;$/$1/r
            }
        }
    };


    sub StorePattern ( $line, $patterListRef, $localSettingsHashRef ) {
        for my $pattern ( @$patterListRef ) {
            if ( $line =~ /$pattern/ ) {
                my $idNum = $line =~ s/^$pattern\s+(\d)\s*;$/$1/r;
                say $pattern . ":\t" . $idNum;
                if ( ref( $localSettingsHashRef->{ $pattern } ) ne "ARRAY" ) {
                    say "Adding '${idNum}' to '${pattern}' array...";
                    $localSettingsHashRef->{ $pattern } = [ $idNum ];
                };
                foreach ( @{$localSettingsHashRef->{ $pattern }} ) {
                    say "'${pattern}' has: " . $_;
                } ;
            }
        }
    };


    my %localSettingsHash;


    while ( <$localConfFh> ) {
        chomp;
        StorePattern( $_, $cardPatternListRef, \%localSettingsHash );
        #StorePattern( $devicePatternListRef );
        #ShowPattern( $cardPatternListRef );
        #ShowPattern( $devicePatternListRef );
    };
}


chomp ( my $HOME = `echo "\$HOME"` );
open my $DEFAULT_CONF_FH,   '<',    "$HOME/.local/bin/my_scripts/asound.conf";
open my $LOCAL_CONF_FH,     '<',    "$HOME/.asoundrc";
#if ( ! $DEFAULT_CONF_FH )   { die "Failed to read default configuration file.\n" };
#if ( ! $LOCAL_CONF_FH )     { die "Failed to read local configuration file.\n" };
# It's supposed that all cards have the same identification number in the
# configuration file. Same thing for the devices.
my @CARD_PATTERN_LIST   = qw( defaults.ctl.card   defaults.pcm.card );
my @DEVICE_PATTERN_LIST = qw( defaults.ctl.device defaults.pcm.device );
my $HW_LOL_REF          = StoreHardwareData;


ReadLocalSettings ( \@CARD_PATTERN_LIST, \@DEVICE_PATTERN_LIST, $LOCAL_CONF_FH );
while ( @ARGV && $ARGV[0] =~ /^-/ ) {
    $_ = shift;
    #say $_;
    last if /^--$/;
    if ( /^-h$/ )           { HelpMsg; next };                      # Will exit.
    if ( /^-D([1-3])$/ )    { $DEBUG = $1; say "Debug level: $DEBUG"; next };
    if ( /^-v$/ )           { $VERBOSE++; say "Verbose mode is on."; next };
    if ( /^-l$/ )           { ListPlayback ( $HW_LOL_REF ); next }; # Will exit.
    if ( /^-m([0-9]{1,2}),([0-9]{1,2})$/ )
                            { SetPlayback ( $HW_LOL_REF, $1, $2 ); next };  # Will exit.
    #...                    # other switches
    if ( /^-.*$/ ) { die "Wrong switch: '$_'. '-h' for help.\n"; }; # Last one.
}
