#!/usr/bin/env dash


# References:
#   https://unix.stackexchange.com/questions/93791/benchmark-ssd-on-linux-how-to-measure-the-same-things-as-crystaldiskmark-does-i
#   https://forums.freebsd.org/threads/is-there-an-alternative-for-crystaldiskmark.77251/
#   https://www.jamescoyle.net/how-to/599-benchmark-disk-io-with-dd-and-bonnie
#   https://www.brendangregg.com/ActiveBenchmarking/bonnie++.html


pathToBench="$@"
pathToBench="${pathToBench:-/tmp}"
flags="oflag=dsync"
#flags="conv=fdatasync,notrunc"  # alternate syntax for GNU/dd
benchCount=1
benchmarks=$(cat <<- EOF
	512:256
	4k:256
	512k:192
	64M:12
	1G:1
	EOF
)


_checkDep() {
    # Check if command is executable.
    command -v "$1" > /dev/null \
        || return 1
}


_ddBench() {
    echo "${benchmarks}" \
        | while IFS=":" read -r blockSize count; do
            # Let drive to cool a little.  Difference between 5 and 60 seconds is
            # negligible for disk with write rate 430MB/s , +/- 1...2%.
            [ "${benchCount}" -gt "1" ] \
                && {
                    sleep 5; printf "\n"
                }

            printf "%s\n" "${pathToBench}/dd_ssd_bench_${benchCount}.img	bs=${blockSize}	count=${count}"

            echo "Writing..."; dd \
                if=/dev/zero \
                of=${pathToBench}/dd_ssd_bench_${benchCount}.img \
                bs=${blockSize} \
                count=${count} \
                ${flags}

            # Clear Ram Cache:
            # First choice you have is to Clear PageCache only.
            # The second option you have is to Clear dentries and inodes.
            # The third and the last choice you get is to Clear pagecache, dentries, and
            # inodes.
            #sudo su -c "sync; echo 3 > /proc/sys/vm/drop_caches"
            sync; echo 3 > /proc/sys/vm/drop_caches

            echo "Reading..."; dd \
                if=${pathToBench}/dd_ssd_bench_${benchCount}.img \
                of=/dev/null \
                bs=${blockSize} \
                count=${count} \
                ${flags}

            benchCount=$((benchCount + 1))
        done


    printf "\n%s\n" "Deleting files from ${pathToBench}..."
    rm ${pathToBench}/dd_ssd_bench_*.img
}


_bonnieBench() {
    read -p "Specify user or user:group for \`bonnie++' or leave it as deafalt 1000: " bonnieUser
    bonnieUser="${bonnieUser:-1000}"

    bonnie++ -d ${pathToBench} -r 2048 -u ${bonnieUser}
}


_fioBench() {
    fio \
        --loops=5 \
        --size=1000m \
        --filename=${pathToBench}/fio_ssd_bench.tmp \
        --stonewall \
        --ioengine=libaio \
        --direct=1 \
        --name=Seqread --bs=1m --rw=read \
        --name=Seqwrite --bs=1m --rw=write \
        --name=512Kread --bs=512k --rw=randread \
        --name=512Kwrite --bs=512k --rw=randwrite \
        --name=4kQD32read --bs=4k --iodepth=32 --rw=randread \
        --name=4kQD32write --bs=4k --iodepth=32 --rw=randwrite


    printf "\n%s\n" "Deleting files from ${pathToBench}..."
    rm ${pathToBench}/fio_ssd_bench.tmp
}


[ "$(whoami)" != "root" ] \
    && {
        cat <<- EOF
			Run as root, please.
			I.e. \`sudo su -s /path/to/ssd_benchmark.sh'
		EOF
        exit 1
    }


cat <<- EOF
	Using ${pathToBench} directory.
	One should use \`iozone', \`bonnie++', \`kdiskmark' or \`fio' instead of \`dd'.
EOF


if _checkDep fio; then
    printf "%s\n\n" "Benchmarking via \`fio'"
    _fioBench
elif _checkDep bonnie++; then
    printf "%s\n\n" "Benchmarking via \`bonnie++'"
    _bonnieBench
elif _checkDep dd; then
    printf "%s\n\n" "Benchmarking via \`dd'"
    _ddBench
else
    cat <<- EOF
		Missing dependencies.
		There is no any suitable program installed to benchmark with.
		EOF

    exit 1
fi
