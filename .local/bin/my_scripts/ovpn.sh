#!/usr/bin/env dash


. my_functions


conf_path="${HOME}/.local/share/ovpn"
auth_path="${HOME}/.keys/ovpn"
# Not all distros allow to a non root user to see the whole scope of executables.
ovpn_exec="/usr/sbin/openvpn"
config="$1"
provider="$2"


_kill_ovpns() {
    pgrep -x openvpn \
        && _uberUser pkill -x openvpn
    while pgrep -x openvpn; do sleep 0.1; done
}


_list_srvrs() {
    printf '%s\n' \
        "= Configuration files ==========================================="
    ls ${conf_path}/*/*.ovpn
    printf '%s\n' \
        "= Authentication files =========================================="
    ls ${auth_path}
}


_main() {
    # TODO: If there is not any predefined symbolic link for a location in the
    #       specific country in the folder with configurations, then choose a
    #       random location for this country.
    if [ -f "${ovpn_exec}" ] \
        && [ -f "${conf_path}/${provider}/${config}.default" ] \
        && [ -f "${auth_path}/${provider}" ]; then
            pgrep -x openvpn \
                && _uberUser pkill -x openvpn
            while pgrep -x openvpn; do sleep 0.1; done
            _uberUser "${ovpn_exec}" \
                --config "${conf_path}/${provider}/${config}.default" \
                --auth-user-pass "${auth_path}/${provider}" \
                --writepid /var/run/openvpn.pid \
                --user nobody --group nobody \
                --daemon --verb 4
            if [ "$?" = "0" ]; then
                printf '\t%s\n' \
                    "Loading '${config}.default' for '${provider}'."
            else
                printf '\t%s\n' \
                    "Failed to load" \
                    "'${conf_path}/${provider}/${config}.default'" \
                    "for '${provider}'."
            fi
    else
        printf '\t%s\n' \
            "Fail to find" \
            "'${conf_path}/${provider}/${config}.default'" \
            "or '${auth_path}/${provider}'."
        exit 1
    fi
}


case $@ in
    list)
        _list_srvrs
        ;;
    kill)
        _kill_ovpns
        ;;
    *)
        _main
        ;;
esac
