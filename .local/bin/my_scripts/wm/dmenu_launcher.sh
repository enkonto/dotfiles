#!/usr/bin/env dash


_loadGoods() {
    . my_functions
    . my_aliases
}


# Lists possible actions to choose from.
_listActions() {
    printf "%s\n" \
        "System monitor" \
        "Alsamixer" \
        "File manager" \
        "Calculator" \
        "My notes" \
        "Wiki" \
        "Jabber" \
        "Mail" \
        "IRC" \
        "Transmission client" \
        | _dynamicMenu 6 2 "Launch"
}


_loadGoods
choice="$(_listActions)"    # Get stdout of `_listActions'.

# Run the selected command. Be aware of that the case variable should be first
# letters of the action in the list correspondingly.
case "$choice" in
    Sy*) eval $EMBTERM htop ;;
    Al*) eval $EMBTERM alsamixer ;;
    Fi*) eval $EMBTERM nnn ;;
    Ca*) eval $EMBTERM bc ;;
    My*) eval $EMBTERM v --cmd ':cd ~/docs/notes' ;;
    Wi*) eval $EMBTERM v $XDG_DATA_HOME/vimwiki/index.wiki ;;
    Ja*) eval $EMBTERM mcabber ;;
    Ma*) eval $EMBTERM neomutt ;;
    IR*) eval $EMBTERM irssi ;;
    Tr*) eval $EMBTERM torque ;;  #tremc ;;   #stig ;;
esac
