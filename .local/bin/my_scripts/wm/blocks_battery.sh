#!/usr/bin/env dash

# Dependencies: `xbacklight'

#      


_checkDep() {
    command -v "$1" >/dev/null || return 1
}


_timeLeft() {
    # Don't decrease delay less then 30 seconds. It have to be double less then
    # renew threshold of your status line. Than higher delay, then result of
    # computation of estimated time prediction is more accurate, yet lag is
    # higher to provide each battery status.
    delay=30    # in seconds
    left=""
    full_lvl="$(cat "$@"/energy_full 2>/dev/null)"
    old_lvl="$(cat "$@"/energy_now 2>/dev/null)"
    sleep "${delay}"; new_lvl="$(cat "$@"/energy_now 2>/dev/null)"
    charge_velo=$(((new_lvl - old_lvl) / delay))

    if [ "${charge_velo}" -gt 0 ]; then
        seconds=$(((full_lvl - new_lvl) / charge_velo))
    elif [ "${charge_velo}" -lt 0 ]; then
        seconds=$(((0 - new_lvl) / charge_velo))
    fi

    minutes=$((seconds / 60))
    min_rem=$((minutes % 60))
    hours=$((minutes / 60))
    [ "${min_rem}" -gt 0 ] && minutes="${min_rem}"
    left="${hours}:${minutes}"

    if [ -n "${left}" ] && [ "${left}" != "0:0" ]; then printf "%s" "${left}"; fi
}


# Wrap code in function since there is lag after ``_timeLeft'' function
# implemented.
_buildStr() {
# Execute for every battery in the following path.
    for battery in /sys/class/power_supply/BAT?; do

        # Define battery's capacity. If error then there's no any batteries, exit.
        capacity=$(cat "${battery}"/capacity 2>/dev/null) || exit 1
        # Define battery's status.
        status=$(cat "${battery}"/status)
        left=$(_timeLeft ${battery})

        # Avoid lonely backslash character after percentage.
        #if [ "${capacity}" -ge 98 ]; then
        #    left=""
        #else
        #    # Get estimated time to full/empty battery.
        #    if _checkDep acpi; then
        #        left=$(acpi -b | awk -c '{n=5; printf "/%.5s\n", $n}')
        #    else
        #        left=$(_timeLeft ${battery})
        #    fi
        #fi

        # Check capacity to draw battery icon.
        case ${capacity} in
            9[0-9]|100)             status_icon="" ;;
            7[0-9]|8[0-9])          status_icon="" ;;
            4[0-9]|5[0-9]|6[0-9])   status_icon="" ;;
            2[0-9]|3[0-9])          status_icon="" ;;
            *)
                                    status_icon=""

                # Set back light of laptop display to 30% if AC is unplugged and
                # current backlight level is greater then 30%.
                _checkDep xbacklight \
                    && [ $(cat /sys/class/power_supply/AC/online) -eq 0 ] \
                    && [ $(printf "%.2s\n" $(xbacklight -get)) -ge 30 ] \
                    && { \
                        xbacklight -set 30
                        # Send signal for `dwmblocks' to renew it's brightness state.
                        sleep 0.5; pkill -RTMIN+5 dwmblocks
                    }
                ;;
        esac

        printf "%s%s%s\n" "$(printf '%s' "${status}" \
            | sed \
                "s/,//; \
                s/Discharging/${status_icon}/; \
                s/Not charging/${status_icon}/; \
                s/Unknown/${status_icon}/; \
                s/Charging//; \
                s/Full//; \
                s/ 0*/ /g; \
                s/ :/ /g")" \
            "$(printf '%s' "${capacity}")" \
            "$(printf '%s' "${left}")"
    done
}


printf "%s\n" "$(_buildStr)"
