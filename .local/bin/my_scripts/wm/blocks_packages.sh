#!/usr/bin/env dash

# Check monthly, after every 1st date of the month. Create a flag in /tmp.
# This script should to check presence of the flag. Approach like this should
# provide distribution wide compatibility.

p_number = $(doas xi -Mun | wc -l)
echo "$p_number"
# if > 100 then draw                  
