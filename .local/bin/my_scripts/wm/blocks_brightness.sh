#!/usr/bin/env dash

# Dependencies: `xbacklight'


#_checkDep() {
#    command -v "$1" > /dev/null || return 1
#}


#_checkDep xbacklight && xbl_lvl="$(xbacklight -d eDP-1 -get 2&>/dev/null)"
bl_path="/sys/class/backlight/intel_backlight"


if [ -d "${bl_path}" ]; then
    current_lvl="$(cat ${bl_path}/brightness)"
    max_lvl="$(cat ${bl_path}/max_brightness)"
    lvl="$(( $current_lvl * 1000 / $max_lvl / 10 ))"
elif [ -n "${xbl_lvl}" ]; then
    lvl="${xbl_lvl}"
else
    printf '%s\n' "  error"
    return 1
fi

printf "%0.f%%\n" "${lvl}"
