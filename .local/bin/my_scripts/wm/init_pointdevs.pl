#!/usr/bin/env perl

#use v5.10;
use v5.34;
use utf8;
use warnings;
use diagnostics;    # One should better use `perl -Wdiagnostics' for debugging.
use strict;
use autodie;
no warnings "experimental::signatures";
use experimental qw(signatures); #use feature 'signatures';
#use Cpanel::JSON::XS;   # May be absent on your system by default, unfortunately.
use JSON::XS;
use Sys::Hostname;


# Description.
# =============

# Set mice, trackballs, trackpoints, trackpads via `xinput'.
# Toggle touchpad via `synaptics' or `xinput'.
# Use IDs instead of names for proper properties settings for `xinput' because
# some devices have trailing spaces in their names thus can be bugs issued.
# Output of `xinput --list' have to be in the same order for both names and
# IDs to keep device array in the right order.

# Dependencies: `xbanish', `synclient', `xinput', `unclutter-xfixes', `ksh'


# Specifying settings in JSON.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# [
#     {
#         "comment": "This value is not used in script and can be omitted.",
#         "device name": "Gaming Device",
#         "hostnames": [ "desktop", "laptop" ],
#         "libinput": {
#             "Left Handed Enabled": "1",
#             "Accel Speed": "-0.55",
#             "Accel Profile Enabled": "0, 1"
#         }
#     },
#     {
#       ...
#     }
# ]


# TODO: Check for dependencies.

my $SCRIPT_NAME    = $0 =~ s/^\/*.*\///r;
my $SCRIPT_DIR     = $0 =~ s/\/$SCRIPT_NAME$//r;
my $DEBUG;
my $VERBOSE;
my $LOCAL_HOSTNAME = hostname(); if ( ! $LOCAL_HOSTNAME ) { die "Missing hostname." };
my $PROPS_FILE_NAME = "init_pointdevs.json";  # Must be in the same folder as $0.
my $TOGGLE_TOUCHPAD;


sub HelpMsg {
    print STDERR << "	EOF";
	Options are:
	  -h:       Show this message;
	  -v:       Run in verbose mode;
	  -D[1-3]:  Run in debug mode;
	  -t:       Toggle touch pad.
	EOF
    exit;
};
#HelpMsg;


sub ErrorMsg {
    # One should use `strftime' if milliseconds are have to be shown.
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
    print STDERR "[", $year + 1900, $mon + 1, $mday, "T$hour$min$sec], $SCRIPT_NAME:\n";

    foreach my $line ( @_ ) {
        print STDERR "\t$line\n";
    };
}
#ErrorMsg "error1" . "error2", "error3";


sub BuildDevList( $idListRef, $nameListRef ) {
    # Returns an array: device IDs as indexes, device name as items' value.
    # BEWARE of that some items can be undefined and `print @devList' should
    # croak a warning, but it's ok. Just skip the empty ones.

    my @devList;


    for my $item ( 0..@$idListRef - 1 ) {
        $devList[$$idListRef[$item]] = $$nameListRef[$item];
    };

    chomp(@devList);
    @devList;
}


sub GetDevArray {
    my @devIdList   = `xinput --list --id-only`;
    my @devNameList = `xinput --list --name-only`;


    if ( @devIdList == @devNameList ) { BuildDevList ( \@devIdList, \@devNameList ); }
    else                              { die "! \@devIdList eq \@devNameList\n"; };
}


sub GetDevId( $devNameRef, $devArrayRef ) {
    # Index is ID.
    my @indexes;


    for my $index ( 0 .. @$devArrayRef - 1 ) {
        # if ( $$devArrayRef[$index] && $$devArrayRef[$index] =~ $$devNameRef )
        if ( $$devArrayRef[$index] && $$devArrayRef[$index] eq $$devNameRef )
        { push @indexes, $index; };
    };

    if    ( scalar( @indexes ) == 1 ) { $indexes[0] }
    elsif ( scalar( @indexes ) == 0 ) { return 0 }
    else  { die "Too many IDs(@indexes) have been gethered for a device name.\n" };
}

sub XinputSwitchBool ( $propertyName, $deviceId ) {
    my $newValue;
    my @devProps     = `xinput --list-props "$deviceId"`;
    my @propsGrepped = grep { /$propertyName[ \t]+\(/ } @devProps;

    if ( @propsGrepped eq 1 ) { $propsGrepped[0] =~ s/^.+:[ \t]+// ;
                                if    ( $propsGrepped[0] =~ /^1$/ ) { $newValue = "0" }
                                elsif ( $propsGrepped[0] =~ /^0$/ ) { $newValue = "1" }
                                else                                { die "Old value is innapropriate.\n" } }
    else                      { die "Grepped too much!\n" };

    if ( $DEBUG ) { say "`xinput --set-prop \"$deviceId\" \"$propertyName\" \"$newValue\"`" };
    `xinput --set-prop "$deviceId" "$propertyName" "$newValue"`
};


sub GrepDev {
    my ( $devArrayRef, $searchExprRef ) = @_;
    my $devName;
    my $countDevs;
    my $grepDevErrorMsg = "Can't define $$searchExprRef device!";


    foreach ( @$devArrayRef ) {
        if ( $_ && /$$searchExprRef/ )          { $devName = $_; ++$countDevs; };
        if ( $countDevs && $countDevs gt 1 )    { die "$grepDevErrorMsg\n"; }
    }

    if ( $devName ) { $devName }
    else            { die "$grepDevErrorMsg\n"; }
}


sub ToggleTouchpad ( $devArrayRef ) {
# Alternative:
#    synclient TouchpadOff=$(synclient -l \
#        | sed -n 's/^[ \t]*TouchpadOff[ \t]*=[ \t]*0$/1/p; \
#                s/^[ \t]*TouchpadOff[ \t]*=[ \t]*1$/0/p') \
#        >/dev/null 2>&1


    my $searchExpr   = "Synaptics";
    my $propertyName = "Synaptics Off";
    my $synapticsDevName = GrepDev( $devArrayRef, \$searchExpr );


    XinputSwitchBool($propertyName, GetDevId ( \$synapticsDevName, $devArrayRef ));
};


sub SetMappingProps ( $mappingStrRef, $deviceIdRef ) {
    if ( $DEBUG ) { say "`xinput --set-button-map \"$$deviceIdRef\" $$mappingStrRef`" };
    `xinput --set-button-map \"$$deviceIdRef\" $$mappingStrRef`;
}


sub SetInHashProps ( $deviceHashRef, $driverRef, $deviceIdRef ) {
    my $driverHashRef = %$deviceHashRef{$$driverRef};
    my @properties = keys %{$driverHashRef};


    for my $key ( @properties ) {
        my $value = %{$driverHashRef}{$key};

        if ( $value ne "") {
            if ( $DEBUG ) { say "`xinput --set-prop \"$$deviceIdRef\" \"$$driverRef $key\" $value`" };
            `xinput --set-prop \"$$deviceIdRef\" \"$$driverRef $key\" $value`;
        }
        else { next };
    }
};


sub SetPointers ( $devArrayRef ) {
    open my $pointdevsPropsFileFH, '<', "$SCRIPT_DIR/$PROPS_FILE_NAME";
    my $devsPropsDataRef = do {
        local $/ = undef;
        decode_json ( <$pointdevsPropsFileFH> );
    };
    close $pointdevsPropsFileFH;


    foreach ( @$devsPropsDataRef ) {
        my $deviceName       = %$_{'device name'};
        my $hostnamesListRef = %$_{'hostnames'};
        my $mappingStrRef    = \%$_{'mapping'};  # Can be undefined.
        my $libinputHashRef  = %$_{'libinput'};  # Can be undefined.
        my $synapticsHashRef = %$_{'Synaptics'}; # Can be undefined.
        my $devsPropsHashRef = $_;
        my @inpDriverKeyList = qw ( libinput Synaptics );
        my $deviceId         = GetDevId ( \$deviceName, $devArrayRef );


        if ( ! $deviceName )                                  { die "Missing device name." }
        if ( ! $hostnamesListRef || @$hostnamesListRef < 1 )  { die "Missing hostnames." };
        if ( ( ! grep { /$LOCAL_HOSTNAME/ } @$hostnamesListRef ) || ( ! $deviceId ) ) { next };
        if ( $DEBUG ) { say "id: " . $deviceId . "\tdevice: " . $deviceName };
        if ( $$mappingStrRef ) { SetMappingProps( $mappingStrRef, \$deviceId ) };


        foreach my $driver ( @inpDriverKeyList ) {
            if ( %$devsPropsHashRef{$driver} ) {
                SetInHashProps( $devsPropsHashRef, \$driver, \$deviceId )
            }
            else { next };
        };
    }

};



my @devArray = GetDevArray;

# TODO: Add verbose mode.

while ( @ARGV && $ARGV[0] =~ /^-/ ) {
    $_ = shift;
    say $_;
    last if /^--$/;
    if ( /^-h$/ )           { HelpMsg; next };  # Will exit.
    if ( /^-D([1-3])$/ )    { $DEBUG = $1; say "Debug level: $DEBUG"; next };
    if ( /^-v$/ )           { $VERBOSE++; say "Verbose mode is on."; next };
    if ( /^-t$/ )           { $TOGGLE_TOUCHPAD++; next };
    #...                    # other switches
    if ( /^-.*$/ ) { die "Wrong switch: '$_'. '-h' for help.\n"; }; # Last one.
}


if ( $TOGGLE_TOUCHPAD ) { ToggleTouchpad( \@devArray ) }
else                    { SetPointers ( \@devArray ) };
