
# General tips for selecting a computer keyboard.
-------------------------------------------------

Buy a keyboard such that the <<Ctrl>> and <Alt> keys are large. Buy a
keyboard where <Ctrl> and <Alt> are also available on the right side. The
<Ctrl> and <Alt> key's positions on the left and right sides should have
the same distance to your left and right thumbs (while your hands are rested
in standard touch-type position). Specifically: the distance from the left
<Alt> to the F key should be the same as the right <Alt> to the J key.
Look at the distance from left <Alt> and F. They should not be far. This
lets you easily hit <Alt> by a thumb curl. Same for Right <Alt> and J.
Mechanical keyboard helps greatly.

Laptop computer keyboards are the worst beast. It is the quickest way to get
RSI. The keys are packed into a neat little rectangular space and flat.
<Ctrl> and <Alt> become tiny squares, jammed together with Fn. Many
dedicated keys such as Home, PageDown, Arrow keys, are reshaped into squares
to fit into the rectangular array — losing their distinct positions that can
be easily located by touch. Dedicated keypad for numbers is gone. Time saving
Function keys, great for macros, become a thin strip and require 2 key presses
with a <Fn> modifier key, also requires visual-contact to hit correctly.

Also one should consider that lower keycap's and switch's profile than less
possibility to stumble with fingers by keycaps. The height of keycaps and
switches should not be tall or small, but in balance.

Do not use just one hand to type a <Control>+key combo. Use one hand to
press <Ctrl>, use the other hand to press the letter key. Using a single
hand to press <Ctrl>+key combo means your hand is shaped into spider legs,
thus putting stress on it when done repeatedly. This is also why it is
important to chose a keyboard with large <Ctrl> keys positioned on both
sides of the keyboard.

When in a active coding/writing session, perhaps more than 50% of the time
your hands are actually not typing. You constantly take a pause to read or
think. This pause can be 1 second to 10 seconds or more. However, for many
people, their hands are still tensed up during these times, ready to type.
It's a good habit to remove your hands from the keyboard or mouse when you are
not pressing keys, even if the duration is just few seconds. Remind yourself
to check your hands when you are not actually in action of typing or using the
mouse. See if your hand is completely relaxed.

Always choose ``pc104'' ANSI keyboards if custom ones are not available for
proper experience with <Hyper_RL> keys usage.


# Notes for keyboard initial script.
------------------------------------

The main idea is to switch from modern keyboards mapping to kind of
``Symbolics'' keyboard, ``Space-Cadet'' keyboard or NeXT Computer keyboard,
motivated by intention to use more convenient mappings in such programs as
`nvim' (successor of `vim', weach is successor of `vi'), `emacs' or even system
wide. Instead of this bottom row:
               <Control><Super><Alt><Space><Alt><Super><Control>
use that:
        <Hyper><Alt><Super><Control><Space><Control><Super><Alt><Hyper>

But since there is not enough keys on modern keyboards at the bottom row to do
this, it will be useful to add to <Tab> and <Backslash> keys <Hyper> key.
Remapping <Caps Lock> to <Escape> is great yet there is higher frequency of
using <Escape> key then <Caps Lock>.  This is so true for programs like `vi' in
which one's bindings force user to press <Escape> a lot.  <Caps Lock> can be
easily remapped to <Shift> + <Shift>.

Emacs's bindings (also as Vi's) historically are designed for a keyboard that
has the <Control> keys as inner keys and <Alt> keys as outer keys.  Therefore
<Control> and <Alt> keys can be swapped on modern keyboards for the convenience
reasons in such programs with heavy <Control> key use. Nevertheless neither
<Control> or <Alt> keys are placed in the bottom corners all have to be pressed
sidewise with a pinky or with a palm in case of conjunction with <Shift> key to
prevent pain in the pinkies because of excessive stretching.
Keyboards with low profile key caps like on a laptop keyboard are inappropriate
for that particular task because of a inappropriate human hand's structure to
push these key caps with side of a palm easily.  One can try to squeeze hardly
his/her pinky to push these keys by pinkie's knuckle to mitigate this.  But
that's not easy because of a big size of a knuckle compared to a small size of
the key cap on a laptop keyboard and needs a lot of practice to reach good
accuracy.

As a rule of thumb the best practice is to research for possibility to add a
specific binding before adding any custom bindings in conjunction with <Alt>,
<Control>, <Shift>, <Escape>, <Space> since such combinations are already used
by a plenty of other programs.

Hints:  <Hyper> + <Alt> + <any key> can be pressed with to hands. <Hyper> and
        <Alt> keys with a thumb for an <Alt> key and middle finger for a <Hyper>
        key, and <any key> with the opposite hand.
        Don't use right hand combination of <Control>, <Alt> and <Hyper> keys to
        avoid unneeded Xorg exiting, since <Hyper> key can be missed with
        <BackSpace> key.
        Do not use <Shift> key for bindings simultaneously with characters
        accessed via <Shift> key.

`xcape' is a great tool to make modifier keys to act as a standalone key. For
example, <Alt> key can act as <ISO_Level3_Latch>, <Control> key as <Multi_key>.
`xcape' have to be launched every time exactly after restart of `xmodmap' or
another keysym modifier program.  `sxhkd' have to be launched right after when
all key mapping settings was applied. Keep it done via `bspwm' launch scripts.

`xmodmap' is not directly related to XKB; it uses different (pre-XKB) ideas on
how keycodes are processed within X.  In particular, it lacks the notion of
groups and types, so trying to set more than one keysym per key is not likely to
work.  In general, except for the simplest modifications of keymaps or pointer
button mappings, `xkbcomp' should be used instead.

For proper work define key mapping via `xkbcomp' because of incompatibility
`xmodmap' and `setxkbmap' or apply `xmodmap' modifications only after running
`setxkbmap'.  Use `xev' to find out needed key code.  It's hard to use *keysym*
for evaluation to unify settings. Use *keycode* instead.

Check ``/usr/share/X11/xkb/symbols/'' for needed layouts.
Check ``/usr/share/X11/xkb/rules/evdev.lst'' for `setxkbmap' options.
`setxbmap' have to be evaluated before `xcape' and `sxhkd'  Do not change
the sequence!!! At first run `setxkbmap'  then `xmodmap'  and only then run
`xcape' 

<compose> + <letter> + <sign> usage examples:
  o+[']=ó, o+[.]=ȯ, o+[,]=ǫ, o+["]=ö, a+[e]=æ, a+[*]=å, a+[^]=â, a+[(]=ă,
  o+[~]=õ, o+[`]=ò, o+[/]=ø, o+[>]=ô, o+[o]=°, c+[<]=č, etc.
 Path: /usr/share/X11/locale/en_US.UTF-8/Compose

# Some guide lines for keyboard user interface design.
------------------------------------------------------

https://developer.apple.com/design/human-interface-guidelines/designing-for-macos
https://learn.microsoft.com/en-us/previous-versions/windows/desktop/dnacc/guidelines-for-keyboard-user-interface-design?redirectedfrom=MSDN#atg_keyboardshortcuts_creating_shortcut_keys_and_access_keys
https://en.wikipedia.org/wiki/Modifier_key

Use the following guidelines for designing shortcut keys:
* Assign simple and consistent key combinations to remember.
* Make shortcut keys customizable.
* Use a shortcut with the <Control> key for actions that represent a large-scale
  effect within program, such as <Control> + S to save current file.
* Use the <Shift> + key combination for actions that extend or complement the
  actions of the standard shortcut key.  For example, the <Alt> + <Tab> shortcut
  key displays the primary window of a running program in Windows OS.
  Alternatively, the <Shift> + <Alt> + <Tab> key combination allows you to
  navigate backward through currently running programs that have been previously
  accessed.
* Use the <Space> key as the default action of a control, such as for
  pressing a button control or toggling the status of a check box control.  This
  is similar to clicking the primary mouse button.
* Use the <Enter> key for the default action of a dialog box, if available.
* Use the <Escape> key to stop or cancel an operation.
* Avoid creating a shortcut by adding a <Shift> key to an existing shortcut,
  unless the shortcuts are related.
* Avoid modified or case-sensitive letters for shortcuts.
* Avoid using the following characters for shortcut keys: @ {} [] \ ~ | ^ ' < >
* Avoid <Alt> + letter combinations because they may conflict with access keys.
  In addition, the system uses many specific key combinations for specialized
  input; for example, <Alt> + ~ invokes an input editor for the Japanese
  language in Windows OS.
* Avoid <Ctrl> + <Alt> combinations because the system may interpret this
  combination in some language versions as an <AltGr> key, which generates
  alphanumeric characters.
* Avoid assigning combinations that are reserved or defined by the system or are
  commonly used by other programs.
* Avoid use of <Super> and <Hyper> key as a modifier key for non-system-level
  functions.
* Always respect the system-reserved keyboard shortcuts in your program so that
  users aren’t confused when the shortcuts they know work differently in your
  program.
* List multiple modifier keys in the correct order.
* Identify a key with two characters by the lower character, unless Shift is
  part of the shortcut.
* <Control>/<Super> + <Alt> + <Shift>, <Control> + <Alt> + <Hyper> or
  <Super> + <Alt> + <Hyper> are the longest convenient combinations can be
  pressed with one hand.
* Beware of that <Super> key can be only one key on a keyboard on any side,
  right or left. Thererfore bindings can be convenient on one keyboard and don't
  on the other one.
* Beware of that bindgins with <Alt> key mostly used on a system level with
  conjuction with <F.> keys.

# General notes to user environment.
-----------------------------------

Please don't use any compositing utility since it adds lots of latency.
Compositing window managers is that they seem to also enforce vertical
synchronization (V-Sync). This means we need to wait until a picture is
displayed on screen before we can start drawing the next one.
https://www.lofibucket.com/articles/dwm_latency.html
