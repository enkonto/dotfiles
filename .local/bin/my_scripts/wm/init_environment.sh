#!/usr/bin/env dash


_loadGoods() {
    . my_functions
    . my_aliases
}


scriptName=$(basename $0)
scriptDir=$(dirname $0)


_helpMsg() {
    cat <<- EOF
		Options are:
		  -w: Set wallpapers
		  -h: Show this message
	EOF
}


_errorMsg() {
    scriptName=$(basename $0)

    {
        printf '%s\n' "[$(date +'%Y-%m-%dT%H:%M:%S.%z')], ${scriptName}:"
        for arg in $*;do
            printf '\t%s\n' "${arg}" >&2
        done
    } >&2

    exit 1
}


_setDisplay() {
    # Inactivity period for standby mode should be more or less then the sum of
    # screensaver's timers but less then suspend and off modes to prevent locker
    # been disable to work. To prevent screen blinking set standby timer less or
    # equal to screensaver's one.

    echo "Setting display..."
    xset s 300 60
    xset s noblank
    xset dpms 600 1200 1200
}


_setScLocker() {
    # Don't forget to edit ``xorg.conf'' to mitigate the backdoors.

    _setIgnrSlp() {
        _checkSysD && echo "--ignore-sleep"
    }


    echo "Setting screen locker..."

    _killEmAll xss-lock xscreensaver; sleep 0.1

    if _checkDep xss-lock xsecurelock; then
        xss-lock $(_setIgnrSlp) -q -l -- xseclock.sh &
        #xss-lock --ignore-sleep -q -l -- sh -c "xseclock.sh" &
        #xss-lock --ignore-sleep -q -l -- sh -c "xsecurelock || kill -9 -1" &
    elif _checkDep xss-lock xscreensaver; then
        xscreensaver --no-splash &
        xss-lock $(_setIgnrSlp) -q -l -- xseclock.sh &
        #xss-lock $(_setIgnrSlp) -q -l -- xscreensaver-command --suspend &
    elif _checkDep xscreensaver; then
        xscreensaver --no-splash &
    fi
}


_procXres() {
    echo "Merging Xresources..."
    xrdb -merge ${XDG_CONFIG_HOME:-$HOME}/.Xresources
}


_loadUrxvtd() {
    echo "Loading URXVTd..."

    if _checkDep urxvt256cd && ! _checkLoad urxvt256cd urxvtd; then
        #urxvt256cd -q -f -o -m &
        urxvt256cd -q -o &
    elif _checkDep urxvtd && ! _checkLoad urxvt256cd urxvtd; then
        #urxvtd -q -f -o -m &
        urxvtd -q -o &
    fi
}


_genWpStr() {
        wpStr=""
        xrandr --listactivemonitors \
            | awk -P 'NR!=1 {gsub("(/[0-9]*x|/[0-9+]*)"," ",$3); print $3" "$4}' \
            | while read -r width height output; do
                #echo "width: '$width'\nheight: '$height'\noutput: '$output'"
                if [ "${width}" -ge "${height}" ]; then
                    wpStr="${wpStr} --output ${output} \
                        --zoom ${wpPath}/horisontal-wallpaper"
                else
                    wpStr="${wpStr} --output ${output} \
                        --zoom ${wpPath}/vertical-wallpaper"
                fi
                printf '%s' "${wpStr}"
            done
}


_setWallpapers() {
    # TODO: For Wayland compatibility redo with `wpaperd' or `swaybg' option.

    # Provide symlinks to ``horisontal-wallpaper'', ``vertical-wallpaper''.
    # Only horisontal:
    #   - anime-girl-cat-raining-4k-4w.jpg
    #   - vinyl-record-spinning-e6.jpg
    #   - wallpaper-before-sunrise.png
    # Both horisontal and vertical pair:
    #   - wallpaper-horisontal.jpg, wallpaper-vertical.jpg

    [ -h "${wpPath}/horisontal-wallpaper" ] \
        && [ -h "${wpPath}/vertical-wallpaper" ] \
        && {
            echo "Setting wallpapers..."
            _killEmAll xwallpaper
            xwallpaper --daemon $(_genWpStr)
        }
}


_hideCursor() {
    # `xbanish' hides cursor automatically when pressing any keyboard button,
    # except <Shift>, <CapsLock>, <Control>, <Alt>, etc.  Launching `xbanish'
    # without ``&'' cause freezes. `xbanish' is sluggish, though.  One should
    # use `unclutter-xfixes' instead.  But `unclutter-xfixes' works incorrectly
    # with `xmonad' and `i3' unfortunately.  Use ``updatePointer'' and/or
    # ``Hhp'' from ``xmonad.hs'' instead of `xbanish' and/or `unclutter-xfixes'.
    ! _checkLoad xmonad i3 && if _checkDep unclutter 1>/dev/null 2>&1; then
            _killEmAll unclutter
            echo "Loading \`unclutter'..."
            unclutter   --timeout 2 \
                        --jitter 10 \
                        --exclude-root \
                        --ignore-scrolling \
                        --hide-on-touch \
                        --start-hidden &
        elif ! _checkLoad unclutter && _checkDep xbanish 1>/dev/null 2>&1; then
            _killEmAll xbanish
            echo "Loading \`xbanish'..."
            xbanish -i shift lock control mod1 mod2 mod3 mod4 mod5 &
        fi
}


_loadSshA() {
    echo "Loading SSH-agent..."
    _checkLoad ssh-agent || eval $(ssh-agent -s)
}


_loadStray() {
    echo "Loading bar..."
    _checkLoad polybar || { _checkLoad xmobar && stalonetray & }
}


_loadDwmBlks() {
    # Loads `dwmblocks' for `dwm'.
    # Should be loaded as the last one to set all variables it blocks properly.

    echo "Loading dwmblocks..."
    if _checkLoad dwm && _checkDep dwmblocks && ! _checkLoad dwmblocks; then
        dwmblocks &
    fi
}


_main() {
    _setDisplay
    _setScLocker
    _procXres
    _setWallpapers
    _hideCursor
    _loadUrxvtd
    _loadEmacsDmn
    _loadSshA

    # udiskie &
    # dunst &

    _loadStray
    _loadDwmBlks
}


_loadGoods

wpPath="${XDG_CONFIG_HOME:-$HOME}/.local/share/wallpapers"  # Wallpapers path.

# Set arguments for this script.
while getopts "wh" options; do
    case "${options}" in
        w) _setWallpapers; exit 0 ;;
        h) _helpMsg; exit 0 ;;
        *) _helpMsg; exit 1 ;;
    esac
done

_main
