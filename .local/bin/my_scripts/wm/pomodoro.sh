#!/usr/bin/env dash

# YAPTfDWMB - yet another pomodoro timer for DWM blocks.

# This script shows the state of the `blocks_pomodoro.sh' via pipe.
# All management should be done via `pomodoro.sh', writing current data to
# `blocks_pomodoro.pipe' for the next parsing by `blocks_pomodoro.sh'.
#
#   - for clock
# For pomodoro:
#                               u


. my_functions


fifoPipePath="${HOME}/tmp/blocks_pomodoro.pipe"
scriptName=$(basename $0)


_writeStr() {
    [ -p "${fifoPipePath}" ] && rm "${fifoPipePath}"
    while [ -p "${fifoPipePath}" ]; do sleep 0.1; done
    [ -p "${fifoPipePath}" ] || mkfifo "${fifoPipePath}"
    [ -p "${fifoPipePath}" ] && echo "$*" >> "${fifoPipePath}" &
}


_setPomStr() {
    case "$@" in
        pause)
            pomodoro_str="$(_getPomStr) pause"
            ;;
        resume)
            pomodoro_str=$(_getPomStr | sed 's/ pause//')
            ;;
        *)
            # Remove the old pipe an then create a new one. Send to pipe work
            # time, rest time, date, pause/stop/start values, pause date. Send
            # the process to background.
            pomodoro_str="$*"
        ;;
    esac

    _writeStr "${pomodoro_str}"
    _sendSignal
    printf "%s\n" "$*"
}


_getPomStr() {
    [ -p "${fifoPipePath}" ] && cat < "${fifoPipePath}"
    [ -p "${fifoPipePath}" ] && rm "${fifoPipePath}"
}


_sendSignal() {
    # Send signal to `dwmblocks' to renew `blocks_pomodoro.sh' state.
    sleep 0.5; pkill -RTMIN+10 dwmblocks;
}


_pomNotificate() {
    # Beep one's speaker.
    _checkDep beep && beep -f 1000 -r 2 -n -r 5 -l 10 -n &

    # Notification via `herbe', `libnotify' for GNOME or via OSA for MacOS.
    if _checkDep herbe; then
        herbe "$@"
    elif _checkDep notify-send; then
        notify-send \
            -u critical \
            -i appointment \
            -t 2000 "$@" \
    #elif _checkDep osascript; then
    #    printf '%s\n' "tell app \"System Events\" to display dialog \"$@\"" \
    #        | osascript
    else
        return 1
    fi

    # Just print right to the TTY.
    printf '%s\n' "$@"
}


_callErrorMsg() {
    printf '%b' "Usage:\n\tpomodoro.sh [ pause | start | work_time rest_time " \
        "[\"startmessage\"] [\"endmessage\"]\n"
    exit 1
}


_pomodoroCall() {
    # Load default value if it's not given, minutes.
    load_settings() {
        if [ -z "${work_time}" ] || [ -z "${rest_time}" ]; then
            work_time=43
            rest_time=17
        fi
        [ -z "${startmessage}" ] \
            && startmessage=" You have ${work_time} minutes left."
        [ -z "${endmessage}" ] \
            && endmessage=" Take a short break for ${rest_time}."
    }

    work_time="$1"
    rest_time="$2"
    startmessage="$3"
    endmessage="$4"
    notf_time=${notf_time:=3}   # Set time for notification before the next
                                # state will change, minutes.

    case $# in
        0) load_settings ;;
        2|3|4)
            # Check if correct arguments was given.
            if [ "${notf_time}" -gt 0 ] \
                && [ "${work_time}" -gt "${notf_time}" ] \
                && [ "${rest_time}" -gt "${notf_time}" ]; then
                    load_settings
            else
                _callErrorMsg
            fi
            ;;
        *) _callErrorMsg ;;
    esac

    # If there are more then 1 process are running then kill all old ones and
    # leave a current one.
    if [ $(pgrep -x "${scriptName}" | wc -l) -gt 1 ]; then
        kill $(pgrep -x "${scriptName}" | sed "s/$$//")
    fi

    while true; do
        _pomNotificate "Pomodoro started!" "${startmessage}"
        _setPomStr "${work_time}" "${rest_time}" work
        sleep $(((${work_time} - ${notf_time}) * 60))
        _pomNotificate "${notf_time} minutes left untill your next break time!"
        sleep $((${notf_time} * 60))
        _pomNotificate "Pomodoro ended!" "${endmessage}"
        _setPomStr "${work_time}" "${rest_time}" rest
        sleep $((${rest_time} * 60))
    done

}


case "$@" in
    "stop")
        printf '%s\n' "Stopping pomodoro processes..."
        pkill -x "${scriptName}"

        if [ "$?" = "0" ]; then
            _pomNotificate "  ${scriptName} have been stopped!"
        else
            printf '%s\n' \
                "Couldn't stop any pomodoro process (no pomodoro running?)"
        fi

        exit
        ;;
    "pause")
        printf '%s\n' "Pausing pomodoro process..."
        pkill -STOP -f "${scriptName} *[1-9]"

        if [ "$?" = "0" ]; then
            _pomNotificate "  ${scriptName} have been paused!" \
                "To make them continue, run \"${scriptName} start\""
            _setPomStr pause
        else
            printf '%s\n' \
                "Couldn't pause any pomodoro process (no pomodoro running?)"
        fi

        exit
        ;;
    "resume")
        printf '%s\n' "Resuming pomodoro process..."
        pkill -CONT -f "${scriptName} *[1-9]"

        if [ "$?" = "0" ]; then
            _pomNotificate "  ${scriptName} have been resumed!" \
                "The counters will continue as if nothing happened"
            _setPomStr resume
        else
            printf '%s\n' "Couldn't resume any pomodoro process (no pomodoro found?)"
        fi

        exit
        ;;
    *)
        #_pomodoroCall "$@"
        printf '%s\n' "${scriptName}"
        ;;
esac
