#!/usr/bin/env dash


_loadGoods() {
    . my_functions
    . my_aliases
}


scriptName=$(basename $0)
scriptDir=$(dirname $0)
xkbPath="${HOME}/.config/xkb/"
# extKbMap="${HOME}/.Xmodmap.pc104"
# intKbMap="${HOME}/.Xmodmap.thinkpad"
kbMapPath="${HOME}/.Xmodmap"
kbModel="pc104" # Set default value.


_helpMsg() {
    cat <<- EOF
		Options are:
		  -q: Set to QWERTY mapping
		  -h: Show this message
	EOF
}


_errorMsg() {
    scriptName=$(basename $0)

    {
        printf '%s\n' "[$(date +'%Y-%m-%dT%H:%M:%S.%z')], ${scriptName}:"
        for arg in $*;do
            printf '\t%s\n' "${arg}" >&2
        done
    } >&2

    exit 1
}


_setXkb() {
    # Set keyboard model, layout and modify mapping instead `_setLayouts' and
    # `_setMapping'.

    setxkbmap -option # Reset all options.
    #setxkbmap \
    #    -I${xkbPath} \
    #    -layout "$@" \
    #    -model "${kbModel}" \
    #    -rules "evdev" \
    #    -option "keypad:pointerkeys" \
    #    -option "terminate:ctrl_alt_bksp" \
    #    -option "mod_led:compose" \
    #    -symbols "+custom(hypers)" \
    #    -synch \
    #    -v 10
    xkbcomp \
        -w10 \
        -I/home/ds/.config/xkb \
        /home/ds/.config/xkb/keymap/thinkpad.xkb
}


_setLayouts() {
    # Set keyboard model, layout and modify mapping.
    setxkbmap -option && \
        setxkbmap \
            -layout "$@" \
            -model "${kbModel}" \
            -rules "evdev" \
            -option "grp:win_space_toggle" \
            -option "keypad:pointerkeys" \
            -option "terminate:ctrl_alt_bksp" \
            -synch \
            -v 10
}


_setMapping() {
    printf '%s\n' "Setting mapping for the ${kbMapPath} keyboard..."
    [ -f "${kbMapPath}" ] && xmodmap "${kbMapPath}"
}


_setXcape() {
    # Run `xcape' to enable double action for one key. Don't forget to store a
    # secondary key in unused key codes name space, i.e.
    # keycode 252 = Tab ISO_Left_Tab
    # keycode 253 = backslash bar
        # -t 220 \
        # -e "Control_L=Tab;Control_R=backslash;Alt_L=ISO_Level3_Latch;Alt_R=ISO_Level3_Latch;Super_L=Multi_key;Super_R=Multi_key" \
    # TODO: replace `xcape' with `xremap' or `evcape' for Wayland compatibility.
    xcape \
        -t 200 \
        -e "Control_L=Escape;Control_R=Return;Alt_L=ISO_Level3_Latch;Alt_R=ISO_Level3_Latch;Super_L=Tab;Super_R=backslash;Shift_L=Delete;Shift_R=BackSpace" \
        &
}


_main() {
    _checkLoad Xorg && case "$@" in
        "")
            printf '%s\n' \
                "Setting \"Coder Colemak\" and \"Машинопись\" keymaps..."

            _killEmAll xcape && {
                # NOTE: keep the order of execution.

                # _setXkb "us(coder_colemak_dh),ru(typewriter)"
                # _setLayouts "us(coder_dvorak),ru"
                _setLayouts "us(coder_colemak),ru"
                # BUG:  AE12 acts like AD11 with active ru(typewriter) layout
                # _setLayouts "us(coder_colemak),ru(typewriter)"
                _setMapping
                _setXcape
            }
            ;;

        "guest")
            printf '%s\n' \
                "Setting \"QWERTY\" and \"ЙЦУКЕН\" keymaps..."

            _killEmAll xcape && _setLayouts "us,ru"
            ;;

        *)
            _errorMsg "Wrong argument. Keyboard map have not been set!"
            ;;
    esac


    exit 0
}


_loadGoods

xinput --list --name-only   | grep -q -i 'ThinkPad'             && kbModel="thinkpad"
lsusb                       | grep -q -i 'COUGAR.*ATTACK.*X3'   && kbModel="pc104"
printf "%s\n" "Keyboard Model: ${kbModel}"

# Set arguments for this script.
while getopts "qh" options; do
    case "${options}" in
        q) _main guest;;
        h) _helpMsg; exit 0 ;;
        *) _helpMsg; exit 1 ;;
    esac
done

_main
