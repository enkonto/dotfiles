#!/usr/bin/env dash

_loadGoods() {
    . my_functions
    . my_aliases
}


_loadXseclock() {
    # Check ``/usr/share/doc/xsecurelock/README.md'' for instructions.

    export XSECURELOCK_SAVER=saver_blank
    export XSECURELOCK_AUTH_CURSOR_BLINK=0
    export XSECURELOCK_AUTH_TIMEOUT=30
    export XSECURELOCK_BLANK_DPMS_STATE=standby
    export XSECURELOCK_BLANK_TIMEOUT=570
    export XSECURELOCK_DISCARD_FIRST_KEYPRESS=1
    export XSECURELOCK_FONT='monospace:size=10'
    export XSECURELOCK_NO_COMPOSITE=1
    export XSECURELOCK_COMPOSITE_OBSCURER=0
    export XSECURELOCK_PASSWORD_PROMPT=hidden
    export XSECURELOCK_SHOW_HOSTNAME=0
    export XSECURELOCK_SHOW_USERNAME=0
    export XSECURELOCK_SINGLE_AUTH_WINDOW=1

    xsecurelock || kill -9 -1
}


_loadXscsvr() {
    xscreensaver-command --suspend &
}


_loadGoods
_checkLoad xss-lock && _checkDep xsecurelock && { _loadXseclock; exit 0; }
_checkLoad xss-lock && _checkDep xscreensaver && { _loadXscsvr; exit 0; }
_checkLoad xscreensaver && { _loadXscsvr; exit 0; }
