#!/usr/bin/env dash

# TODO: Add temperature and load levels for nVidia and AMD GPUs.

#if ! gputemp=$(nvidia-smi --format=nounits,csv,noheader --query-gpu=temperature.gpu | xargs echo); then
#    gputemp=0
#fi
#if [ "$gputemp" -gt 0 ]; then
#    echo "$gputemp°C"
#else
#    echo "$no nvidia driver installed"
#fi

_drawTemp() {
    temperature="$@"
    [ -z "${temperature}" ] && return 1

    case "${temperature}" in
        [1-2][0-9])             temp_icon="" ;;
        [3-4][0-9])             temp_icon="" ;;
        5[0-9])                 temp_icon="" ;;
        6[0-9])                 temp_icon="" ;;
        7[0-9])                 temp_icon="" ;;
        8[0-9])                 temp_icon="" ;;
        9[0-9])                 temp_icon="" ;;
        *)                      temp_icon="" ;;
    esac

    printf '%s' "${temp_icon}"
}

_showTopLoad() {
    top -ibn1 -w 200 \
        | awk -c ' \
            BEGIN { FS="," } \
            $0 ~ /^%Cpu\(s\)/ { cpu_load = int($4) }
            $0 ~ /^MiB Mem/ { gsub(/[a-zA-Z :]/,""); \
                ram_used = int($3); ram_total = int($1) }
            $0 ~ /^MiB Swap/ { gsub(/used\./,"used,"); gsub(/[a-zA-Z :]/,""); \
                swap_used = int($3); swap_total = int($1) }
            END { printf "%d%%%d%%%d%%", \
                (( 100 - cpu_load )), \
                (( ram_used / ram_total * 100 )), \
                (( swap_used / swap_total * 100 )) }'
}


_cpuTemp(){
    # NOTE: Switch with commenting according line.
    cpu_temp=$(sed 's/...$//' /sys/class/thermal/thermal_zone*/temp)    # Intel.
    #cpu_temp=$(sed 's/...$//' /sys/class/hwmon/hwmon2/temp*_input)      # AMD.
    max_temp=0

    for temp in ${cpu_temp}; do
        if [ "${temp}" -gt "${max_temp}" ]; then max_temp="${temp}"; fi
    done

    printf "%s" "${max_temp}"
}


#gpu_vendor=$(_checkDep glxinfo && glxinfo -B | \
#    awk '{ if ($1=="Vendor:") print $2 }')
#gpu_vendor=$(_checkDep lspci -m | \
#    awk -c 'BEGIN { FS="\"" } \
#    $2=="VGA compatible controller" { printf "%s\n", $4 }')
#if [ "$gpu_vendor" = "Intel Corporation" ]; then
#    gpu_temp="0"
#fi

    #"$(_drawTemp "${_gpu_temp}")" \
printf '%b' \
    "$(_drawTemp "$(_cpuTemp)")" \
    "$(_showTopLoad)" \
    "\n"
