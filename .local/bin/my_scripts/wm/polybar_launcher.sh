#!/usr/bin/env dash


_loadGoods() {
    . my_functions
    . my_aliases
}


scriptName=$(basename $0)
scriptDir=$(dirname $0)


_helpMsg() {
    cat <<- EOF
		Options are:
		  -k: kill \`polybar'
		  -h: Show this message"
		EOF
}


# Launches an instance of polybar
_runPolybar() {
    configPath="${HOME}/.config/polybar/config.ini"

    _checkDep polybar || exit 1

    printf "%s\n" "Launching \`polybar'..."

    _checkLoad i3 && WM_MODULE="xworkspaces"
    _checkLoad bspwm && WM_MODULE="bspwm"
    _checkLoad herbstluftwm && WM_MODULE="herbstluftwm"
    echo "WM_MODULE=${WM_MODULE}"

    for m in $(polybar --list-monitors | cut -d":" -f1); do
        echo "MONITOR=${m}"
        MONITOR=${m} WM_MODULE=${WM_MODULE} polybar -r -q -c ${configPath} main &
    done

    printf "%s\n" "\`polybar' have been launched."
    #notify-send -i notify_icon "Polybar have been reloaded."
}


_killPolybar() {
    #polybar-msg cmd restart
    printf "%s\n" "Stopping \`polybar'..."
    _killEmAll polybar
    printf "%s\n" "\`polybar' have been stopped."
    #notify-send -i notify_icon "Polybar have been stoped."
}


_main() {
        _killPolybar
        _runPolybar
}


_loadGoods

# Set arguments for this script.
while getopts "kh" options; do
    case "${options}" in
        k) _killPolybar; exit 0 ;;
        h) _helpMsg; exit 0 ;;
        *) _helpMsg; exit 1 ;;
    esac
done

_main
