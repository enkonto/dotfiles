#!/usr/bin/env dash


unread="$(find ~/.local/share/mail/*/INBOX/new/* -type f | wc -l 2>/dev/null)"

[ "${unread}" -gt "0" ] \
    && printf '%s\n' "${unread}"
