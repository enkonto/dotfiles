#!/usr/bin/env dash

#set -eu


scriptName=$(basename $0)
scriptDir=$(dirname $(readlink -f $0))
sleepDelay=1    # By default systemd-logind allows 5 seconds maximum of delay.

if [ -n $USER ]; then
    userId=$USER
elif [ -n $USERNAME ]; then
    userId=$USERNAME
else
    userId=$(id -un)
fi

# Make sure we have a properly owned cache file.
xradnrCache=/home/${userId}/.cache/${scriptName}
[ -f ${xradnrCache} ] \
    || { touch ${xradnrCache}; chown ${userId}: ${xradnrCache}; }


_notifications() {
    # notifications start|stop
    case ${@:-start} in
        stop) killall -SIGUSR1 dunst & ;;
        start) killall -SIGUSR2 dunst & ;;
    esac
}


_lockXscreensaver() {
    # If xscreensaver isn't running, initialize the daemon and wait
    # for it to start up and respond to our queries.
    # Proceed to lock screen.
    if ! xscreensaver-command --time 1>/dev/null 2>&1; then
        xscreensaver --display $DISPLAY --no-splash 1>/dev/null 2>&1 &
        while ! xscreensaver-command --time 1>/dev/null 2>&1; do sleep 0.1; done
    fi
    xscreensaver-command --lock 1>/dev/null 2>&1 && sleep 0.1
    xscreensaver-command --watch | _xscreensaverEventHandler
}


_xscreensaverEventHandler() {
    while read line
    do
        case "${line}" in
            UNBLANK*)
                pkill -x xscreensaver-command
                ;;
        esac
    done
}


_cacheXrandr() {
    xrandr -q > ${xradnrCache}
}


_xmonadRestartIfDisplayChange() {
    [ "$(cat ${xradnrCache})" == "$(xrandr -q)" ] || xmonad --restart
}


_xmonadRestartAlways() {
    xmonad --restart
}


_main() {
    _cacheXrandr
    #notifications stop
    _lockXscreensaver
    #_lockXscreensaverNew
    #_lockI3lockOld
    #_xmonadRestartIfDisplayChange # mostly for xmobar/systray refresh
    #_xmonadRestartAlways
    #_notifications start
}


_main
