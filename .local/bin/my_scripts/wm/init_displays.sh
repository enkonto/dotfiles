#!/usr/bin/env dash


# [ "lostsoul", "imp", "demon" , "revenant", "baron", "tyrant", "mancubus" ]


_loadGoods() {
    . my_functions
    . my_aliases
}


scriptName="${0##*/}"   # scriptName=$(basename $0)
scriptDir="${0%/*}"     # scriptDir=$(dirname $0)


_helpMsg() {
    cat <<- EOF
		If arguments wasn't specified, then default mode will be activated.
		Options are:
		  -m: switch to mode: internal, single, dual, share, add_int
		  -o: turn off display: internal, single, dual, share
		  -h: Show this message
	EOF
}


_errorMsg() {
    scriptName=$(basename $0)

    {
        printf '%s\n' "[$(date +'%Y-%m-%dT%H:%M:%S.%z')], ${scriptName}:"
        for arg in $*;do
            printf '\t%s\n' "${arg}" >&2
        done
    } >&2

    exit 1
}


_setBackLight() {
    _checkDep xbacklight || return 1

    xbacklight -set $*
    # Do some work for `dwmblocks' to renew status bar in `dwm'.
    _checkLoad dwmblocks && {
        sleep 0.5; pkill -RTMIN+4 dwmblocks; pkill -RTMIN+5 dwmblocks
    }
}


_checkLptpPanel() {
    internalOutList="eDP1 LVDS-1"
    actMonList="$(xrandr --listactivemonitors)"

    for panel in $(echo $internalOutList); do
        echo "${actMonList}" | grep -i '$panel' && return 0
    done

    return 1
}


_checkAcOn() {
    [ $(cat /sys/class/power_supply/AC/online) -eq 1 ] && return 0
}


_initBackLight() {

    _checkLptpPanel || return 1
    _checkDep xbacklight || return 1

    # Set back light level in dependency by batteries charge level and
    # if AC plugged in or not.
    if _checkAcOn && [ $(printf "%.0f\n" $(xbacklight -get)) -lt 70 ]; then
            _setBackLight 70
    else
            _setBackLight 30
    fi
}


_checkOutput() {
    argCount=0

    for i_arg in $*; do
        argCount=$((argCount + 1))
    done

    [ "${argCount}" != "$#" ] \
        && _errorMsg "An output wasn't specified for this mode."
}


_getResolution() {
    xrandr --listactivemonitors \
        | grep -i "$@" \
        | cut -d" " -f4 \
        | sed 's/\/[[:digit:]]\+//g; s/+[[:digit:]]\+//g'
}


_turnOff() {

    printf '%s\n' "Turning off \"$@\" display..."


    case "$@" in
        internal)
            offed="${internalOut}"
            ;;
        single)
            offed="${singleOut}"
            ;;
        dual)
            offed="${dualOut}"
            ;;
        share)
            offed="${sharedOut}"
            ;;
        *)
            _errorMsg "Invalid display was specified."
            ;;
    esac
    xrandr --output "${offed}" --off


    exit 0
}


_defOutputs() {
    if [ -f /etc/hostname ]; then
        machineName=$(cat /etc/hostname)
    elif [ -n "${HOST}" ]; then
        machineName="${HOST}"
    elif [ -n "${HOSTNAME}" ]; then
        machineName="${HOSTNAME}"
    else
        _errorMsg "Failed to obtain hostname."
    fi


    case ${machineName} in

        # Settings for desktop.
        tyrant|revenant)
            internalOut=""
            singleOut="HDMI-0"
            dualOut=""
            sharedOut="DP-1"
            defaultOut="single"
            ;;

        # Settings for laptop, models like T440p, T470 and T480.
        spectre|pinky)
            #internalOut="LVDS-1"  # Old TN panel.
            internalOut="eDP1"     # Modern IPS panel.
            singleOut="DP2-1"
            dualOut="DP2-2"
            sharedOut="HDMI2"
            defaultOut="internal"

            _initBackLight
            ;;

        *) _errorMsg "Any appropriate host name wasn't specified at the host name list." ;;

    esac
}


_main() {

    printf '%s\n' "Setting \"$@\" display mode..."

    _defOutputs

    # Initial settings. If any output wasn't found, then it will be ignored.
    case "$@" in

        internal)
            _checkOutput "${internal}"
            xrandr \
                --output "${sharedOut}" \
                --off \
                --output "${singleOut}" \
                --off \
                --output "${dualOut}" \
                --off \
                --output "${internalOut}" \
                --auto \
                --primary
            ;;

        single)
            _checkOutput "${singleOut}"
            xrandr \
                --output "${internalOut}" \
                --off \
                --output "${sharedOut}" \
                --off \
                --output "${dualOut}" \
                --off \
                --output "${singleOut}" \
                --auto \
                --primary
            ;;

        dual)
            _checkOutput "${singleOut}" "${dualOut}"
            xrandr \
                --output "${internalOut}" \
                --off \
                --output "${sharedOut}" \
                --off \
                --output "${singleOut}" \
                --auto \
                --primary
                --output "${dualOut}" \
                --auto \
                --rotate left \
                --right-of "${singleOut}" \
                --pos 0x-600
            ;;

        share)
            _checkOutput "${sharedOut}"
            xrandr \
                --output "${sharedOut}" \
                --auto \
                --same-as "${singleOut}" \
                --scale-from "$(_getResolution "${singleOut}")"
            ;;

        add_int)
            _checkOutput "${internalOut}"
            xrandr \
                --output "${internalOut}" \
                --auto \
                --left-of "${singleOut}"
            ;;

        "") _main "${defaultOut}" ;;

        *) _errorMsg "Invalid mode was specified." ;;

    esac


    exit 0
}


_loadGoods

# Set arguments for this script.
while getopts "m:o:h" options; do
    case "${options}" in
        m) _main "$OPTARG" ;;
        o) _turnOff "$OPTARG"; exit 0 ;;
        h) _helpMsg; exit 0 ;;
        *) _helpMsg; exit 1 ;;
    esac
done

_main
