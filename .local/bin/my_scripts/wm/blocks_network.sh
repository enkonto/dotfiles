#!/usr/bin/env dash

# This script shows status of internet and vpn connections.
# Install whois and bind-utils for flawless work.

# Dependencies: `dig'(bind-utils), `whois'

# TODO: draw:
# if wi-fi is connected    (signal)
# if inthernet connection .
# no connection .
# only ethernet y
# for tor 
# for DOH 
# for vpn  (country)
# for cell     


_pingFunc() {
    ip_to_ping="8.8.8.8"
    ping_requests="1"   # 1 - for speed, 5 - for precision.

    ping -c "${ping_requests}" "${ip_to_ping}" \
        | awk -c ' \
            BEGIN { FS="/"; status="disconnected" } \
            $1=="rtt min" { printf "%0.f%s\n", $5, "ms"; status="connected" } \
            END { if ( status=="disconnected" ) printf "%s\n", "" }'
}


_vpnFunc() {
    vpn_interface="tun0"

    ip link show "${vpn_interface}" \
        | awk -c -v vpn_interface="${vpn_interface}" ' \
            BEGIN { FS=": "; status="" } \
            { if ( $2==vpn_interface ) status="" } \
            END { printf "%s\n", status }'
}


_countryFunc() {
    whois $(dig +short myip.opendns.com @resolver1.opendns.com) \
        | awk -c '$1=="country:" { printf "%s\n", $2 }'
}

case "$@" in
    show)
        printf '%s/%s/%s\n' "$(_vpnFunc)" "$(_countryFunc)" "$(_pingFunc)"
        ;;
    "")
        printf '%s\n' ""
        ;;
    *)
        printf '%s\n' "Wrong arguements!"
        ;;
esac
