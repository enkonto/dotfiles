#!/usr/bin/env dash

# Description.
# =============

# Set mice, trackballs, trackpoints, trackpads via `xinput'.
# Toggle touchpad via `synaptics' or `xinput'.

# Dependencies: `xbanish', `synclient', `xinput', `unclutter-xfixes', `ksh'


# Specifying settings list.
# ~~~~~~~~~~~~~~~~~~~~~~~~~

# Comment starts with a octoterp(number sign). Device name line starts with a
# "device". Property line starts with a single quote. Everything else isn't
# legit. Ba aware of that device name is a pattern. Thus there is can be some
# ambiguity if device name wasn't specified precisely.


_loadGoods() {
    . my_functions
    . my_aliases
}

scriptName="${0##*/}"   # scriptName=$(basename $0)
scriptDir="${0%/*}"     # scriptDir=$(dirname $0)


_helpMsg() {
    cat <<- EOF
		Options are:
		  -t: Toggle touch pad
		  -h: Show this message
	EOF
}


_errorMsg() {
    scriptName=$(basename $0)

    {
        printf '%s\n' "[$(date +'%Y-%m-%dT%H:%M:%S.%z')], ${scriptName}:"
        for arg in $*;do
            printf '\t%s\n' "${arg}" >&2
        done
    } >&2

    exit 1
}


_xinputSwitchBool() {
    property="$1"
    deviceId="$2"
    value="$(xinput --list-props "${deviceId}" \
        | grep -i "${property}[[:space:]]\+(" \
        | sed -e '/1$/ { s/1$/0/; b end } ; /0$/ { s/0$/1/; b end }' \
            -e ': end s/^.*:[[:space:]]*//')"
    #echo $value

    xinput --set-prop "${deviceId}" "${property}" ${value} >/dev/null 2>&1
}


# Switch on/off touchpad.
_toggleTouchpad() {
    if _checkDep synclient && synclient 1>/dev/null 2>&1; then

        # TODO: End.
        synclient TouchpadOff=$(synclient -l \
            | sed -n 's/.*TouchpadOff.*= 0/1/p; s/.*TouchpadOff.*= 1/0/p') \
            >/dev/null 2>&1

    elif _checkDep xinput; then

        #devId="$(_getDevId 'synaptics')"
        #xinput --set-prop \'"${devId}"\' 'Synaptics Off' ${value} \
        #    >/dev/null 2>&1
        devId="$(_getDevId 'synaptics')"

        [ -z "${devId}" ] \
            && _errorMsg "Failed to to obtain synaptics device's ID."
        _xinputSwitchBool "Synaptics Off" "${devId}"
    else
	_errorMsg "There's no \`synclient' or \`xinput' installed."
    fi


    exit 0
}


_getShellName() {
    for i_shell in $*; do
        if [ -f "${i_shell}" ]; then
            printf '%s\n' "${i_shell}"
            break
        fi
    done
}


_getDevId() {
    # Returns a list of devices in three columns: number of row, id, name.

    shellName=""
    # Shell paths specified in order of priority and performance. Up-to-date
    # `ksh' is not very popular among big amount of Linux or BSD distributions,
    # but it's the most efficient shell after `dash' and expressive enough
    # compared to `zsh' and `bash'.
    shellName="$(_getShellName "/bin/ksh" "/bin/zsh" "/bin/bash")"
    devName=${@#*\"}; devName=${devName%\"*}


    # Use `join' to join lines of devices and IDs if any appropriate shell was
    # found. The fastest shell `dash' can't handle ``<()'' as stdin
    # unfortunately.  Thus, one should use slow sorting instead of `join' to
    # keep script in POSIX way.  Be aware of that `grep' ignores case
    # distinctions in patterns and input.
    if [ -n "${shellName}" ]; then
        #echo "Using \`join'..."
        eval "${shellName} -c '\
            join --nocheck-order -i -j1 \
                <(xinput --list --id-only | cat -n) \
                <(xinput --list --name-only | cat -n)'" \
            | grep -i "${devName}" \
            | cut -d" " -f2
    else
        nameLineCounter=0
        printf '%s\n' "${devNameList}" \
            | while read -r nameLine; do
                nameLineCounter=$((nameLineCounter + 1))
                printf '%s\n' "${nameLine}" \
                    | grep -i -q "${devName}" \
                    && {
                        idLineCounter=0
                        printf '%s\n' "${devIdList}" \
                            | while read -r idLine; do
                                idLineCounter=$((idLineCounter + 1))
                                [ "${nameLineCounter}" -eq "${idLineCounter}" ] \
                                    && {
                                        printf '%s\n' "${idLine}"
                                        break
                                    }
                            done
                        break
                    }
            done
    fi
}


# TODO: Rewrite in Perl.
_setPointers() {
    # Filter devices and settings.
    [ -f "${settingsList}" ] || _errorMsg "Failed to find settings file."


    printf '%s\n' "${scriptName} setting:"


    settingsLineCounter=0
    grep \
        -e '^device[[:blank:]]\+".\+"' \
        -e "^mapping[[:blank:]]\+'.\+'" \
        -e "^'.\+'[[:blank:]]\+.\+" "${settingsList}" \
        | while read -r settingsLine; do
            settingsLineCounter=$((settingsLineCounter + 1))
            #echo "settingsLine: $settingsLine"
            # Apply settings.
            case ${settingsLine} in
                "device "*)
                    devId=""; devId="$(_getDevId ${settingsLine})"
                    #echo "devId: ${devId}"
                    ;;
                "mapping "*)
                    [ -n "${devId}" ] && {
                        map="${settingsLine%\'*}"
                        map="${map#*\'}"
                        printf '\t%s\n' "device=${devId}; button map: \"${map}\""
                        xinput --set-button-map "${devId}" ${map} #\
                            #>/dev/null 2>&1
                    }
                    ;;
                "'"*)
                    [ -n "${devId}" ] && {
                        property="${settingsLine%%\' *}"
                        property="${property#*\'}"
                        value="${settingsLine##*\' }"
                        printf '\t%s\n' "device=${devId}; ${property}: ${value}"
                        xinput --set-prop "${devId}" "${property}" ${value} #\
                            #>/dev/null 2>&1
                    }
                    ;;
                "#"*|"")
                    :
                    ;;
                *)
                    printf '%b' "Inappropriate line ${settingsLineCounter} " \
                        "in ${settingsList}.\n"
                    exit 1
            esac
        done
}


_main() {
    _setPointers
    exit 0
}


_loadGoods

#  Use IDs instead of names for proper setting properties because some devices
#  have trailing spaces in their names.
#  Output of `xinput --list' have to be in the same order for both names and
#  IDs to keep script consistent.
settingsList="${scriptDir}/init_pointdevs.props"
devNameList=$(xinput --list --name-only)
devIdList=$(xinput --list --id-only)

# Set arguments for this script.
while getopts "th" options; do
    case "${options}" in
        t) _toggleTouchpad ;;
        h) _helpMsg; exit 0 ;;
        *) _helpMsg; exit 1 ;;
    esac
done

_main
