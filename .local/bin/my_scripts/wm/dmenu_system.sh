#!/usr/bin/env dash


_loadGoods() {
    . my_functions
    . my_aliases
}


_sysCmd() {
    # Define how to run `reboot' and `poweroff' commands under root user
    # privileges whether the machine is running `systemd' or not.

    if _checkSysD; then
        systemctl "$1"
    else
        if
        _checkDep doas; then
            doas "$1"
        elif _checkDep sudo; then
            sudo "$1"
        else
            printf '%s\n' "Failed to execute command as super user" && exit 1
        fi
    fi
}


_rebootMachine() {
    _sysCmd 'reboot'
}


_poweroffMachine() {
    _sysCmd 'poweroff'
}


# Lists possible actions to choose from.
_listActions() {
    printf "%s\n" \
        "Kill Xorg" \
        "Reboot" \
        "Power off" \
        "Shared display mode" \
        "Default display mode" \
        "My keymaps" \
        "Original keymaps" \
        "Point devices initialization" \
        "Environtment initialization" \
	| _dynamicMenu 6 2 "System"
}


_loadGoods
choice="$(_listActions)"    # Get stdout of `_listActions'.

# Run the selected command. Be aware of that the case variable should be first
# letters of the action in the list correspondingly.
case "$choice" in
    Ki*) _killEmAll Xorg ;;
    Re*) _rebootMachine   ;;
    Po*) _poweroffMachine ;;
    Sh*) init_displays.sh share ;;
    De*) init_displays.sh default ;;
    My*) init_keyboards.sh ;;
    Or*) init_keyboards.sh original ;;
    Po*) init_pointdevs.sh ;;
    En*) init_environtment.sh ;;
esac
