#!/usr/bin/env dash

# This script gets current satus from fifo pipe /tmp/blocks_pomodoro.pipe, 
# prints it and returns it back to pipe: 'work_time rest_time work|rest|pause'.

# Set path to fifo pipe for `blocks_pomodoro.sh'.
fifo_pipe="/tmp/blocks_pomodoro.pipe"
# Set pomodoro's programm name.
p_name="pomodoro.sh"

# Check if there is fifo pipe and main programm is running.
# if [ -p $fifo_pipe ] && \
# [ $(pidof "$p_name" | awk -c '{print NF}') -gt 0 ]; then
if [ -p "${fifo_pipe}" ]; then
    awk -c -v fifo_pipe="${fifo_pipe}" ' \
        function status_icon(state) { \
            if (state=="work") printf "%s", ""; \
            if (state=="rest") printf "%s", ""; \
            if (state=="pause") printf "%s", "" \
        } \
        function status_time(state,work_time,rest_time) { \
            if (state=="work") printf "%s", work_time; \
            if (state=="rest") printf "%s", rest_time; \
            if (state=="pause") printf "%s", work_time"/"rest_time \
        } \
        BEGIN {} \
        $1 !~/[^0-9]/ && $2 !~/[^0-9]/ && $3 ~/work|rest|pause/ && NF==3 && NR==1 \
            { \
                pattern_passed=1; \
                printf "%s\n", status_icon($3)status_time($3,$1,$2); \
                system("echo "$0" >> "fifo_pipe" &") \
                next \
            } \
        pattern_passed!=1 { printf "%s\n", "u !!!" } \
        END {} \
        ' "${fifo_pipe}"
# Check if there is fifo pipe, but main programm is not running.
# elif [ -p $fifo_pipe ] && \
# [ $(pidof "$p_name" | awk -c '{print NF}') -eq 0 ]; then
#     rm $fifo_pipe 
#     echo ""
# If there is no any pomodoro time or fifo pipe.
else
    echo ""
fi
