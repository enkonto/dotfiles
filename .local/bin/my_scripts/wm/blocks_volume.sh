#!/usr/bin/env dash

# TODO: status for microphone                 


# Run `sleep' command because of slower `amixer' execution then `echo'.
sleep 0.1


if [ "$(amixer controls | wc -l)" -le "2" ]; then
    am_status=""
else
    # Define current default audio card name
    am_card=$(amixer scontrols \
        | head -n 1 \
        | sed -e "s/Simple mixer control '/\"/" -e "s/',0/\"/")

    # Define current status of the card. Field should be defined manualy.
    am_status=$(amixer get "${am_card}" \
        | tail -n1 \
        | awk -c '{ print $6 }' \
        | sed -e 's/\[//' -e 's/\]//')
fi


case "${am_status}" in
    on)
        # Define current volume level
        am_vol="$(amixer get "${am_card}" \
            | tail -n1 \
            | sed -r 's/.*\[(.*)%\].*/\1/')"

        case "${am_vol}" in
            100|9[0-9]) printf '%s' "${am_vol}%" ;;
            [0-5])      printf '%s' "${am_vol}%" ;;
            *)          printf '%s' "${am_vol}%" ;;
        esac
        ;;
    off)    printf '%s' "" ;;
    *)     printf '%s' ""  ;;
esac
