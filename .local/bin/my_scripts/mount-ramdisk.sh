#!/usr/bin/env dash


[ -d "/mnt/ramdisk" ] \
    || {
        echo "Creating random file..."
        mkdir /mnt/ramdisk
        mount -t tmpfs -o size=2g tmpfs /mnt/ramdisk
    }
