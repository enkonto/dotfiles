#!/bin/sh

# Just swallow all output fed to the terminal

# setsid $@ >/dev/null 2>&1

# echo "$@" | sed \
#     "s/'/\\\'/g; \
#     s/\!/\\\!/g; \
#     s/ /\\\ /g"

awk -c -v sh_inp="$@" ' \
    END { print sh_inp } \
    '
