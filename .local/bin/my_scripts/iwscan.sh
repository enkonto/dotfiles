#!/usr/bin/env dash


dev_name=$({
    name="$(iwconfig 2>&1 | grep -i "ieee")"
    name="${name%% *}"
    printf "%s" "${name}"
})


scan_air() {
    # * Get list of accessible SSIDs.
    ap_list="$(/usr/bin/ls /etc/wpa_supplicant.conf.* | sed 's/^.\+conf\.//')"
    # * Get SSIDs.
    # * Get better mode.
    # * Get better signal.
    # printf "%s" "${best_AP}"
    iw "$*" scan | awk -c -v ap_list="${ap_list}" ' \
        BEGIN { signal=-100; freq=0; SSID=""; fr_f=0; s_f=0;
            split(ap_list,ap_ar) }
        $1 ~ /freq:$/ { if ($2 > freq) { freq=$2; fr_f=1 } }
        $1 ~ /signal:$/ { if (int($2) > signal) { signal=int($2); s_f=1 } }
        $1 ~ /SSID:$/ && fr_f==1 && s_f==1 { for (i in ap_ar) { if (ap_ar[i]==$2) print $2 } }
        $1 ~ /^BSS/ { fr_f=0; s_f=0 }'
}


if $(ip link show | grep "${dev_name}" | grep -q "state UP"); then
    scan_air "${dev_name}"
fi



# Get a wireless device name.
#wl_dev_name="$(iw dev | grep "Interface" | head -n 1)"
#wl_dev_name="${wl_dev_name## }"
#
#wl_connect(){
#    if [ "$(ip link show "${wl_dev_name}" | grep -q "DOWN")" ]; then
#        ip link set "${wl_dev_name}" up || { echo error; return 1 }
#    elif [ "$(ip link show "${wl_dev_name}" | grep -q "UP")" ]; then
#        return 0
#    else
#        echo error; return 1
#    fi
#    if [ "$(iw "${wl_dev_name}" link | grep -i -q "not connected")" ]; then
#        echo error; return 1
#    else
#        return 0
#    fi
#    wpa_supplicant -B -i "${wl_dev_name}" -c "/etc/wpa_supplicant.conf.${ap_name}"
#    dhclient "${wl_dev_name}"
#}
#
#wl_disconnect(){
#    rm "/var/run/wpa_supplicant/${wl_dev_name}"
#    dhclient -r
#}
