List of common key bindings.  All definitions are stored at:

    ~/.config/sxhkd/sxhkdrc_master
    ~/.config/sxhkd/sxhkdrc_bspc


Legend:

    !D - function disabled by developer.


Last review 2020-12-17.


Basic keys
==========

Escape                      |-Reload `bspwm'. !D

Alt + Escape                |-Quit `bspwm'.

Shift + Escape              |-Launch system management menu.

Control + Escape            |-Reload `sxhkd'.


Node focus
==========

{h,j,k,l} + {_,Control}     |-Move focus in the given direction between
                            | {all nodes,only floating nodes}.

{f,b} + {_,control,alt}     |-Cycle focus on {next, previous} node at the
                            | focused desktop among {all, tiled, floating}
                            | nodes.

Tab + Shift                 |-Switch to last active node.


Node modification
=================

m : {h,j,k,l}               |-Swap focused node with the another one or move
                            | a floating node in continuous mode.

{alt,control} + m           |-Circulate nodes {backward, forward} in a parent
                            | node.

i + {control,alt} ; {p,r}   |-Insert {focused, marked} node(-s) in the
                            | {preselection area, receptacle}.

i                           |-Insert all marked nodes to the focused desktop.

a+{control,alt}: {h,j,k,l}  | Spawn a receptacle in a given direction
                            | respectively to the {focused nodes, tree} in
                            | continuous mode.

a : {h,j,k,l}               |-Set preselection for next node (split focused).
                            | Cancel preselection with the same command.

a : {1-9}                   |-Set the preselection area's splitting ratio.

e+{_,shift}+{control,alt}   |-Set {branch, tree} with {applied, reset} equal
                            | splitting ratio. Useful after breaking a branch to
                            | restore nodes' placement.

e + {_, shift} : {1-9}      |-Set splitting ratio of the {branch, tree} in
                            | continuous input mode.

r+{_,shift}+{_,control,alt} |-Rotate nodes in {branch, tree}
                            | {180, clockwise 90, counter-clockwise 90}.

s : {h,j,k,l}               |-{expand, fallback} both opposite of node's edge in
                            | the given direction.

s:{h,j,k,l}+{control,alt}   |-{expand, fallback} one of node's edge in the given
                            | direction.

Backspace                   |-Close focused node safely (apps might require
                            | confirmation).

Backspace + Shift           |-Kill focused node (no confirmation whatsoever).

Backspace+{_,shift}+Control |-Kill {the first matching, all} receptacle(-s) at
                            | the focused desktop.

Backspace + alt             |-Cancel a preselection area at the focused desktop.

Backspace+alt+control+{_,shift} |-{close, kill} all nodes in the present desktop
                            | except the {focused, marked} ones.


Toggle node/desktop layout/flags
====================================

Space                       |-Toggle between tiled and monocle layout.

x ; {t, f, p, x}            |-Toggle {tiled, floating, pseudo tiled, full
                            | screen} mode.

{!,#}                       |-Incrementally {decrease, increase} gaps between
                            | tiled nodes.

semicolon ; {h,s,p,l,m,u}   |-Set the node flags: {hidden, sticky, private,
                            | locked, marked, urgent}.

semicolon;{control,alt}+{h,s,p,l,m,u} |-{add, remove} {hidden, sticky, private,
                            | locked, marked, urgent} flag at all nodes at the
                            | focused desktop.

semicolon ; {_, shift} + d  |-Remove all flags at the {focused node, all nodes
                            | at the focused desktop}.

semicolon;shift+{h,s,p,l,m,u}:{f,b} |-Switch to the {next, previous} node with
                            | {hidden, sticky, private, locked, marked, urgent}
                            | flag at the focused desktop.


Desktop operations
====================

g                           |-Build grid layout. !D

{v, z}                      |-Switch tiled nodes to {vertical, horizontal}
                            | layout.

{0-9} + {_, control, alt}   |-Switch between desktops or send {focused node, all
                            | nodes} to a given desktop.

Tab + {_, control, alt}     |-Switch to the last visited desktop or cycle focus
                            | to the {next, previous} occupied desktop at the
                            | current monitor. After switching remove desktop if
                            | it's empty.

{period,comma}+{_,control,alt} |-Switch to the {next, previous} monitor or send
                            | {focused node, all local nodes} to the {next,
                            | previous} monitor.


Launching programs
==================

p + {_, Control}            |-Run {program launcher, `keepmenu'}.

apostrophe                  |-Launch program menu.

Return + {_, Control}       |-Open a standalone/tabbed terminal emulator.

Return + Alt                |-Open a Emacs client.


Useful extras
=============

F1                          |-Launch a floating node with this cheat sheet

F3; {semicolon,q,j,k}       |-DeaDBeeF: play-pause, stop, previous, next.


Without modifiers
=================

Print                       |-Save a screenshot of the focused desktop to the
                            | `~/shots'.

Shift + Print               |-Save a screenshot of the focused desktop to the
                            | `~/shots' in the interactive mode. Select a
                            | window or rectangle with a pointer.

right click                 |-Drag floating node.

left click                  |-Resize floating node from the nearest edge.


Further reading
===============

Study the ~/.config/sxhkd/sxhkdrc_master and ~/.config/sxhkd/sxhkdrc_bspc.
They are extensively documented.  One will find key chords for tasks not
included herein, such as multimedia keys, setting node flags, and more.
