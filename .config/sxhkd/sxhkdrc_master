
# Description
# ===========

# SXHKD module with WM-agnostic keys
# Use `xev' to find out keys' names.

# TODO: Add scratchpad (mod3 + dollar to open, repeat to close)


# Environment
# -----------

# Manage system stuff. Quit `bspwm', reload `sxhkd', run system menu.
mod3 + {_, control, alt, shift} + Escape
    { \
        bspwm_menu.sh, \
        dmenu_system.sh, \
        bspc quit, \
        pkill -USR1 -x sxhkd \
    }


# Backup keys.
mod4 + {_, control, alt, shift} + Escape
    { \
        bspwm_menu.sh, \
        dmenu_system.sh, \
        bspc quit, \
        pkill -USR1 -x sxhkd \
    }


# Main programs
# -------------

# Use terminal emulator as a standalone tool or embedded in tabbed
mod3 + {_, control} + Return
    {eval $MY_TERM, eval $TABTERM}


# Run `emacs'.
mod3 + alt + Return
    eval $MY_EMACS


# Invoke run dialog, run program from the menu or password manager.
mod3 + {_, control, alt} + p
    {eval $MY_LAUNCHER, dmenu_launcher.sh, keepmenu}


# Switch windows for EWMH compatible WMs.
mod3 + w
    rofi -show window


# Miscellaneous tools and media keys
# ----------------------------------

# Screenshots (requires `scrot`).  First one is for the focused window.
# The other one is for interactive mode. Use `sleep' to prevent flaws on key
# release.
@Print
    sleep 0.2; scrot -u -e 'mv $f ~/shots/'
@shift + Print
    sleep 0.2; scrot -s -l color='#91231C' -e 'mv $f ~/shots/'


# Bring up the help text for common keybindings
mod3 + F1
    eval $EMBTERM less "$HOME/.config/sxhkd/cheatsheet_sxhkdrc.txt"


# Specify `deadbeef' keys for keyboards without media keys.
mod3 + F3; {apostrophe, q, j, k}
    deadbeef { \
        --play-pause, \
        --stop, \
        --prev, \
        --next \
    }


# Specify `deadbeef` keys for keyboards with media keys.
XF86Audio{Play, Stop, Prev, Next}
    deadbeef { \
        --play-pause, \
        --stop, \
        --prev, \
        --next \
    }


# Set brightness for monitor screen. Use only if `acpi' can't be managed.
XF86MonBrightness{Down, Up}
    xbacklight -{dec, inc} 10


# # Control the [last] currently active player.
# mod3 + XF86Audio{Play, Prev, Next}
#     playerctl {play-pause, previous, next}


# # Speaker/output volume.  Requires `amixer` and dedicated keys.
# XF86Audio{Mute, LowerVolume, RaiseVolume}
#     amixer set Master {toggle, 5%-, 5%+}


# # Microphone/input volume.  Same requirements as previous definition.
# XF86AudioMicMute
#     amixer set Capture toggle
# mod3 + XF86Audio{Mute, LowerVolume, RaiseVolume}
#     amixer set Capture {toggle, 5%-, 5%+}
