-- From https://github.com/wbthomason/packer.nvim

-- When we are bootstrapping a configuration, it doesn't make sense to execute
-- the rest of the init.lua. You'll need to restart nvim, and then it will work.


-- Install packer.
local install_path = vim.fn.stdpath 'data' .. '/site/pack/packer/start/packer.nvim'
local is_bootstrap = false
if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  is_bootstrap = true
  vim.fn.system { 'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path }
  vim.cmd [[packadd packer.nvim]]
end

-- Only required if you have packer configured as `opt`
--vim.cmd [[packadd packer.nvim]]

--return require('packer').startup(function(use)
require('packer').startup(function()
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'


  -- use {
  -- 'nvim-telescope/telescope.nvim', tag = '0.1.1',
  -- -- or                            , branch = '0.1.x',
  -- requires = { {'nvim-lua/plenary.nvim'} }
  -- }


  -- -- A native telescope sorter to significantly improve sorting performance.
  -- --use {'nvim-telescope/telescope-fzf-native.nvim', run = 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build' }
  -- use {'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }


  -- use {
  --   -- Bundle all the "boilerplate code" necessary to have nvim-cmp (a popular
  --   -- autocompletion plugin) and nvim-lspconfig working together.
  --   'VonHeikemen/lsp-zero.nvim',
  --   requires = {
  --         {'neovim/nvim-lspconfig'},-- Configurations for Nvim LSP

  --         -- Portable package manager for Neovim that runs everywhere Neovim
  --         -- runs. Easily install and manage LSP servers, DAP servers, linters,
  --         -- and formatters.
  --         {'williamboman/mason.nvim'}, {'williamboman/mason-lspconfig.nvim'},

  --         {'j-hui/fidget.nvim'},    -- Useful status updates for LSP
  --         {'folke/neodev.nvim'},    -- Additional lua configuration, makes nvim
  --                                   -- stuff amazing.
  --         -- Autocomplition
  --         {'hrsh7th/nvim-cmp'},     -- A completion engine plugin for neovim written
  --                                   -- in Lua. Completion sources are installed from
  --                                   -- external repositories and "sourced".
  --         {'hrsh7th/cmp-nvim-lua'}, -- Nvim-cmp source for neovim Lua API.
  --         {'hrsh7th/cmp-nvim-lsp'}, -- Nvim-cmp source for neovim's built-in
  --                                   -- language server client.
  --         {'hrsh7th/cmp-buffer'},   -- Nvim-cmp source for buffer words.
  --         {'hrsh7th/cmp-cmdline'},  -- Nvim-cmp source for vim's cmdline.
  --         {'hrsh7th/cmp-path'},     -- Nvim-cmp source for filesystem paths.
  --         --{'f3fora/cmp-spell'},     -- Nvim-cmp source for spelling.
  --         {'lukas-reineke/cmp-rg'}, -- source `ripgrep' to nvim-cmp.

  --         -- Snippets
  --         {'L3MON4D3/LuaSnip'},
  --         {'saadparwaiz1/cmp_luasnip'},-- Luasnip completion source for nvim-cmp.
  --     }
  -- }


  -- use('ThePrimeagen/harpoon')


  -- use {
  --   'LukasPietzschmann/telescope-tabs',
  --   requires = { 'nvim-telescope/telescope.nvim' },
  --   config = function()
  --   	require'telescope-tabs'.setup{
  --           close_tab_shortcut_i = '<C-A-S-x>', -- if you're in insert mode
  --           close_tab_shortcut_n = '<leader>x', -- if you're in normal mode
  --   	}
  --   end
  --   }


  -- use('nvim-treesitter/nvim-treesitter', {run = ':TSUpdate'})
  -- use 'nvim-treesitter/playground'
  -- -- Enable treesitter syntax highlighting
  -- require('nvim-treesitter.configs').setup {
  --   highlight = {
  --     enable = true
  --   }
  -- }


  -- use { -- Additional text objects via treesitter
  --   'nvim-treesitter/nvim-treesitter-textobjects',
  --   after = 'nvim-treesitter',
  -- }


  -- -- Rainbow parentheses for neovim using tree-sitter. This is a module for
  -- -- nvim-treesitter, not a standalone plugin. It requires and is configured via
  -- -- nvim-treesitter.
  -- use 'p00f/nvim-ts-rainbow'


  -- A high-performance color highlighter for Neovim which has no external
  -- dependencies! Written in performant Luajit. Why another highlighter?
  -- Mostly, RAW SPEED.  This has no external dependencies, which means you
  -- install it and it just works.
  use 'norcalli/nvim-colorizer.lua'
  --require'colorizer'.setup()


  use 'ellisonleao/gruvbox.nvim'


  use {
    'phaazon/hop.nvim',
    branch = 'v2', -- optional but strongly recommended
  }


  -- A blazing fast and easy to configure Neovim statusline written in Lua.
  use 'nvim-lualine/lualine.nvim'


  -- -- Information provided by active lsp clients from the '$/progress' endpoint
  -- -- as a statusline component for lualine.nvim.
  -- use 'arkav/lualine-lsp-progress'


  -- -- Super fast git decorations implemented purely in lua/teal.
  -- use {
  --   'lewis6991/gitsigns.nvim',
  -- }


  -- An autocommand that removes all:
  --   - trailing whitespace
  --   - empty lines at the end of the buffer
  -- on every 'BufWritePre'.
  use({
      "mcauley-penney/tidy.nvim",
      config = function()
          require("tidy").setup({
            filetype_exclude = { "markdown", "diff" },
            })
      end
  })


  -- -- Highlight and search for comments like TODO, HACK, BUG in your code base.
  -- use {
  -- "folke/todo-comments.nvim",
  -- requires = { "nvim-lua/plenary.nvim",   -- with ripgrep
  --            'nvim-telescope/telescope.nvim',
  --            "folke/trouble.nvim", },
  -- }

  -- -- A pretty list for showing diagnostics, references, telescope results,
  -- -- quickfix and location lists to help you solve all the trouble your code is
  -- -- causing.
  -- use 'folke/trouble.nvim'

  -- Neovim's default :bdelete command can be quite annoying, since it also
  -- messes up your entire window layout by deleting windows. bufdelete.nvim
  -- aims to fix that by providing useful commands that allow you to delete a
  -- buffer without messing up your window layout.
  use 'famiu/bufdelete.nvim'


  -- Complementary pairs of mappings: commonly used ex commands, linewise
  -- mappings, mappings for toggling options, mappings for encoding and
  -- decoding, miscellaneous category.
  use 'tpope/vim-unimpaired'
  -- Surround.vim is all about "surroundings": parentheses, brackets, quotes,
  -- XML tags, and more.
  use 'tpope/vim-surround'
  -- I'm not going to lie to you; fugitive.vim may very well be the best Git
  -- wrapper of all time. Fugitive is the premier Vim plugin for Git.
  use 'tpope/vim-fugitive'
  -- Think of sensible.vim as one step above 'nocompatible' mode: a universal
  -- set of defaults that (hopefully) everyone can agree on.
  use 'tpope/vim-sensible'
  use 'tpope/vim-commentary'    -- Comment stuff out.


  -- A simple, easy-to-use Vim alignment plugin.
  use 'junegunn/vim-easy-align'


  -- This plugin provides a start screen for Vim and Neovim.
  use 'mhinz/vim-startify'


  -- Neomake is a plugin for Vim/Neovim to asynchronously run programs.
  use 'neomake/neomake'

-------------------------------------------------------------------------------

  -- The plug-in visualizes undo history and makes it easier to browse and switch
  -- between different undo branches.
  use {
    'mbbill/undotree',
--  -- Undootree settings.
--  if !exists('g:undotree_WindowLayout')
--      let g:undotree_WindowLayout = 2
--  endif
--  -- e.g. using 'd' instead of 'days' to save some space.
--  if !exists('g:undotree_ShortIndicators')
--      let g:undotree_ShortIndicators = 1
--  endif
--  -- undotree window width
--  if !exists('g:undotree_SplitWidth')
--      if g:undotree_ShortIndicators == 1
--          let g:undotree_SplitWidth = 24
--      else
--          let g:undotree_SplitWidth = 30
--      endif
--  endif
--  -- diff window height
--  if !exists('g:undotree_DiffpanelHeight')
--      let g:undotree_DiffpanelHeight = 10
--  endif
--  -- auto open diff window
--  if !exists('g:undotree_DiffAutoOpen')
--      let g:undotree_DiffAutoOpen = 1
--  endif
--  -- if set, let undotree window get focus after being opened, otherwise
--  -- focus will stay in current window.
--  if !exists('g:undotree_SetFocusWhenToggle')
--      let g:undotree_SetFocusWhenToggle = 1
--  endif
--  -- tree node shape.
--  if !exists('g:undotree_TreeNodeShape')
--      let g:undotree_TreeNodeShape = '*'
--  endif
--  -- tree vertical shape.
--  if !exists('g:undotree_TreeVertShape')
--      let g:undotree_TreeVertShape = '|'
--  endif
--  if !exists('g:undotree_DiffCommand')
--      let g:undotree_DiffCommand = "diff"
--  endif
--  -- relative timestamp
--  if !exists('g:undotree_RelativeTimestamp')
--      let g:undotree_RelativeTimestamp = 1
--  endif
--  -- Highlight changed text
--  if !exists('g:undotree_HighlightChangedText')
--      let g:undotree_HighlightChangedText = 1
--  endif
--  -- Highlight changed text using signs in the gutter
--  if !exists('g:undotree_HighlightChangedWithSign')
--      let g:undotree_HighlightChangedWithSign = 1
--  endif
--  -- Highlight linked syntax type.
--  -- You may chose your favorite through ":hi" command
--  if !exists('g:undotree_HighlightSyntaxAdd')
--      let g:undotree_HighlightSyntaxAdd = "DiffAdd"
--  endif
--  if !exists('g:undotree_HighlightSyntaxChange')
--      let g:undotree_HighlightSyntaxChange = "DiffChange"
--  endif
--  if !exists('g:undotree_HighlightSyntaxDel')
--      let g:undotree_HighlightSyntaxDel = "DiffDelete"
--  endif
--  -- Deprecates the old style configuration.
--  if exists('g:undotree_SplitLocation')
--      echo "g:undotree_SplitLocation is deprecated,
--                  \ please use g:undotree_WindowLayout instead."
--  endif
--  -- Show help line
--  if !exists('g:undotree_HelpLine')
--      let g:undotree_HelpLine = 1
--  endif
--  -- Show cursorline
--  if !exists('g:undotree_CursorLine')
--      let g:undotree_CursorLine = 1
--  endif
--  -- User commands.
--  command! -n=0 -bar UndotreeToggle   :call undotree#UndotreeToggle()
--  command! -n=0 -bar UndotreeHide     :call undotree#UndotreeHide()
--  command! -n=0 -bar UndotreeShow     :call undotree#UndotreeShow()
--  command! -n=0 -bar UndotreeFocus    :call undotree#UndotreeFocus()
  }


  -- Add custom plugins to packer from ~/.config/nvim/lua/custom/plugins.lua
  local has_plugins, plugins = pcall(require, 'custom.plugins')
  if has_plugins then
    plugins(use)
  end

  if is_bootstrap then
    require('packer').sync()
  end
end)


if is_bootstrap then
  print '=================================='
  print '    Plugins are being installed'
  print '    Wait until Packer completes,'
  print '       then restart nvim'
  print '=================================='
  return
end

-- Automatically source and re-compile packer whenever you save this init.lua
local packer_group = vim.api.nvim_create_augroup('Packer', { clear = true })
vim.api.nvim_create_autocmd('BufWritePost', {
  command = 'source <afile> | silent! LspStop | silent! LspStart | PackerCompile',
  group = packer_group,
  pattern = vim.fn.expand '$MYVIMRC',
})

---- vim: set et fdm=marker sts=4 sw=4:
