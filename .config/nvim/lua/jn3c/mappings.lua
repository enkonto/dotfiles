-------------------- Two rules of the good key bindings: -----------------------
-------------------- 1) stay with a default key bindings; ----------------------
-------------------- 2) STAY WITH a default key bindings. ----------------------

-- Convenient ``Explorer'' invocation.
vim.keymap.set('n', '<leader>e', function ()
  -- BUG: Don't work.
  if vim.bo.filetype ~= "netrw" then
    vim.cmd('Ex')
  else
    vim.cmd('Rex')
  end
end)


local function simpleMap(m, k, v)
  vim.keymap.set(m, k, v, { silent = true })
end

-- Emacs inline movements
simpleMap("i", "<A-e>", "<ESC>A")
simpleMap("i", "<A-a>", "<ESC>I")
simpleMap("i", "<A-f>", "<ESC>W")  -- BUG: Don't work
simpleMap("i", "<A-b>", "<ESC>B")  -- BUG: Don't work

-- Keymaps for better default experience. See `:help vim.keymap.set()`
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })

-- Use `[[' and `]]' anyway even when ``{'' or ``}'' are not in the first column
-- BUG: Don't work
vim.keymap.set({ 'n', 'v' }, "]]", "j0[[%/{<CR>")
--   :map ]] j0[[%/{<CR>
vim.keymap.set({ 'n', 'v' }, "[[", "?{<CR>w99[{")
--   :map [[ ?{<CR>w99[{
vim.keymap.set({ 'n', 'v' }, "][", "/}<CR>b99]}")
--   :map ][ /}<CR>b99]}
vim.keymap.set({ 'n', 'v' }, "[]", "k$][%?}<CR>")
--   :map [] k$][%?}<CR>

-- Remap for dealing with word wrap
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- Highlight on yank. See `:help vim.highlight.on_yank()`
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = highlight_group,
  pattern = '*',
})

-- Move visual blocks up and down.
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")    -- acts same as <J> with V-LINE.
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- Better ``join line'' function (:h J). Cursor stays at the begining of a line.
--vim.keymap.set("n", "J", "mzJ`z")

-- Alternate current word placed under cursor.
vim.keymap.set("n", "<leader>a",
    [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]], {noremap = true})

-- Cursor stays in the middle of a window after <C-e> or <C-y>.
--vim.keymap.set("n", "<C-e>", "<C-e>zz")
--vim.keymap.set("n", "<C-y>", "<C-y>zz")
-- Cursor stays in the middle of a window after <C-d> or <C-u>.
--vim.keymap.set("n", "<C-d>", "<C-d>zz")
--vim.keymap.set("n", "<C-u>", "<C-u>zz")
-- Cursor stays in the middle of a window after <C-f> or <C-b>.
--vim.keymap.set("n", "<C-f>", "<C-f>zz")
--vim.keymap.set("n", "<C-b>", "<C-b>zz")
-- FIX: Does not full page scrolls. About four fifths.

-- Search term stays in the middle of a window after <n> or <N>.
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- Better quickfix navigation.
vim.keymap.set("n", "<C-k>", "<cmd>cnext<CR>zz")
vim.keymap.set("n", "<C-j>", "<cmd>cprev<CR>zz")
vim.keymap.set("n", "<leader>k", "<cmd>lnext<CR>zz", {noremap = true})
vim.keymap.set("n", "<leader>j", "<cmd>lprev<CR>zz", {noremap = true})

-- Yank to the clipboard of X if vim doesn't have clipboard support enabled.
-- NOTE: `wl-clip' for Wayland, `pbcopy' for MacOS.
vim.api.nvim_set_keymap('n', '<leader>y', "'<,'>w !xclip -selection clipboard", {noremap = true})

-- Preserve yanked part in the buffer after pasting in place of highlighted part.
vim.api.nvim_set_keymap('n', '<leader>p', [["_dP]], {noremap = true})

-- Key maps to emulate the "system clipboard" shortcuts. Not needed for `neovim'
-- launched via `st'.
vim.api.nvim_set_keymap('i', '<C-A-v>',     '<ESC>"+pa',    {noremap = true})
vim.api.nvim_set_keymap('v', '<C-A-v>',     '"+P',          {noremap = true})
vim.api.nvim_set_keymap('n', '<C-A-v>',     '"+P',          {noremap = true})
vim.api.nvim_set_keymap('v', '<C-S-A-v>',   '"+p',          {noremap = true})
vim.api.nvim_set_keymap('n', '<C-S-A-v>',   '"+p',          {noremap = true})
vim.api.nvim_set_keymap('v', '<C-A-c>',     '"+y',          {noremap = true})
vim.api.nvim_set_keymap('n', '<C-A-c>',     '"+y',          {noremap = true})
vim.api.nvim_set_keymap('n', '<C-S-A-c>',   '"+yy',         {noremap = true})
vim.api.nvim_set_keymap('v', '<C-A-d>',     '"+d',          {noremap = true})
vim.api.nvim_set_keymap('n', '<C-A-d>',     '"+d',          {noremap = true})
vim.api.nvim_set_keymap('n', '<C-S-A-d>',   '"+dd',         {noremap = true})

vim.keymap.set("n", "Q", "<nop>")   -- Disable <Q>. NOTE: Don't know what for.

-- Switch `tmux' sessions.
vim.keymap.set("n", "<leader>m", "<cmd>silent !tmux neww tmux-sessionizer<CR>",
    {noremap = true})


-- EasyAlign
vim.api.nvim_set_keymap('x', 'ga', '<Plug>(EasyAlign)', {})
vim.api.nvim_set_keymap('n', 'ga', '<Plug>(EasyAlign)', {})

-- Wipe to the black hole.
vim.api.nvim_set_keymap('n', 'x', '"_x', {noremap = true})

-- Add empty lines.
vim.api.nvim_set_keymap('n', '<leader>o', 'o<Esc>^D', {noremap = true})
vim.api.nvim_set_keymap('n', '<leader>O', 'O<Esc>^D', {noremap = true})

-- -- Telescope. See `:help telescope.builtin'.
-- local builtin = require('telescope.builtin')
-- vim.keymap.set('n', '<leader>sf', builtin.find_files, { desc = '[S]earch [F]iles' })
-- vim.keymap.set('n', '<leader>sg', builtin.live_grep, { desc = '[S]earch by [G]rep' })
-- vim.keymap.set('n', '<leader>sv', builtin.git_files, {})
-- vim.keymap.set('n', '<leader>so', builtin.oldfiles, { desc = 'Find recently [O]pened files' })
-- vim.keymap.set('n', '<leader>sb', builtin.buffers, { desc = '[ ] Find existing buffers' })
-- vim.keymap.set('n', '<leader>sh', builtin.help_tags, { desc = '[S]earch [H]elp' })
-- vim.keymap.set('n', '<leader>sw', function()
--     builtin.grep_string({ search = vim.fn.input("Grep > ") });
-- end, { desc = '[S]earch current [W]ord' })
-- vim.keymap.set('n', '<leader>sc', function()
--   -- Pass additional configuration to telescope to change theme, layout, etc.
--   builtin.current_buffer_fuzzy_find(require('telescope.themes').get_dropdown {
--     winblend = 10,
--     previewer = false,
--   })
-- end, { desc = 'Fuzzily search in [C]urrent buffer' })

-- nvim-lspconfig does not set keybindings or enable completion by default. The
-- following example configuration provides suggested keymaps for the most
-- commonly used language server functions, and manually triggered completion
-- with omnifunc (<c-x><c-o>).
-- Note: you must pass the defined 'on_attach' as an argument to every
-- 'setup {}' call and the keybindings in 'on_attach' only take effect on
-- buffers with an active language server.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
local opts = { noremap=true, silent=true }
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<leader>lo', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '<leader>ll', vim.diagnostic.setloclist, opts)
-- This function gets run when an LSP connects to a particular buffer.
-- Use an on_attach function to only map the following keys after the language
-- server attaches to the current buffer.
local on_attach = function(client, bufnr)
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    -- Enable completion triggered by <c-x><c-o>
    vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

    -- Function that lets more easily define mappings specific for LSP related
    -- items. It sets the mode, buffer and description for us each time.
    local nmap_lsp = function(keys, func, desc)
      local bufopts = { noremap=true, silent=true, buffer=bufnr, desc = desc }
      if desc then desc = 'LSP: ' .. desc; end
      vim.keymap.set('n', keys, func, bufopts)
    end

    nmap_lsp('<leader>lt', vim.lsp.buf.type_definition, '[T]ype Definition')
    nmap_lsp('<leader>ld', vim.lsp.buf.definition, 'Goto [D]efinition')
    nmap_lsp('<leader>li', vim.lsp.buf.implementation, 'Goto [I]mplementation')
    nmap_lsp('<leader>ln', vim.lsp.buf.rename, '[R]ename')
    nmap_lsp('<leader>lc', vim.lsp.buf.code_action, 'Code [A]ction')
    nmap_lsp('<leader>lr', vim.lsp.buf.references, 'Goto [R]eferences')
    nmap_lsp('<leader>lh', vim.lsp.buf.hover, '[H]over Documentation')
    nmap_lsp('<leader>lS', vim.lsp.buf.signature_help, '[S]ignature Documentation')
    nmap_lsp('<leader>lD', vim.lsp.buf.declaration, 'Goto [D]eclaration')
    nmap_lsp('<leader>lf', vim.lsp.buf.formatting)
    nmap_lsp('<leader>ls', require('telescope.builtin').lsp_document_symbols, 'Document [S]ymbols')
    nmap_lsp('<leader>lws', vim.lsp.buf.workspace_symbol, '[W]orkspace [S]ymbols')
    nmap_lsp('<leader>lwa', vim.lsp.buf.add_workspace_folder, '[W]orkspace [A]dd Folder')
    nmap_lsp('<leader>lwr', vim.lsp.buf.remove_workspace_folder, '[W]orkspace [R]emove Folder')
    nmap_lsp('<leader>lwl', function()
        print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, '[W]orkspace [L]ist Folders')

    -- Create a command `:Format` local to the LSP buffer
    vim.api.nvim_buf_create_user_command(bufnr, 'Format', function(_)
      vim.lsp.buf.format()
    end, { desc = 'Format current buffer with LSP' })
end

-- Hop is the EasyMotion alternate written in lua.
-- BUG: Not working in command line window (ctrl-f in command line) and in
--      visual mode.
--      Sometimes cause error when invoked while cursor is on empty line.
-- TODO: Add pattern search over the whole document.
local hop = require('hop')
local directions = require('hop.hint').HintDirection
vim.keymap.set('', '<leader>hf', function()
  hop.hint_char1({ direction = directions.AFTER_CURSOR,
    current_line_only = false, case_insensitive = false })
end, {noremap=true})
vim.keymap.set('', '<leader>hF', function()
  hop.hint_char1({ direction = directions.BEFORE_CURSOR,
    current_line_only = false, case_insensitive = false })
end, {noremap=true})
vim.keymap.set('', '<leader>ht', function()
  hop.hint_char1({ direction = directions.AFTER_CURSOR,
    current_line_only = false, case_insensitive = false, hint_offset = -1 })
end, {noremap=true})
vim.keymap.set('', '<leader>hT', function()
  hop.hint_char1({ direction = directions.BEFORE_CURSOR,
    current_line_only = false, case_insensitive = false, hint_offset = 1 })
end, {noremap=true})
-- BUG: Don't work on empty lines (^$).
vim.keymap.set('', '<leader>he', function()
  hop.hint_words({ direction = directions.AFTER_CURSOR,
    hint_position = require'hop.hint'.HintPosition.END,
    current_line_only = false })
end, {noremap=true})
-- BUG: Don't work on empty lines (^$).
vim.keymap.set('', '<leader>hge', function()
  hop.hint_words({ direction = directions.BEFORE_CURSOR,
    hint_position = require'hop.hint'.HintPosition.END,
    current_line_only = false })
end, {noremap=true})
vim.keymap.set('', '<leader>hw', function()
  hop.hint_words({ direction = directions.AFTER_CURSOR,
    current_line_only = false})
end, {noremap=true})
vim.keymap.set('', '<leader>hb', function()
  hop.hint_words({ direction = directions.BEFORE_CURSOR,
    current_line_only = false})
end, {noremap=true})
vim.keymap.set('', '<leader>hl', function()
  hop.hint_lines_skip_whitespace({ current_line_only = false})
end, {noremap=true})
vim.keymap.set('', '<leader>hp', function()
  hop.hint_patterns({ current_line_only = false})
end, {noremap=true})

vim.api.nvim_set_keymap('n', '<leader>t', ':tabnew<CR>', {noremap = true})
vim.api.nvim_set_keymap('n', '<leader>T', ':terminal<CR>', {noremap = true})
vim.api.nvim_set_keymap('n', '<leader>*', ':nohlsearch<CR>', {noremap = true})
vim.api.nvim_set_keymap('n', '<leader>#', ':nohlsearch<CR>', {noremap = true})
vim.keymap.set("n", "<Leader>c", function()
    -- Set variable ``vim.opt.spelllang'' in ``options.lua''.
    vim.o.spell = not vim.o.spell
    print("spell: " .. tostring(vim.o.spell))
end)

vim.api.nvim_set_keymap('n', '<leader>u', ':UndotreeToggle<CR>', {noremap = true})
-- TODO: Make it continuous.

-- Run file from the current buffer.
vim.api.nvim_set_keymap('n', '<leader>r', ':!"%:p"<CR>', {noremap = true})


--" LuaSnip keymaps.
--" press <Tab> to expand or jump in a snippet. These can also be mapped separately
--" via <Plug>luasnip-expand-snippet and <Plug>luasnip-jump-next.
--imap <silent><expr> <Tab> luasnip#expand_or_jumpable() ? '<Plug>luasnip-expand-or-jump' : '<Tab>'
--" -1 for jumping backwards.
--inoremap <silent> <S-Tab> <cmd>lua require'luasnip'.jump(-1)<Cr>
--
--snoremap <silent> <Tab> <cmd>lua require('luasnip').jump(1)<Cr>
--snoremap <silent> <S-Tab> <cmd>lua require('luasnip').jump(-1)<Cr>
--
--" For changing choices in choiceNodes (not strictly necessary for a basic setup).
--imap <silent><expr> <C-E> luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-E>'
--smap <silent><expr> <C-E> luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-E>'
--
--" You can't stop me
--cmap w!! w !sudo tee %
