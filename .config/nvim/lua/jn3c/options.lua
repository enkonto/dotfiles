
-- Truecolor works just out of the box for `neovim'. But, unfortunately,
-- ``termguicolors'' turn off syntax highlighting in terminals like ``urxvt''.
--if has('termguicolors')
--    set termguicolors
--else
--    set notermguicolors
--    let g:rehash256=1
--endif


vim.cmd [[set termguicolors]]
vim.cmd [[syntax enable]]   -- Enables syntax highlighting.


local options = {
  -- Tree style for Explore
  netrw_liststyle=3,
  -- Must happen before any plugins are set up
  mapleader = " ",
  -- (otherwise wrong leader will be used).
  maplocalleader = " ",
  loaded_matchparen = 1,
}


for k, v in pairs(options) do
  vim.g[k] = v
end
