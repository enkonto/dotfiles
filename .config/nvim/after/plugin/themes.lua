
require("gruvbox").setup({

  undercurl = true,
  underline = true,
  bold = true,
  --italic = true,  -- FIX: broken after udate
  strikethrough = true,
  invert_selection = true,
  invert_signs = false,
  invert_tabline = false,
  invert_intend_guides = true,
  inverse = true, -- invert background for search, diffs, statuslines and errors
  contrast = "hard", -- can be "hard", "soft" or empty string

  --palette_overrides = {},
  --overrides = {},
  --overrides = {},
  --dim_inactive = false,
  --transparent_mode = false,
})


-- theme setup must be called before loading the colorscheme
--vim.cmd.colorscheme "lunaperche"
--vim.cmd.colorscheme "slate"
vim.cmd.colorscheme "habamax"
vim.cmd.colorscheme "gruvbox" -- load any embedded theme since ``gruvbox''
                              -- issues errors
--vim.cmd [[colorscheme gruvbox]]
