require'hop'.setup {
  -- Normalized in order of accessibility.
  --keys = 'uhetonasc.ypbkmjwqr,z\'l;idgxf',  -- for Dvorak layout
  keys = 'tnseriaodhfuwyplvkgmbjc,zx./q;\'',  -- for Colemad-DH layout
  multi_windows = true,
}
