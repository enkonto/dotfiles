local options = {
  -- Fresh start every time.
  -- viminfo='',
  -- Use "gui" colors instead of "cterm" colors in TUI. Requries terminal
  -- compatible with some ISO, most terminals are.
  termguicolors = true,
  -- The encoding displayed.
  encoding="utf-8",
  -- The encoding written to file.
  fileencoding="utf-8",
  -- Ring the bell for error messages.
  errorbells=false,
  -- Enable your mouse.
  mouse=a,
  -- Tells Neovim what the "inherited" (terminal/GUI) background looks like.
  -- Useful for themes that support dark and bright colors like ``gruvbox''.
  background=dark,
  -- Copy paste between `vim' and everything else through ``+'' register. `vim'
  -- have to be compiled with ``+clipboard'' flag, `nvim' need to have access to
  -- a provider for clipboard support, i.e. `xclip' or `xsel' for X11 (RTFM).
  clipboard="unnamedplus",
  -- Automatically read file when changed outside of Vim.
  autoread=true,
  -- Keep multiple buffers, don't unload when it is abandoned.
  hidden=true,
  -- Don't keep backup file after overwriting a file.
  backup=false,
  -- Don't make a backup before overwriting a file.
  writebackup=false,
  -- Don't save undo information in a file.
  undofile=false,
  -- Don't use a swap file for a buffer.
  swapfile=false,
  -- Your working directory will always be the same as your working directory.
  autochdir=true,


  -- Language(s) to do spell checking for.
  spelllang={"en"},
  -- Disable spell checking. It's local only. Enable with a leader key.
  spell=false,
  -- Spelling for camel words.
  spelloptions="camel",
  -- Specify how Insert mode completion works.
  complete=vim.opt.complete+{".","w","b","u","U","k","kspell","s","t"},
  -- Time out time for mapped sequence to complete (ms).
  timeoutlen=750,
  -- Faster completion. If this time (ms) nothing is typed the swap file will be
  -- written to disk.  Triggered in Normal mode only. It is not triggered when
  -- waiting for a command argument to be typed, or a movement after an
  -- operator, or while recording.
  updatetime=300,


  -- So that I can see `` in markdown files.
  conceallevel=0,
  -- Number of spaces that <Tab> uses while editing.
  softtabstop=4,
  -- Insert number of spaces for the <Tab>.
  tabstop=4,
  -- A number of space characters inserted for indentation.
  shiftwidth=4,
  -- Converts tabs to spaces.
  expandtab=true,
  -- Makes indenting smarter. Automatically deletes a number of space characters
  -- defined by ``shiftwidth'' variable.
  smarttab=true,
  -- Makes indenting stupid. Don't use it.
  smartindent=false,
  -- Take indent for new line from previous line.
  autoindent=true,
  -- More cleverly indent then autoindent and smartindent
  cindent=true,
  -- Copy the structure of the existing lines indent.
  copyindent=true,
  -- Preserve as much of the indent structure as possible.
  preserveindent=true,
  -- Insert two spaces after .,?! with a join command.
  joinspaces=true,
  -- Treat dash separated words as a word text object.
  iskeyword=vim.opt.iskeyword+{"-"},
  -- Automatically format lines.
  formatoptions=vim.opt.formatoptions-{"tcrow]j"},
  -- FIX: Comments at the same line after any expression are not wrapped after
  --      reaching line lenght more then in was set by ``textwitdth'' and
  --      current comment leader is now inserted at new line.


  -- Horizontal splits will automatically be under.
  splitbelow=false,
  -- Vertical splits will automatically be to the right.
  splitright=true,
  -- Show the cursor position all the time.
  ruler=true,
  -- Minimal number of screen lines to keep above and below the cursor.
  scrolloff=2,
  -- Minimum visible lines left/right of cursor.
  sidescrolloff=4,
  -- Print current line number in front of each line.
  number=true,
  -- Minimum number column margin.
  numberwidth=1,
  -- Count lines relativity to current line.
  relativenumber=true,
  -- Enable highlighting of the current line.
  cursorline=true,
  -- Insert <EOL> after specified character number.
  wrapmargin=80,
  -- Display long lines as just one line.
  wrap=true,
  -- String to use at the start of wrapped lines.
  showbreak="§   ",
  -- Wrapped lines continue with the same indent
  breakindent=true,
  -- Use tree sitter for folding.
  -- foldmethod="expr",
  -- expr="nvim_treesitter#foldexpr()",
  -- Start with folds open.
  foldenable=false,
  -- Max fold levels.
  foldnestmax=2,
  -- Maximum width of text that is being inserted. A longer line will be broken.
  textwidth=80,
  -- Columns to highlight.
  colorcolumn="80",
  -- Always show tabs.
  showtabline=2,
  -- Don't show current mode message like ``-- INSERT --'' on status line anymore.
  showmode=false,
  -- Always display the status line.
  laststatus=2,
  -- More space for displaying messages.
  cmdheight=2,
  -- Makes pop up menu smaller.
  pumheight=15,

  -- Ignore case in search patterns.
  ignorecase=true,
  -- Highlight match while typing search pattern.
  incsearch=true,
  -- Highlight matches with last search pattern. <Leader>* to stop highlighting.
  hlsearch=true,

  -- Show special characters. They have to be loaded after `nvim' launch.
  list=true,
  -- Draw some special characters.
  listchars={eol="¶",tab="┊ ",nbsp="‡",trail="•",extends="›",precedes="‹"}
}


for k, v in pairs(options) do
  vim.opt[k] = v
end


-- Apparently used to get rid of some redundant messages.
vim.opt.shortmess:append "c"
-- Get rid of intro message when starting vim. Something is causing it to
-- flicker and dissapear anyway and I don't need it.
vim.opt.shortmess:append "I"
