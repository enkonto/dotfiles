vim.keymap.set("n", "<leader>fg", vim.cmd.Git)

local FugitiveGroup = vim.api.nvim_create_augroup("FugitiveGroup", {})

local autocmd = vim.api.nvim_create_autocmd
autocmd("BufWinEnter", {
    group = FugitiveGroup,
    pattern = "*",
    callback = function()
        if vim.bo.ft ~= "fugitive" then
            return
        end

        local bufnr = vim.api.nvim_get_current_buf()
        local opts = {buffer = bufnr, remap = false}
        vim.keymap.set("n", "<leader>fp", function()
            vim.cmd.Git('push')
        end, opts)

        -- Rebase always.
        vim.keymap.set("n", "<leader>fP", function()
            vim.cmd.Git({'pull',  '--rebase'})
        end, opts)

        -- NOTE: It allows to easily set the branch for pushing and any
        --       tracking needed if the branch wasn't set up correctly.
        vim.keymap.set("n", "<leader>ft", ":Git push -u origin ", opts);
    end,
})
