------------------------------------------------------------
-- DS awesome theme based on awesome default theme --
------------------------------------------------------------

theme = {}

theme.font          = "Iosevka 8"

theme.bg_normal     = "#222222"
-- theme.bg_focus      = "#62a541"
-- theme.bg_focus      = "#689d6a"
-- theme.bg_focus      = "#4a7049"
theme.bg_focus      = "#365135"
theme.bg_urgent     = "#ff0000"
theme.bg_minimize   = "#444444"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#aaaaaa"
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#ffffff"

theme.border_width  = 1
theme.border_normal = "#444444"
theme.border_focus  = "#535d6c"
theme.border_marked = "#91231c"

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- Example:
--theme.taglist_bg_focus = "#ff0000"
theme.taglist_fg_empty = "#444444"
theme.titlebar_bg_normal = "#282828"
theme.titlebar_bg_focus = "#365135"

-- Display the taglist squares
theme.taglist_squares_sel   = "~/.config/awesome/themes/DS/taglist/squarefw.png"
theme.taglist_squares_unsel = "~/.config/awesome/themes/DS/taglist/squarew.png"

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = "~/.config/awesome/themes/DS/submenu.png"
theme.menu_height = 25
theme.menu_width  = 200
theme.menu_border_color = "#535d6c"
theme.menu_border_width = 1

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal = "~/.config/awesome/themes/DS/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = "~/.config/awesome/themes/DS/titlebar/close_focus.png"

theme.titlebar_ontop_button_normal_inactive = "~/.config/awesome/themes/DS/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = "~/.config/awesome/themes/DS/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = "~/.config/awesome/themes/DS/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = "~/.config/awesome/themes/DS/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = "~/.config/awesome/themes/DS/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = "~/.config/awesome/themes/DS/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = "~/.config/awesome/themes/DS/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = "~/.config/awesome/themes/DS/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = "~/.config/awesome/themes/DS/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = "~/.config/awesome/themes/DS/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = "~/.config/awesome/themes/DS/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = "~/.config/awesome/themes/DS/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = "~/.config/awesome/themes/DS/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = "~/.config/awesome/themes/DS/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = "~/.config/awesome/themes/DS/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = "~/.config/awesome/themes/DS/titlebar/maximized_focus_active.png"

-- theme.wallpaper = "~/.config/awesome/themes/DS/wallpaper-vertical.jpg"
-- theme.wallpaper = "~/.config/awesome/themes/DS/wallpaper-horisontal.jpg"
theme.dir = "~/.config/awesome/themes/DS/"
-- All screens. Uncomment for set of horisontal displays
theme.wallpaper = "wallpaper-horisontal.jpg" 
-- Just did screen 1. Uncomment only for one horisontal display
-- theme.wallpaper = { "wallpaper-horisontal.jpg" }
-- Did both screens. Uncomment for dual display set
-- theme.wallpaper = { "wallpaper-vertical.jpg", "wallpaper-horisontal.jpg" }

-- You can use your own layout icons like this:
theme.layout_fairh = "~/.config/awesome/themes/DS/layouts/fairhw.png"
theme.layout_fairv = "~/.config/awesome/themes/DS/layouts/fairvw.png"
theme.layout_floating  = "~/.config/awesome/themes/DS/layouts/floatingw.png"
theme.layout_magnifier = "~/.config/awesome/themes/DS/layouts/magnifierw.png"
theme.layout_max = "~/.config/awesome/themes/DS/layouts/maxw.png"
theme.layout_fullscreen = "~/.config/awesome/themes/DS/layouts/fullscreenw.png"
theme.layout_tilebottom = "~/.config/awesome/themes/DS/layouts/tilebottomw.png"
theme.layout_tileleft   = "~/.config/awesome/themes/DS/layouts/tileleftw.png"
theme.layout_tile = "~/.config/awesome/themes/DS/layouts/tilew.png"
theme.layout_tiletop = "~/.config/awesome/themes/DS/layouts/tiletopw.png"
theme.layout_spiral  = "~/.config/awesome/themes/DS/layouts/spiralw.png"
theme.layout_dwindle = "~/.config/awesome/themes/DS/layouts/dwindlew.png"
--theme.layout_cornernw = themes_path.."default/layouts/cornernww.png"
--theme.layout_cornerne = themes_path.."default/layouts/cornernew.png"
--theme.layout_cornersw = themes_path.."default/layouts/cornersww.png"
--theme.layout_cornerse = themes_path.."default/layouts/cornersew.png"

-- Generate Awesome icon:
--theme.awesome_icon = theme_assets.awesome_icon(
--    theme.menu_height, theme.bg_focus, theme.fg_focus
--)
theme.awesome_icon = "/usr/share/awesome/icons/awesome16.png"

-- Define the icon theme for application icons. If not set then the icons 
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

return theme
-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
