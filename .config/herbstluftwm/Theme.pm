package Theme;

use v5.34;
use utf8;
use warnings;
use diagnostics;    # One should better use `perl -Wdiagnostics' for debugging.
use strict;
use Helper;
use JSON::XS;
# use Cpanel::JSON::XS;   # May be absent on your system by default, unfortunately.


*HC = \&Helper::HC;
*DoConfig = \&Helper::DoConfig;

my $SCRIPT_NAME = $0 =~ s/^\/*.*\///r;
my $SCRIPT_DIR  = $0 =~ s/\/${SCRIPT_NAME}$//r;

# Background for gruvbox-dark: "Hard '#1d2021', medium '#282828', soft '#32302f'.",
my $COLOR_SCHEME_FILE_NAME = "gruvboxdark.json";
open my $colorSchemeFileFH, '<', "${SCRIPT_DIR}/${COLOR_SCHEME_FILE_NAME}";
my $colorSchemeRef = do { local $/ = undef; decode_json ( <$colorSchemeFileFH> ); };
close $colorSchemeFileFH;


my %attributes = (
    "theme.tiling.reset"            => "1",
    "theme.tab_color"               => "$colorSchemeRef->{'black'}",
    "theme.tab_title_color"         => "$colorSchemeRef->{'green'}",
    "theme.active.tab_color"        => "$colorSchemeRef->{'black'}",
    "theme.active.tab_outer_color"  => "$colorSchemeRef->{'black'}",
    "theme.active.tab_title_color"  => "$colorSchemeRef->{'green'}",
    "theme.floating.reset"          => "1",
    "theme.floating.outer_width"    => "5",
    "theme.floating.border_width"   => "5",
    "theme.floating.title_when"     => "always",
    "theme.floating.outer_color"    => "$colorSchemeRef->{'black'}",
    "theme.background_color"        => "$colorSchemeRef->{'darkgrey'}",
    "theme.active.inner_color"      => "$colorSchemeRef->{'darkyellow'}",
    "theme.active.outer_color"      => "$colorSchemeRef->{'darkgrey'}",
    "theme.active.color"            => "$colorSchemeRef->{'darkyellow'}",
    "theme.normal.color"            => "$colorSchemeRef->{'darkgrey'}",
    "theme.urgent.color"            => "$colorSchemeRef->{'red'}",
    "theme.urgent.outer_color"      => "$colorSchemeRef->{'red'}",
    "theme.urgent.inner_color"      => "$colorSchemeRef->{'black'}",
    "theme.outer_color"             => "$colorSchemeRef->{'black'}",
    "theme.inner_color"             => "$colorSchemeRef->{'black'}",
    "theme.inner_width"             => "5",
    "theme.border_width"            => "5",
    "theme.padding_top"             => "3"  # Line above windows.
);


my %settings = (
    "frame_border_width"        => "2",
    "frame_border_active_color" => "$colorSchemeRef->{'darkyellow'}",
    "frame_border_normal_color" => "$colorSchemeRef->{'darkgrey'}",
    "frame_bg_active_color"     => "$colorSchemeRef->{'darkyellow'}",
    "frame_bg_normal_color"     => "$colorSchemeRef->{'darkgrey'}",
    "frame_bg_transparent"      => "true",
    "frame_transparent_width"   => "0",     # Outer frame.
    "frame_gap"                 => "0",
    "frame_padding"             => "0",
    "padding_top"               => "30",    # Top bar height.
    "window_gap"                => "5",
    "always_show_frame"         => "true",
    "gapless_grid"              => "true",
    "snap_distance"             => "100",
    "snap_gap"                  => "100",
    "mouse_recenter_gap"        => "100",
    "update_dragged_clients"    => "0",
    "focus_follows_mouse"       => "1",
    "smart_window_surroundings" => "off",
    "smart_frame_surroundings"  => "on",
    "default_frame_layout"      => "10",    # FIX: This option does not works!

    #"tree_style"                => "╾│ ├└╼─┐"
    "tree_style"                => "╾│ ├╰╼─╮"
    #"tree_style"                => "*| +`>-."
    #"tree_style"                => "*| +`-->"

);


sub SetTheme {
    # system("xsetroot -solid '$colorSchemeRef->{'background'}'");
    say("xsetroot -solid '$colorSchemeRef->{'background'}'");

    # BUG:  Not all characters are rendered without issues in the title of the
    #       window. Some IPA characters are not, i.e. first one in word
    #       ``Ꙁападьнороусьскъ''.
    HC("and , attr theme.title_font 'Ubuntu Mono:style=Regular:size=8' "
            . ", attr theme.title_color '$colorSchemeRef->{'black'}' "
            . ", attr theme.title_when multiple_tabs "
            . ", attr theme.title_align center "
            . ", attr theme.title_height 15 "
            . ", attr theme.title_depth 5");


    DoConfig ( "attr",  \%attributes );
    DoConfig ( "set",   \%settings );
}


1;
