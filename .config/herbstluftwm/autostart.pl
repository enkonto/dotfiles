#!/usr/bin/env perl


# TODO: Add script to manage monitors.  Moving and resizing should be in
#       continuous mode.


use v5.34;
use utf8;
use warnings;
use diagnostics;    # One should better use `perl -Wdiagnostics' for debugging.
use strict;
# use autodie;
# use experimental qw(signatures); #use feature 'signatures';
# no warnings "experimental::signatures";
# use Sys::Hostname;

use lib ".";
use Helper;
# use KeyBindings;
# use Tags;
# use Theme;
# use Rules;
use Monitors;

# *HC = \&Helper::HC;


# Setting defaults.
my $SCRIPT_NAME = $0 =~ s/^\/*.*\///r;
my $SCRIPT_DIR  = $0 =~ s/\/${SCRIPT_NAME}$//r;
my $DEBUG;
my $VERBOSE;



# HC("list_monitors");
# KeyBindings->SetBindings;
# Tags->SetTags;
# Theme->SetTheme;
# Rules->SetRules;
Monitors->SetMonitors;

# Helper->HC("list_monitors");
# HC("emit_hook reload");


# HC("keyunbind --all");
# HC("mouseunbind --all");
# HC("unrule -F");
# HC("lock");

# Monitors::SetMonitors

# HC("unlock"); # unlock, just to be sure



# Wallpaper should be reloaded every time `herbstluftwm' reloads.
# `init_environment.sh -w`

# `polybar_launch.sh`
