package KeyBindings;

# NOTE: `spawn' command can't process pipes. Use `dash -c' since it cost
#       nothing. Also leave shell variables inside shell.

use v5.34;
use utf8;
use warnings;
use diagnostics;    # One should better use `perl -Wdiagnostics' for debugging.
use strict;
use Helper;


*HC         = \&Helper::HC;
*DoConfig   = \&Helper::DoConfig;

our $SUPER          = "Mod4";
my $RESIZE_STEP     = 0.02;
my $MY_PASSKEEPER   = 'keepmenu'


my %mouseBindings = (
    "${SUPER}-Button1"  => "move",
    "${SUPER}-Button2"  => "zoom",
    "${SUPER}-Button3"  => "resize"
);


my %keyBindings = (
    "${SUPER}-Control-Escape"   => "reload",
    "${SUPER}-Alt-Escape"       => "quit",
    "${SUPER}-Escape"           => "spawn dmenu_system.sh",
    "${SUPER}-Return"           => "spawn dash -c '\${MY_TERM:-urxvt}'",
    "${SUPER}-Alt-Return"       => "spawn dash -c '\${TABTERM:-urxvt}'",
    "${SUPER}-e"                => "spawn dash -c '\${MY_EMACS:-emacs}'",
    "${SUPER}-p"                => "spawn dash -c '\${MY_LAUNCHER:-dmenu}'",
    "${SUPER}-Alt-p"            => "spawn dash -c '\$${MY_PASSKEEPER}'",
    "${SUPER}-grave"            => "spawn ${SCRIPT_DIR}/scripts/scrtchpd.sh '' '\${MY_TERM:-urxvt} -n '",
    "${SUPER}-Control-s"        => "spawn xset s activate",
    # "${SUPER}-Control-s"        => "spawn xseclock.sh",

    # Basic movement in tiling and floating mode focusing clients.
    "${SUPER}-h" => "focus left",
    "${SUPER}-j" => "focus down",
    "${SUPER}-k" => "focus up",
    "${SUPER}-l" => "focus right",

    # Moving clients.  Can be done only in tiling or floating mode.
    # TODO: Implement in continuous mode: mod+control+space:hjkl
    "${SUPER}-Control-Alt-h" => "shift left",
    "${SUPER}-Control-Alt-j" => "shift down",
    "${SUPER}-Control-Alt-k" => "shift up",
    "${SUPER}-Control-Alt-l" => "shift right",

    # Resizing frames and floating clients.
    # TODO: Implement in continuous mode: mod+alt+space:hjkl
    "${SUPER}-Alt-h" => "resize left  +${RESIZE_STEP}",
    "${SUPER}-Alt-j" => "resize down  +${RESIZE_STEP}",
    "${SUPER}-Alt-k" => "resize up    +${RESIZE_STEP}",
    "${SUPER}-Alt-l" => "resize right +${RESIZE_STEP}",

    # Splitting frames or creation of an empty frame at the specified direction.
    "${SUPER}-Control-h" => "split left   0.5",
    "${SUPER}-Control-j" => "split bottom 0.5",
    "${SUPER}-Control-k" => "split top    0.5",
    "${SUPER}-Control-l" => "split right  0.5",
    # Let the current frame explode into subframes in binary tree pattern.
    "${SUPER}-Control-x" => "split explode",

    # Close client window.
    "${SUPER}-BackSpace"        => "close",
    # Remove frame but not a client window.
    "${SUPER}-Alt-BackSpace"    => "remove",

    # Manage layouts.
    "${SUPER}-Control-g" => "set_layout grid",
    "${SUPER}-Control-z" => "set_layout horizontal",
    "${SUPER}-Control-v" => "set_layout vertical",
    "${SUPER}-Control-m" => "set_layout max",
    "${SUPER}-Control-a" => "floating toggle",

    # Manage client placing.
    "${SUPER}-Alt-t" => "pseudotile toggle",
    "${SUPER}-Alt-a" => "set_attr   clients.focus.floating toggle",
    "${SUPER}-Alt-m" => "fullscreen toggle",

    # Cycle through tags.
    "${SUPER}-Tab"                  => "use_previous",
    "${SUPER}-Backslash"            => "use_previous",
    "${SUPER}-Control-Tab"          => "use_index +1 --skip-visible",
    "${SUPER}-Control-Backslash"    => "use_index -1 --skip-visible",
    # TODO: Change with ``switch to previous window''.
    "${SUPER}-Alt-Tab"              => "spawn rofi -show window",

    # Focus.
    "${SUPER}-u"            => "jumpto urgent",
    "${SUPER}-f"            => "cycle +1",
    "${SUPER}-b"            => "cycle -1",
    "${SUPER}-Alt-f"        => "cycle_frame +1",
    "${SUPER}-Alt-b"        => "cycle_frame -1",
    "${SUPER}-Control-f"    => "cycle_monitor +1",
    "${SUPER}-Control-b"    => "cycle_monitor -1",

    # Rotations of frames.
    "${SUPER}-r"            => "chain . lock . rotate . unlock",
    "${SUPER}-Alt-r"        => "chain . lock . rotate . rotate . unlock",
    "${SUPER}-Control-r"    => "chain . lock . rotate . rotate . rotate . unlock",

    # Make screenshots. Use with mod key to prevent occasional pressings on laptop.
    "Release-${SUPER}-Print"        => "spawn scrot -q 100 -e 'mv \$f ~/shots/'",
    "Release-${SUPER}-Alt-Print"    => "spawn scrot -s -q 100 -e 'mv \$f ~/shots/'",

    # Manage media.
    "XF86AudioPlay"               => "spawn deadbeef --play-pause",
    "XF86AudioStop"               => "spawn deadbeef --stop",
    "XF86AudioPrev"               => "spawn deadbeef --prev",
    "XF86AudioNext"               => "spawn deadbeef --next",
    "XF86AudioMute"               => "spawn amixer set Master toggle",
    "XF86AudioLowerVolume"        => "spawn amixer set Master 5%-",
    "XF86AudioRaiseVolume"        => "spawn amixer set Master 5%+",
    "Alt-XF86AudioMute"           => "spawn amixer set Capture toggle",
    "Alt-XF86AudioLowerVolume"    => "spawn amixer set Capture 5%-",
    "Alt-XF86AudioRaiseVolume"    => "spawn amixer set Capture 5%+",
    "XF86MonBrightnessDown"       => "spawn xbacklight -dec 10",
    "XF86MonBrightnessUp"         => "spawn xbacklight -inc 10",
    "${SUPER}-F7"                 => "spawn touchpad_switch.sh",
    # TODO: Add script to manage all possible players with common key bindings, like
    #       T. Wissman did, but may be just specify commands for just every possible
    #       player in the system.
    "${SUPER}-x"        => "spawn deadbeef --stop",
    "${SUPER}-c"        => "spawn deadbeef --play-pause",
    "${SUPER}-d"        => "spawn deadbeef --prev",
    "${SUPER}-v"        => "spawn deadbeef --next",
    "${SUPER}-apostrophe"   => "spawn amixer -q set Master toggle",
    "${SUPER}-plus"     => "spawn amixer -q set Master playback 2dB+",
    "${SUPER}-minus"    => "spawn amixer -q set Master playback 2dB-"
    # "${SUPER}-apostrophe"   => "spawn amixer -q -c 0 -- sset Master toggle",
    # "${SUPER}-plus"     => "spawn amixer -q -c 0 -- sset Master playback 2dB+",
    # "${SUPER}-minus"    => "spawn amixer -q -c 0 -- sset Master playback 2dB-",
);


sub SetBindings {

    DoConfig ( "mousebind", \%mouseBindings );
    DoConfig ( "keybind",   \%keyBindings );


    # The following cycles through the available layouts within a frame, but skips
    # layouts, if the layout change wouldn't affect the actual window positions.
    # I.e. if there are two windows within a frame, the grid layout is skipped.
    HC("keybind ${SUPER}-Control-c "
        . "or , and . compare tags.focus.curframe_wcount = 2 "
        . ". cycle_layout +1 vertical horizontal max vertical grid "
        . ", cycle_layout +1");
}


1;
