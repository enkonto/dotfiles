package Rules;

use v5.34;
use utf8;
use warnings;
use diagnostics;    # One should better use `perl -Wdiagnostics' for debugging.
use strict;
use Helper;


*HC         = \&Helper::HC;
*DoConfig   = \&Helper::DoConfig;


my %rules = (
    # Rules. Get window properties with `xprop'.
    "focus=on"              => "", # Normally focus new clients.
    "floatplacement=smart"  => "",

    # Specify floating windows.
    "class=myFloatWindow"   => "floating=on floatplacement=center",
    "class=dmenu"           => "floating=on floatplacement=none",
    "class=Pavucontrol"     => "floating=on floatplacement=center floating_geometry=1600x900",
    "class=Ulauncher"       => "floating=on focus=on",
    "class=Pqiv"            => "floating=on",
    "class=conky"           => "floating=on",
    "class=Steam"           => "floating=on",
    "class=mpv"             => "floating=on",
    "class=Lxappareance"    => "floating=on",
    "class=qt5ct"           => "floating=on",
    "class=kruler"          => "floating=on",
    "class=zoom"            => "floating=on",
    "class=Connman-gtk"     => "floating=on",
    "class=Emacs"           => "floating=on title=capture",

    # In case one have opened terminal in `tabbed'.
    "instance~'(.*[Rr]xvt.*|.*[Tt]erm|[Kk]onsole|st-256color|Alacritty|kitty)'" => "tag=",

    "class~'(.*[Rr]xvt.*|.*[Tt]erm|[Kk]onsole|st-256color|Alacritty|kitty)'"    => "tag=",
    "class~'([Ff]irefox|firefox-default|[Qq]utebrowser|[Ll]ibre[Ww]olf)'"       => "tag=",
    "class=Zathura"             => "tag=",
    "class=TelegramDesktop"     => "tag=",
    "class~'(.sxiv)'"           => "tag= floating=off  floatplacement=center",
    "class~'(MPlayer|[Mm]pv)'"  => "tag= floating=on   floatplacement=center",
    "class=Deadbeef"            => "tag=",
    "class~'([Ee]macs|jetbrains-pycharm-ce|[Vs][Ss][Cc]od.*)'" => "tag=",


    # Rules for Zoom windows.
    "class=zoom" => "title~'Chat.*'                 tag=   floating=off    index=1",
    "class=zoom" => "title~'Participants.*'         tag=   floating=off    index=1",
    "class=zoom" => "title='Zoom Meeting'           tag=   floating=off    index=0",
    "class=zoom" => "title='Zoom'                   tag=   floating=off    index=0",
    "class=zoom" => "title='Zoom - Free Account'    tag=   floating=off",
    "class=zoom" => "title='Zoom Cloud Meetings'    tag=   floating=off",

    "windowtype~'_NET_WM_WINDOW_TYPE_(DIALOG|UTILITY|SPLASH)'"      => "floating=on",
    "windowtype='_NET_WM_WINDOW_TYPE_DIALOG'"                       => "focus=on fullscreen=on",
    "windowtype~'_NET_WM_WINDOW_TYPE_(NOTIFICATION|DOCK|DESKTOP)'"  => "manage=off"
);


sub SetRules {
    DoConfig ( "rule", \%rules );
}


1;
