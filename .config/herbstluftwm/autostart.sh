#!/usr/bin/env ksh

# NOTE: `spawn' command can't process pipes. Use `sh -c' since it cost nothing.


# TODO: Convert this to perl script.  Add script to manage monitors.  Moving and
#       resizing should be in continuous mode.


scriptName="${0##*/}"   # autostart
scriptDir="${0%/*}"     # ~/.config/herbstluftwm/


_loadGoods() {
    . my_functions
    . my_aliases
}


. "${scriptDir}/gruvboxdark.colors"


# Truncate `herbstclient'.
_hc() {
    herbstclient "$@"
}


xsetroot -solid "${background}"
_hc emit_hook reload

# remove all existing keybindings
_hc keyunbind --all

# Keybindings.
SUPER='Mod4'

_hc keybind ${SUPER}-Control-Escape     reload
_hc keybind ${SUPER}-Alt-Escape         quit
_hc keybind ${SUPER}-Escape             spawn dmenu_system.sh
_hc keybind ${SUPER}-Return             spawn dash -c "$MY_TERM"
_hc keybind ${SUPER}-Alt-Return         spawn dash -c "$TABTERM"
_hc keybind ${SUPER}-e                  spawn dash -c "$MY_EMACS"
_hc keybind ${SUPER}-p                  spawn dash -c "$MY_LAUNCHER"
_hc keybind ${SUPER}-Alt-p              spawn dash -c "$MY_PASSKEEPER"
#_hc keybind ${SUPER}-grave spawn ${scriptDir}/bin/scratchpad.sh
#_hc keybind ${SUPER}-grave spawn ${scriptDir}/bin/q3terminal.sh ""
_hc keybind ${SUPER}-grave spawn ~/.config/herbstluftwm/scrtchpd.sh "" "st -n "
_hc keybind ${SUPER}-Control-s          spawn xset s activate
# _hc keybind ${SUPER}-Control-s          spawn xseclock.sh

# Basic movement in tiling and floating mode focusing clients.
_hc keybind ${SUPER}-h focus left
_hc keybind ${SUPER}-j focus down
_hc keybind ${SUPER}-k focus up
_hc keybind ${SUPER}-l focus right

# Moving clients.  Can be done only in tiling or floating mode.
# TODO: Implement in continuous mode: mod+control+space:hjkl
_hc keybind ${SUPER}-Control-h shift left
_hc keybind ${SUPER}-Control-j shift down
_hc keybind ${SUPER}-Control-k shift up
_hc keybind ${SUPER}-Control-l shift right

# Resizing frames and floating clients.
# TODO: Implement in continuous mode: mod+alt+space:hjkl
resizeStep=0.02
_hc keybind ${SUPER}-Alt-h resize left  +${resizeStep}
_hc keybind ${SUPER}-Alt-j resize down  +${resizeStep}
_hc keybind ${SUPER}-Alt-k resize up    +${resizeStep}
_hc keybind ${SUPER}-Alt-l resize right +${resizeStep}

# Splitting frames or creation of an empty frame at the specified direction.
_hc keybind ${SUPER}-Control-Alt-h  split   left    0.5
_hc keybind ${SUPER}-Control-Alt-j  split   bottom  0.5
_hc keybind ${SUPER}-Control-Alt-k  split   top     0.5
_hc keybind ${SUPER}-Control-Alt-l  split   right   0.5
# Let the current frame split into subframes in binary tree pattern.
_hc keybind ${SUPER}-Control-x      split   explode

# Close client window.
_hc keybind ${SUPER}-BackSpace      close
# Remove frame but not a client window.
_hc keybind ${SUPER}-Alt-BackSpace  remove


# Tags.
# BUG:  Some characters from ``Awesome font'' are substituted by `herbstclient'
#       as japanese logograths.
#       Works:                   
#       Don't:                 

# Remove empty tags.
_hc foreach T tags.by-name \
   sprintf ATTR_client_count '%c.client_count' T \
   and , compare ATTR_client_count = 0 \
       , sprintf ATTR_NAME '%c.name' T \
         substitute NAME ATTR_NAME merge_tag NAME

# Tags bindings for Coder Colemak layout.
_numList() {
    cat <<- EOF
		:asciicircum
		:at
		:apostrophe
		:dollar
		:equal
		:exclam
		:ampersand
		:asterisk
		:parenleft
		:parenright
		EOF
}

# Switch tag or move client to the specified tag with the numerical keys.
IFS=":"; tagIndex=0; _numList | while read -r tag tagKey; do
    _hc add "${tag}"
    _hc set_attr tags.by-name."${tag}".index "${tagIndex}"
    # Repeat current tag's key to switch back to the previous one.
    _hc keybind "${SUPER}-${tagKey}" or . and \
        , compare tags.focus.index = "${tagIndex}" \
        , try use_previous . use_index "${tagIndex}"
    _hc keybind "${SUPER}-Control-${tagKey}" move_index "${tagIndex}"
    tagIndex=$((tagIndex+1))
done; IFS=$' \t\n'

# Switch client in a frame with the numerical keys.
# TODO: Number titles of tabs and windows.
IFS=":"; tabIndex=0; _numList | while read -r ignoreTag tabKey; do
    _hc keybind "${SUPER}-Alt-${tabKey}" focus_nth "${tabIndex}"
    tabIndex=$((tabIndex+1))
done; IFS=$' \t\n'

# Leave this block right after tag creation.
if [ "$(_hc attr tags.focus.name)" == "default" ]; then
    _hc use_index 0
    _hc merge_tag default
fi


# Manage tag layout.
_hc keybind ${SUPER}-Control-g  set_layout grid
_hc keybind ${SUPER}-Control-z  set_layout horizontal
_hc keybind ${SUPER}-Control-v  set_layout vertical
_hc keybind ${SUPER}-Control-m  set_layout max
_hc keybind ${SUPER}-Control-a  floating   toggle
# The following cycles through the available layouts within a frame, but skips
# layouts, if the layout change wouldn't affect the actual window positions.
# I.e. if there are two windows within a frame, the grid layout is skipped.
_hc keybind ${SUPER}-Control-c \
    or , and . compare tags.focus.curframe_wcount = 2 \
    . cycle_layout +1 vertical horizontal max vertical grid \
    , cycle_layout +1

# Manage client placing.
_hc keybind ${SUPER}-Alt-t      pseudotile toggle
_hc keybind ${SUPER}-Alt-a      set_attr   clients.focus.floating toggle
_hc keybind ${SUPER}-Alt-m      fullscreen toggle

# Cycle through tags.
_hc keybind ${SUPER}-Tab                use_previous
_hc keybind ${SUPER}-Backslash          use_previous
_hc keybind ${SUPER}-Control-Tab        use_index +1 --skip-visible
_hc keybind ${SUPER}-Control-Backslash  use_index -1 --skip-visible
# TODO: Change with ``switch to previous window''.
_hc keybind ${SUPER}-Alt-Tab    spawn rofi -show window

# Focus.
_hc keybind ${SUPER}-u          jumpto urgent
_hc keybind ${SUPER}-f          cycle +1
_hc keybind ${SUPER}-b          cycle -1
_hc keybind ${SUPER}-Alt-f      cycle_frame +1
_hc keybind ${SUPER}-Alt-b      cycle_frame -1
_hc keybind ${SUPER}-Control-f  cycle_monitor +1
_hc keybind ${SUPER}-Control-b  cycle_monitor -1

# Rotations of frames.
_hc keybind ${SUPER}-r          chain . lock . rotate . unlock
_hc keybind ${SUPER}-Alt-r      chain . lock . rotate . rotate . unlock
_hc keybind ${SUPER}-Control-r  chain . lock . rotate . rotate . rotate . unlock

# Make screenshots. Use with mod key to prevent occasional pressings on laptop.
_hc keybind Release-${SUPER}-Print      spawn scrot -q 100 -e 'mv $f ~/shots/'
_hc keybind Release-${SUPER}-Alt-Print  spawn scrot -s -q 100 -e 'mv $f ~/shots/'

# Manage media.
_hc keybind XF86AudioPlay               spawn deadbeef --play-pause
_hc keybind XF86AudioStop               spawn deadbeef --stop
_hc keybind XF86AudioPrev               spawn deadbeef --prev
_hc keybind XF86AudioNext               spawn deadbeef --next
_hc keybind XF86AudioMute               spawn amixer set Master toggle
_hc keybind XF86AudioLowerVolume        spawn amixer set Master 5%-
_hc keybind XF86AudioRaiseVolume        spawn amixer set Master 5%+
_hc keybind Alt-XF86AudioMute           spawn amixer set Capture toggle
_hc keybind Alt-XF86AudioLowerVolume    spawn amixer set Capture 5%-
_hc keybind Alt-XF86AudioRaiseVolume    spawn amixer set Capture 5%+
_hc keybind XF86MonBrightnessDown       spawn xbacklight -dec 10
_hc keybind XF86MonBrightnessUp         spawn xbacklight -inc 10
_hc keybind ${SUPER}-F7 spawn touchpad_switch.sh
# TODO: Add script for the smart managing all possible players with common key
#       bindings, like T. Wissman did.
_hc keybind ${SUPER}-x          spawn deadbeef --prev
_hc keybind ${SUPER}-c          spawn deadbeef --stop
_hc keybind ${SUPER}-d          spawn deadbeef --play-pause
_hc keybind ${SUPER}-v          spawn deadbeef --next
#_hc keybind ${SUPER}-apostrophe spawn amixer -q -c 0 -- sset Master toggle
#_hc keybind ${SUPER}-plus       spawn amixer -q -c 0 -- sset Master playback 2dB+
#_hc keybind ${SUPER}-minus      spawn amixer -q -c 0 -- sset Master playback 2dB-
_hc keybind ${SUPER}-apostrophe spawn amixer -q set Master toggle
_hc keybind ${SUPER}-plus       spawn amixer -q set Master playback 2dB+
_hc keybind ${SUPER}-minus      spawn amixer -q set Master playback 2dB-

# Mouse.
_hc mouseunbind --all
_hc mousebind ${SUPER}-Button1 move
_hc mousebind ${SUPER}-Button2 zoom
_hc mousebind ${SUPER}-Button3 resize

# Theme.
#_hc lock
_hc attr theme.tiling.reset 1
_hc attr theme.tab_color "${black}"
_hc attr theme.tab_title_color "${green}"
_hc attr theme.active.tab_color "${black}"
_hc attr theme.active.tab_outer_color "${black}"
_hc attr theme.active.tab_title_color "${green}"
# BUG:  Not all characters are rendered without issues in the title of the
#       window. Some IPA characters are not, i.e. first one in word
#       ``Ꙁападьнороусьскъ'' in spite of using font family with IPA support.
_hc and , attr theme.title_font "Ubuntu Mono:style=Regular:size=9" \
        , attr theme.title_color "${black}" \
        , attr theme.title_when multiple_tabs \
        , attr theme.title_align center \
        , attr theme.title_height 16 \
        , attr theme.title_depth 5
_hc attr theme.floating.reset 1
_hc attr theme.floating.outer_width 5
_hc attr theme.floating.border_width 5
_hc attr theme.floating.title_when always
_hc attr theme.floating.outer_color "${black}"
_hc attr theme.background_color   "${darkgrey}"
_hc attr theme.active.inner_color "${darkyellow}"
_hc attr theme.active.outer_color "${darkgrey}"
_hc attr theme.active.color "${darkyellow}"
_hc attr theme.normal.color "${darkgrey}"
_hc attr theme.urgent.color "${red}"
_hc attr theme.urgent.outer_color "${red}"
_hc attr theme.urgent.inner_color "${black}"
_hc attr theme.outer_color "${black}"
_hc attr theme.inner_color "${black}"
_hc attr theme.inner_width 5
_hc attr theme.border_width 5
_hc attr theme.padding_top 3 # Line above windows.
_hc set frame_border_width 2
_hc set frame_border_active_color "${darkyellow}"
_hc set frame_border_normal_color "${darkgrey}"
_hc set frame_bg_active_color "${darkyellow}"
_hc set frame_bg_normal_color "${darkgrey}"
_hc set frame_bg_transparent true
_hc set frame_transparent_width 0 # Outer frame.
_hc set frame_gap 0
_hc set frame_padding 0
_hc set padding_top 30 # Top bar height.
_hc set window_gap 5
_hc set always_show_frame true
_hc set gapless_grid true
_hc unlock # unlock, just to be sure


_hc set snap_distance      100
_hc set snap_gap           100
_hc set mouse_recenter_gap 100
_hc set update_dragged_clients 0
_hc set focus_follows_mouse 1
_hc set smart_window_surroundings off
_hc set smart_frame_surroundings on
_hc set default_frame_layout 10 # FIX: This option does not works!


# Rules. Get window properties with `xprop'.
_hc unrule -F
_hc rule focus=on # Normally focus new clients.
_hc rule floatplacement=smart

# Specify floating windows.
_hc rule class=dmenu        floating=on     floatplacement=none
_hc rule class=Pavucontrol  floating=on     floatplacement=center   floating_geometry=1600x900
_hc rule class=Ulauncher    floating=on     focus=on
_hc rule class=Pqiv         floating=on
_hc rule class=conky        floating=on
_hc rule class=Steam        floating=on
_hc rule class=mpv          floating=on
_hc rule class=Lxappareance floating=on
_hc rule class=qt5ct        floating=on
_hc rule class=kruler       floating=on
_hc rule class=zoom         floating=on
_hc rule class=Connman-gtk  floating=on
_hc rule class=Emacs title=capture floating=on

_hc rule class~'(.*[Rr]xvt.*|.*[Tt]erm|[Kk]onsole|st-256color|Alacritty|kitty)' tag=
# In case one have opened terminal in `tabbed'.
_hc rule instance~'(.*[Rr]xvt.*|.*[Tt]erm|[Kk]onsole|st-256color|Alacritty|kitty)' tag=
_hc rule class~'([Ff]irefox|firefox-default|[Qq]utebrowser|[Ll]ibre[Ww]olf)' tag=
_hc rule class=Zathura            tag=
_hc rule class=TelegramDesktop    tag=
_hc rule class~'(.sxiv)'          tag= floating=off    floatplacement=center
_hc rule class~'(MPlayer|[Mm]pv)' tag= floating=on     floatplacement=center
_hc rule class=Deadbeef           tag=
_hc rule class~'([Ee]macs|jetbrains-pycharm-ce|[Vs][Ss][Cc]od.*)' tag=
_hc rule class=myFloatWindow floating=on floatplacement=center


# Rules for Zoom windows.
_hc rule class=zoom title~"Chat.*" tag= floating=off index=1
_hc rule class=zoom title~"Participants.*" tag= floating=off index=1
_hc rule class=zoom title="Zoom Meeting" tag= floating=off index=0
_hc rule class=zoom title="Zoom" tag= floating=off index=0
_hc rule class=zoom title="Zoom - Free Account" tag= floating=off
_hc rule class=zoom title="Zoom Cloud Meetings" tag= floating=off

#hc rule focus=off # normally do not focus new clients
_hc rule windowtype~'_NET_WM_WINDOW_TYPE_(DIALOG|UTILITY|SPLASH)' floating=on
_hc rule windowtype='_NET_WM_WINDOW_TYPE_DIALOG' focus=on
_hc rule windowtype~'_NET_WM_WINDOW_TYPE_(NOTIFICATION|DOCK|DESKTOP)' manage=off

#_hc set tree_style '╾│ ├└╼─┐'
_hc set tree_style '╾│ ├╰╼─╮'
#_hc set tree_style '*| +`>-.'
#_hc set tree_style '*| +`-->'


# Do multi monitor setup here, e.g.:
#_hc set_monitors 1280x1024+0+0 1280x1024+1280+0
# or simply:
#_hc detect_monitors


# NOTE: There are two variants to add new monitors:
#       * Apply different layouts, calculating monitors' dimensions to untie
#       form different screens' resolutions and sizes.
#       * Split vertically or horizontally focused monitor or group of monitors,
#       like as `bspwm' does.
#       Deletion function will be too complicated for both of them.

# _hc set_monitors 1920x1080+0+0  # Use to initiate monitors sized over full screen.
# _hc rename_monitor 0 main       # Rename all of the initiated screens.
_hc set_monitors 1920x2160+0+0 1920x2160+1920+0
_hc rename_monitor 0 left
_hc rename_monitor 1 right
# _hc set_monitors 1920x1080+0+0 1920x2160+1920+0 1920x1080+0+1080
# _hc rename_monitor 0 main
# _hc rename_monitor 1 second
# _hc rename_monitor 2 third

# Wallpaper should be reloaded every time `herbstluftwm' reloads.
init_environment.sh -w

# Find the panel.
#panel="${scriptDir}/bin/panel.sh"
#[ -x "$panel" ] || panel=/etc/xdg/herbstluftwm/panel.sh
#for monitor in $(_hc list_monitors | cut -d: -f1) ; do
#    # Start it on each monitor.
#    "$panel" "$monitor" &
#done
polybar_launch.sh
