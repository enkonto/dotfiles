package Tags;

use v5.34;
use utf8;
use warnings;
use diagnostics;    # One should better use `perl -Wdiagnostics' for debugging.
use strict;
use autodie;
use experimental qw(signatures); #use feature 'signatures';
no warnings "experimental::signatures";
use Helper;
use Keybindings;


*HC         = \&Helper::HC;
*DoConfig   = \&Helper::DoConfig;
my $SUPER   = "$Keybindings::SUPER";


# BUG:  Some characters from ``Awesome font'' are substituted by `herbstclient'
#       as japanese logograths.
#       Works:                   
#       Don't:                 

# Tags bindings for Coder Colemak layout.
my @tagList = (
    # tag, key
    { "" => "asciicircum" },
    { "" => "at" },
    { "" => "numbersign" },
    { "" => "dollar" },
    { "" => "equal" },
    { "" => "exclam" },
    { "" => "ampersand" },
    { "" => "asterisk" },
    { "" => "parenleft" },
    { "" => "parenright" },
);


sub SetTags {
    # Remove empty tags.
    HC("foreach T tags.by-name "
        . "sprintf ATTR_client_count '%c.client_count' T "
        . "and , compare ATTR_client_count = 0 "
            . ", sprintf ATTR_NAME '%c.name' T "
              . "substitute NAME ATTR_NAME merge_tag NAME");

    # Set some key bindings for tags.
    # for my $tagIndex ( 0..$#tagList ) {
    #     my ( $tag, $tagKey ) = @{$tagList[$tagIndex]};
    while ( my ( $tagIndex, $tagBinding ) = each @tagList ) {
        my ( $tag, $tagKey ) = %{$tagBinding};

        # Switch tag.
        HC("add '${tag}'");
        HC("set_attr tags.by-name.'${tag}'.index '${tagIndex}'");
        # Repeat current tag's key to switch back to the previous one.
        HC("keybind '${SUPER}-${tagKey}' or . and "
            . ", compare tags.focus.index = '${tagIndex}' "
            . ", try use_previous . use_index '${tagIndex}'");

        # Move client to the specified tag with the numerical keys.
        HC("keybind '${SUPER}-Control-${tagKey}' move_index '${tagIndex}'");

        # Switch client in a frame with the numerical keys.
        # TODO: Number titles of tabs and windows.
        HC("keybind '${SUPER}-Alt-${tagKey}' focus_nth '${tagIndex}'");
    }

    # Leave this block right after tag creation.
    if ( HC("attr tags.focus.name") eq "default" ) {
        HC("use_index 0");
        HC("merge_tag default");
    }
}


1;
