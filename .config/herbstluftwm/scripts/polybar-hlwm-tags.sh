#!/usr/bin/env ksh


_loadGoods() {
    . my_functions
    . my_aliases
}


scriptName=$(basename $0)
scriptDir=$(dirname $0) # ~/.config/herbstluftwm/bin
    . "${scriptDir%scripts}gruvboxdark.colors"


herbstclient --idle "tag_*" 2>/dev/null \
    | { while true; do {
        #for tag in $(herbstclient tag_status "${display}"); do
        for tag in $(herbstclient tag_status); do
            name=${tag#?}
            state=${tag%$name}
            link="%{A1:herbstclient use $name:} $name %{A}"
            case "${state}" in
                '.') printf "%%{F$darkgrey}%s%%{F-}" "${link}" ;;
                ':') printf "%%{F$blue}%s%%{F-}" "${link}" ;;
                '+') printf "%%{B$darkgrey}%%{F$black}%s%%{F-}%%{B-}" "${link}" ;;
                '#') printf "%%{B$blue}%%{F$black}%s%%{F-}%%{B-}" "${link}" ;;
                '-') printf "%%{F$cyan}%s%%{F-}" "${link}" ;;
                '%') printf "%%{B$cyan}%%{F$black}%s%%{F-}%%{B-}" "${link}" ;;
                '!') printf "%%{B$red}%%{F$black}%s%%{F-}%%{B-}" "${link}" ;;
                '*') printf "%%{B$white}%%{F$black} %s %%{F-}%%{B-}" "${link}" ;;
            esac
        done
    }; printf "\n"

    read -r || break    # Wait for next event from `herbstclient --idle'
done
} 2>/dev/null
