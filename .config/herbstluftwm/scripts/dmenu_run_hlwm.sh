#!/usr/bin/env bash

if ! command -v dmenu > /dev/null 2>/dev/null ; then
    echo "Error: Requirement dmenu not found in your PATH." >&2
    exit 1
fi

tag=$(herbstclient attr tags.focus.name)    # Get the currently active tag.

# Redirect to `dmenu_path'.
if command -v dmenu_path > /dev/null 2>/dev/null ; then
        selectedPath=$(dmenu_path | dmenu "$@")

# Use the code from latest `dmenu_path' directly.
elif command -v stest > /dev/null 2>/dev/null ; then
    cacheDir=${XDG_CACHE_HOME:-"$HOME/.cache"}
    if [ -d "${cacheDir}" ]; then
        cache="${cacheDir}/dmenu_run"
    else
        cache="${HOME}/.dmenu_cache"
    fi
    IFS=:
    if stest -dqr -n "${cache}" ${PATH}; then
        selectedPath=$(stest -flx ${PATH} \
                        | sort -u \
                        | tee "${cache}" \
                        | dmenu "$@")
    else
        selectedPath=$(dmenu "$@" < "${cache}")
    fi

# Both not found -> unable to continue
else
    echo "Error: Requirements dmenu_path or stest not found in your PATH." >&2
    exit 2
fi

# Stop here if the user aborted
[ -z ${selectedPath} ] && exit 0

# Move next window from this process to this tag. Prepend the rule so
# that it may be overwritten by existing custom rules e.g. in the
# autostart. Also set a maximum age for this rule of 120 seconds and
# mark it as one-time-only rule.
herbstclient rule prepend maxage="120" pid="$$" tag="${tag}" once

exec ${selectedPath}
