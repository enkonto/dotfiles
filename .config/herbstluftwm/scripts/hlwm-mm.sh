#!/usr/bin/env python3

"""
    geom_regex='[[:digit:]]\+x[[:digit:]]\++[[:digit:]]\++[[:digit:]]\+'
    geom=$(xrandr --query | grep "^$MONITOR" | grep -o "$geom_regex")
    monitor=$(herbstclient list_monitors | grep "$geom" | cut -d: -f1)

    * Goals: split screens placed horizontally or vertically into two zones
      named monitors with the ratios 1:1, 1:2, 2:1. There can be only one or two
      monitors on one screen.  Swap monitors' ratio on the current screen.
      Monitor is the variable space on the screen able to display a tag within
      itself.  Initial monitor is the monitor have been set while WM loading.
      Additional monitor is the monitor have been added after WM loading.
    * Initiation. Check if there are any monitors on the screens not in list of
      monitors, return error, ask to delete them.  For every screen set a
      monitor stretched on whole each screen.
    * Split space on the current screen in ratio 1:1, 2:1, 1:2 but only if there
      is one monitor: resize an initial monitor and place an additional one,
      both sized accordingly specified ratio and fitted in the screen.
    * Merge two monitors on the current screen: remove an additional monitor and
      resize initial monitor to full screen.

_hc set_monitors 1100x2160+0+0 1640x2160+1100+0 1100x2160+2740+0 # 2:3:2
_hc set_monitors 1920x2160+0+0 1920x2160+1920+0 # 1:1
_hc set_monitors 1280x2160+0+0 2560x2160+1280+0 # 1:2
_hc set_monitors 2560x2160+0+0 1280x2160+2560+0 # 2:1
_hc set_monitors 3840x2160+0+0
_hc set_monitors 1920x1080+0+0  # Use to initiate monitors sized over full screen.
_hc rename_monitor 0 main       # Rename all of the initiated screens.
_hc add_monitor 1920x1080+0+0  main # Resize existing ones and new ones.
_hc set_monitors 960x1080+0+0 960x1080+960+0
"""
