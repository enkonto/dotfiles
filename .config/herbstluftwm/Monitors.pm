package Monitors;

use v5.34;
use utf8;
use warnings;
use diagnostics;    # One should better use `perl -Wdiagnostics' for debugging.
use strict;
use autodie;
use experimental qw(signatures); #use feature 'signatures';
no warnings "experimental::signatures";
use Helper;


*HC         = \&Helper::HC;
*DoConfig   = \&Helper::DoConfig;


# Do multi monitor setup here, e.g.:
# HC("set_monitors 1280x1024+0+0 1280x1024+1280+0");
# or simply:
# HC("detect_monitors");


sub SetMonitors {
    # TODO: For display size less then 24" should be avaliable only one monitor. For
    #       the bigger ones there are some options:
    #       - for FHD one monitor only;
    #       - for QHD one monitor in FHD mode or two-three monitors splitted in
    #       tiles in QHD mode.
    #       ls /sys/class/drm/*/edid | xargs -i{} sh -c "echo {}; parse-edid < {}"

    # HC("set_monitors 1920x1080+0+0");
    # HC("rename_monitor 0 main");

    # HC("set_monitors 1920x2160+0+0 1920x2160+1920+0");
    # HC("rename_monitor 0 main");
    # HC("rename_monitor 1 second");

    HC("set_monitors 1920x1080+0+0 1920x2160+1920+0 1920x1080+0+1080");
    HC("rename_monitor 0 main");
    HC("rename_monitor 1 second");
    HC("rename_monitor 2 third");


}


1;
