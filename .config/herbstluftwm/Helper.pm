package Helper;


use v5.34;
use utf8;
use warnings;
use diagnostics;    # One should better use `perl -Wdiagnostics' for debugging.
use strict;
# use autodie;


# Truncate `herbstclient'.
sub HC {
    # system("herbstclient @_");
    # `herbstclient @_`;
    say("herbstclient @_");
}

sub DoConfig {
    my ( $command, $hashRef ) = @_;
    while ( my ( $key, $value ) = each %$hashRef ) {
        # HC("$command $key $value");
        say("$command $key $value");
    }
}

1;
