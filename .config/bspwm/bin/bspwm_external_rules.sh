#!/usr/bin/env dash

# External rules for BSPWM.


# Manual tiling
# =============

# Spawn node on the newest receptacle or preselection and switch focus to it.
# For preselections priority is given to the current desktop.
# Receptacles will not switch focus to the current desktop, whereas preselection
# will.  This way we can develop different workflows (e.g. create 3 recepticals
# in one desktop, launch 3 GUIs that take time to load, switch to another
# desktop and continue working, until you decide to go back to the GUIs).  This
# has no effect when all actions occur within the focused desktop.
#bspc_query() {
#    bspc query -N -n "$@"
#}
#
#recept="$(bspc_query 'any.leaf.!window')"
#presel="$(bspc_query 'newest.!automatic')"
#
#if [ -n "$recept" ]; then
#    target="$recept"
#    attention="off"
#elif [ -n "$presel" ]; then
#    target="$presel"
#fi
#
#printf "node=${target:-focused}\n"
#printf "follow=${attention:-on}\n"


# Window rules
# ============

# Operate on windows based on their properties.  The positional arguments are
# defined in the ``external_rules_command'' of `man bspc'.
window_id="$1"
window_class="$2"
window_instance="$3"
window_consequences="$4"
window_title="$(xwininfo -id "${window_id}" | sed ' /^xwininfo/!d ; s,.*"\(.*\)".*,\1,')"


case "${window_class}" in
    my_float_window)
        printf "state=floating\n" ;;
    *[Rr]xvt*|*[Tt]erm|[Kk]onsole|st|Alacritty|kitty)
        printf "desktop=1^n\n" ;;
    [Ff]irefox|qutebrowser)
        printf "desktop=^2\n" ;;
    *[Zz]athura)
        printf "desktop=^3 state=tiled\n" ;;
    *[Ss]xiv)
        printf "state=pseudo_tiled center=on desktop=6\n" ;;
    [Mm]pv|[Vv]lc|[Mm][Pp]layer)
        printf "state=floating center=on desktop=7\n" ;;
    Deadbeaf)
        printf "state=tiled desktop=8\n" ;;
    *)
        case "$(xprop -id "$window_id" _NET_WM_WINDOW_TYPE)" in
            *_NET_WM_WINDOW_TYPE_DIALOG*)
                printf "state=floating\n" ;;
            *)
                printf "state=tiled\n" ;;
        esac
        ;;
esac


case "${window_instance}" in
    my_float_window)
        printf "state=floating\n" ;;
esac


# NOTE: The "file operations" applies to the `caja` file manager.
# FIX:  There should be a better way of handling this.
case "${window_title}" in
    'File Operations'*)
        printf "center=on state=floating\n" ;;
esac
