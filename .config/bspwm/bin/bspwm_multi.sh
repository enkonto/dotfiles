#!/bin/bash

# Facilitates BSPWM operations over multiple selection paths.

_query_nodes() {
	for i in $(bspc query -N -n "$1"); do
		bspc node "$i" "${@:2}"
	done
}

case "$1" in
	# Kill the first receptacle.
	kr)
		bspc node 'any.leaf.!window.local' -k
		;;
	# Kill all receptacles.
	kar)
		_query_nodes '.leaf.!window' -k
		;;
	# Close all local non focused.
	calnf)
		_query_nodes '.!focused.!marked.local.window' -c
		;;
	# Kill all local non focused.
	kalnf)
		_query_nodes '.!focused.!marked.local.window' -k
		;;
	# Lock all local.
	lal)
		_query_nodes '.local.window' -g locked=on
		;;
	# Unlock all local.
	ulal)
		_query_nodes '.local.window' -g locked=off
		;;
	# Summon all marked.
	sam)
		_query_nodes '.marked' -d focused --follow
		;;
	# Bring desktop here.
	bdh)
		for i in $(bspc query -N -d "$2" -n '.window'); do
			bspc node "$i" -d 'focused.focused'
		done
		;;
	# Send desktop there.
	sdh)
		bspwm_dynamic_desktops --send-all-to-desktop "$2"
		;;
	*)
		echo "Wrong argument!"
		exit 1
		;;
esac
