#!/bin/bash

# A more fluid way of moving windows with BSPWM, which is meant to be
# implemented in SXHKD.  If there is a window in the given direction,
# swap places with it.  Else if there is a receptacle move to it
# ("consume" its place).  Otherwise create a receptacle in the given
# direction by splitting the entire viewport (circumvents the tiling
# scheme while respecting the current split ratio configuration).  In
# the latter scenario, inputting the direction twice will thus move the
# focused window out of its current layout and into the receptacle.

# Check how much arguments is given.
[ "$#" -eq 2 ] || { echo \
	"You can pass only two arguments: north,east,south,west and move step"; \
	exit 1; }

# Check if the 1sh argument is a valid direction.
case "$1" in
	north|east|south|west)
		direction="$1"
		;;
	*)
		echo "Not a valid argument."
		echo "Use one of: north,east,south,west"
		exit 1
		;;
esac

# Trancate query expression
_query_nodes() {
	bspc query -N -n "$@"
}

# Trancate moving expression
_move_floating() {
	bspc node -v "$@"
}

# Set move step for floating windows
move_step="$2"

# Do not operate on floating windows.
if [ -z "$(_query_nodes focused.floating)" ]; then
	# Query node without window.
	receptacle="$(_query_nodes 'any.leaf.!window')"
	# Check if there is a non floating node in the given direction. If it is 
	# then swap windows. 
	if [ -n "$(_query_nodes "${direction}.!floating")" ]; then
		bspc node -s "$direction"
	# Check if there is a receptacle. If it is then move focused node to the 
	# receptacle.
	elif [ -n "$receptacle" ]; then
		bspc node focused -n "$receptacle" --follow
	else
		# Create a receptacle in the given direction if there is
		# no nodes or neither receptacles.
		bspc node @/ -p "$direction" -i && bspc node -n "$receptacle" --follow
	fi
# Operate on floating windows.
else
	case ${direction} in
		west)
			_move_floating -${move_step} 0
			;;
		south)
			_move_floating 0 ${move_step}
			;;
		north)
			_move_floating 0 -${move_step}
			;;
		east)
			_move_floating ${move_step} 0
			;;
		*)
			echo "Wrong argument!"
			exit 1
			;;
	esac
fi
