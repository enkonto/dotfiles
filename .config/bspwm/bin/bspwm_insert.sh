#!/bin/bash

# Trancate query expression
bspc_query() {
	bspc query -N -n "$@"
}

# Return state of nodes wether it marked or focused
# Morked nodes are in favor over focused.
marked_or_focused() {
	if [ -n "$(bspc_query 'any.marked')" ]; then
		echo 'any.marked'
	else
		echo 'focused'
	fi
}

# Spawn receptacle in a gived direction in the parent node.
create_receptacle() {
	bspc node @parent/ -p "$1" -i
}

# Get receptacle ID
receptacle="$(bspc_query 'any.leaf.!window')"

case $1 in
	# Insert into a preselection.
	presel)
		bspc node "$(marked_or_focused)" -n 'newest.!automatic' --follow
		;;
	# Insert into a receptacle.
	recept)
		bspc node "$(marked_or_focused)" -n "$receptacle" --follow
		;;
	west)
		create_receptacle $1
		;;
	south)
		create_receptacle $1
		;;
	north)
		create_receptacle $1
		;;
	east)
		create_receptacle $1
		;;
	*)
		echo "Wrong argument!"
		exit 1
		;;
esac
