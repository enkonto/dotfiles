#!/usr/bin/env dash

# Dependency: rofi OR dmenu.


_loadGoods() {
    . my_functions
    . my_aliases
}


_listActions() {
    printf "%s\n" \
        "Sxhkd reload" \
        "Polybar launch/restart" \
        "Kill polybar" \
        "Xresources reload" \
        "Bspwm quit" \
        | _dynamic_menu 6 1 "Bspwm options"
}


_loadGoods
choice="$(_listActions)"   # Get array of actions.

# Run the selected command. Case should be the first letter of the action in
# the action list.
case "${choice}" in
    S*) _checkDep sxhkd && pkill -USR1 -x sxhkd && notify-send -i notify_icon \
            "Key bindings have been reloaded." ;;
    # Provide a symlink in `.config/bspwm/bin' for `polybar' script.
    P*) _checkDep polybar_launcher.sh && polybar_launcher.sh ;;
    K*) _checkDep polybar_launcher.sh && polybar_launcher.sh -k ;;
    X*) xrdb -I "$HOME" -load "$HOME"/.Xresources ;;
    B*) bspc quit ;;
esac
