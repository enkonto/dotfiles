#!/bin/bash

# Resizes, expands or fallbacks the selected node in the given direction.
# TODO: Create warning if size is not an integer number.

size=$3
direction=$2
mode=$1

# Trancate expressions.
r_edge() {
    bspc node -z right "$@"
}
l_edge() {
    bspc node -z left "$@"
}
b_edge() {
    bspc node -z bottom "$@"
}
t_edge() {
    bspc node -z top "$@"
}
w_args() {
    echo "Wrong arguments!"
    exit 1
}

case "$mode" in
    standard)
        case "$direction" in
            left)  
                r_edge -$size 0 
                l_edge +$size 0 
                ;;
            right)  
                r_edge +$size 0 
                l_edge -$size 0 
                ;;
            down) 
                b_edge 0 -$size 
                t_edge 0 +$size
                ;;
            up) 
                b_edge 0 +$size 
                t_edge 0 -$size
                ;;
            *)
                w_args
                ;;
        esac
        ;;
    expand)
        case "$direction" in
            left)  
                l_edge -$size 0 
                ;;
            right)  
                r_edge +$size 0 
                ;;
            down) 
                b_edge 0 +$size 
                ;;
            up) 
                t_edge 0 -$size
                ;;
            *)
                w_args
                ;;
        esac
        ;;
    fallback)
        case "$direction" in
            left)  
                l_edge +$size 0 
                ;;
            right)  
                r_edge -$size 0 
                ;;
            down) 
                b_edge 0 -$size 
                ;;
            up) 
                t_edge 0 +$size
                ;;
            *)
                w_args
                ;;
        esac
        ;;
    *)
        w_args
        ;;
esac
