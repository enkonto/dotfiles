

set all&    " Clear out hellish busted distro/local defaults
let &t_ut=''


if has("gui_running")
    :set guifont=Cascadia_Mono:h16:cANSI:qDRAFT
endif


" Truecolor works just out of the box for `neovim'. But, unfortunately,
" ``termguicolors'' turn off syntax highlighting in ``urxvt''.
if has('termguicolors')
    " 2 lines for `vim'.
    if !has('nvim')
        let &t_8f="\<Esc>[38;2;%lu;%lu;%lum"
        let &t_8b="\<Esc>[48;2;%lu;%lu;%lum"
    endif
    " For both `vim' and `neovim'.
    set termguicolors
else
    set notermguicolors
    let g:rehash256=1
endif


" Apply better cursor shapes for `vim' in terminal emulators in MacOS, for VTE
" compatible terminals (urxvt, st, xterm, gnome-terminal 3.x, Konsole KDE5 and
" others) and wsltty.  Be aware of that for other programs these lines are not
" legit. `neovim' got saner defaults in that way.
"  1 -> blinking block
"  2 -> solid block
"  3 -> blinking underscore
"  4 -> solid underscore
"  5 -> blinking vertical bar
"  6 -> solid vertical bar
if !has('nvim')
    " Cursor shapes
    let &t_SI.="\e[5 q" "SI = INSERT mode
    let &t_EI.="\e[1 q" "EI = NORMAL mode (ELSE)
    if exists("&t_SR")
        let &t_SR.="\e[4 q" "SR = REPLACE mode
    endif
    " The number of color to use
    set t_Co=256    " Support 256 colors.
endif


syntax enable               " Enables syntax highlighting.
"colorscheme sorbet
"colorscheme wildcharm
colorscheme retrobox
"set viminfo=                " Fresh start every time.
"set nocompatible            " Be iMproved, required for Vundle.vim
"filetype off                " Same
set background=dark         " Tells Nvim what the "inherited" (terminal/GUI)
                            " background looks like. Useful for themes that
                            " support dark and bright colors like 'gruvbox'.
set hidden                  " Keep multiple buffers, don't unload when it is
                            " abandoned.
set wrap                    " Don't display long lines as just one line.
set showbreak=§\ \ \        " String to use at the start of wrapped lines.
set encoding=utf-8          " The encoding displayed.
set pumheight=10            " Makes pop up menu smaller.
set fileencoding=utf-8      " The encoding written to file.
set ruler                   " Show the cursor position all the time.
set cmdheight=2             " More space for displaying messages.
set iskeyword+=-            " Treat dash separated words as a word text object.
set mouse=a                 " Enable your mouse.
set splitbelow              " Horizontal splits will automatically be below.
set splitright              " Vertical splits will automatically be to the right.
set conceallevel=0          " So that I can see `` in markdown files.
set laststatus=2            " Always display the status line.
set number                  " Print current line number in front of each line.
set numberwidth=1           " Minimum number column margin.
set relativenumber          " Count lines relativity to current line.
set cursorline              " Enable highlighting of the current line.
set showtabline=2           " Always show tabs.
set noshowmode              " Don't show current mode message like
                            " ``-- INSERT --'' on status line anymore.
set autoread                " Automatically read file when changed outside of Vim.
set nobackup                " Don't keep backup file after overwriting a file.
set nowritebackup           " Don't make a backup before overwriting a file.
set noundofile              " Don't save undo information in a file.
set noswapfile              " Don't use a swap file for a buffer.
set updatetime=300          " Faster completion. If this time (ms) nothing is
                            " typed the swap file will be written to disk.
                            " Triggered in Normal mode only. It is not triggered
                            " when waiting for a command argument to be typed,
                            " or a movement after an operator, or while
                            " recording.
set timeoutlen=750          " Time out time for mapped sequence to complete (ms).
set formatoptions-=tcrow]j  " Stop newline continuation of comments.
set clipboard+=unnamedplus  " Copy paste between `vim' and everything else
                            " through ``+'' register. `vim' have to be compiled
                            " with ``+clipboard'' flag, ``nvim'' need to have
                            " access to a provider (RTFM) for clipboard support,
                            " i.e. `xclip' or `xsel' for X11.
set autochdir               " Your working directory will always be the same as
                            " your working directory.
set tabstop=4               " Insert 4 spaces for a <Tab>.
set shiftwidth=4            " Change the number of space characters inserted for
                            " indentation.
set smarttab                " Makes tabbing smarter. Automatically define you
                            " have 2 or 4 spaces.
set expandtab               " Converts tabs to spaces.
set list                    " Show special characters.
set listchars=eol:¶,tab:┊\ ,nbsp:˽,trail:•,extends:›,precedes:‹
"set smartindent             " Makes indenting stupid. Don't use it.
set autoindent              " Take indent for new line from previous line.
set cindent                 " More cleverly indent then autoindent and smartindent.
set copyindent              " Copy the structure of the existing lines indent.
set preserveindent          " Preserve as much of the indent structure as possible.
"set softtabstop=4           " Number of spaces that <Tab> uses while editing.
"set softtabstop=0 noexpandtab " Number of spaces that <Tab> uses while editing.
set hlsearch                " Highlight matches with last search pattern.
                            " </> <CTRL-C> to abort search.
set incsearch               " Highlight match while typing search pattern.
set ignorecase              " Ignore case in search patterns.
set noerrorbells            " Ring the bell for error messages.
set textwidth=80            " Maximum width of text that is being inserted.
set colorcolumn=80          " Columns to highlight.
set spelllang=en_us         " Language(s) to do spell checking for.
"set spell                   " Enable spell checking.
set nospell                 " Do one need to turn on spell checking for better
                            " completion, if ``kspell'' been applied for
                            " completion? FYI <CTRL-X> <CTRL-S> don't work while
                            " ``spell'' is turned off.
set complete+=.,w,b,u,U,k,kspell,s,t " Specify how Insert mode completion works.

"autocmd FileType go,make,sh set noexpandtab
"autocmd FileType js,json set sw=2 ts=2

let g:mapleader = "\<Space>" " Set a leader key.
