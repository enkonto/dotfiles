" -------------------------------------------------------------
"     Keep this configuration file as simple as possible.
"      Stay with default functions as much as possible
"            and learn the basics of (Neo)Vi(M).
"              Master core keys and commands.
" -------------------------------------------------------------
"  Vim as an IDE. Project IDE rules:
"  1. Make a minimal vimrc
"  2. Fix formatting
"  3. Fix your path
"  4. Setup include-search
"  5. Setup tags
"  6. Add compiler support
"  7. Make config portable (e.g. stow)
" -------------------------------------------------------------
"  Tutorial on Customization and Configuration:
"  1. Initialization. Execute Ex commands
"  2. Load plugin scripts

source $HOME/.config/vim/settings.vim
source $HOME/.config/vim/plugins.vim
source $HOME/.config/vim/mappings.vim
source $HOME/.config/vim/functions.vim
