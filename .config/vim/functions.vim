
" Highlight the extra characters. Colors borrowed from ``SpellBad'' of the
" ``gruvbox8_hard'' color scheme.
fun! ExtraCharactersHL()
    hi ExtarCharacters cterm=italic ctermfg=203 gui=italic guifg=#fb4934 guisp=#fb4934
    let w:m1=matchadd('ExtarCharacters', '\%>80v.\+', -1)
endfun


" Disable editor environment.
fun! ExtraBuffEnv()
    setlocal nospell
    setlocal nocursorcolumn
    setlocal nocursorline
    setlocal nonumber
    setlocal noruler
    setlocal norelativenumber
    hi ExtarCharacters NONE
endfun


" Toggle environment.
fun! BufferEnterEnv()
    let w:current_buf_type = getbufvar('%', '&buftype')
    if w:current_buf_type =~ '^$'
        call ExtraCharactersHL()
    else
        call ExtraBuffEnv()
    endif
    unlet w:current_buf_type
endfun

if !has('nvim')
    autocmd BufWinEnter,TerminalWinOpen * call BufferEnterEnv()
    "autocmd BufEnter,TerminalOpen * call BufferEnterEnv()
else
    autocmd BufWinEnter,TermOpen * call BufferEnterEnv()
    "autocmd BufEnter,TermOpen * call BufferEnterEnv()
endif


" Toggle spelling check highlighting if one needed to leave ``spell'' turned on,
" but don't highlight mistakes. Turns off highlighting at the 1st run. Be aware
" of that spell check highlighting toggles globaly since highlight groups are
" global variables.
fun! SpellHighlightToggle()

    if !exists('g:spell_hl_state')
        let g:spell_hl_state = 1
    endif

    if &spell
        if g:spell_hl_state == 1
            hi SpellBad   NONE
            hi SpellCap   NONE
            hi SpellLocal NONE
            hi SpellRare  NONE
            let g:spell_hl_state = 0
        else
            "Highlight groups borrowed from the ``gruvbox8_hard'' color scheme.
            hi SpellBad   cterm=underline,italic ctermfg=203 gui=undercurl,italic guifg=#fb4934 guisp=#fb4934
            hi SpellCap   cterm=underline,italic ctermfg=109 gui=undercurl,italic guifg=#83a598 guisp=#83a598
            hi SpellRare  cterm=underline,italic ctermfg=175 gui=undercurl,italic guifg=#d3869b guisp=#d3869b
            hi SpellLocal cterm=underline,italic ctermfg=107 gui=undercurl,italic guifg=#8ec07c guisp=#8ec07c
            let g:spell_hl_state = 1
        endif
    endif

endfun

call SpellHighlightToggle()
