" From https://github.com/junegunn/vim-plug
" The default plugin directory will be as follows:
"   - Vim (Linux/macOS): '~/.vim/plugged'
"   - Vim (Windows): '~/vimfiles/plugged'
"   - Neovim (Linux/macOS/Windows): stdpath('data') . '/plugged'
" You can specify a custom plugin directory by passing it as the argument
"   - e.g. `call plug#begin('~/.vim/plugged')`
"   - Avoid using standard Vim directory names like 'plugin'
"call plug#begin('~/.vim/plugged')

" Reload it and :PlugInstall to install the plugins, :PlugUpdate to update them.
" :PlugClean will remove unused plugins, and :PlugUpgrade will update vim-plug
" itself.

" Make sure you use single quotes
"
" To load not loaded plugins call 'plug#load('name1','name2',...)


" set the runtime path to include Vundle and initialize
"set rtp+=~/.vim/bundle/Vundle.vim
"call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
"Plugin 'VundleVim/Vundle.vim'

" This is a simplified and optimized* version of Gruvbox that I have developed
" to address some issues I had with the official color scheme.
" Use with out true colors setting for saturated colors.
""""Plug 'lifepillar/vim-gruvbox8'

" A light and configurable statusline/tabline plugin for Vim.
""""Plug 'itchyny/lightline.vim'

" A Vim plugin which shows a git diff in the 'gutter' (sign column).
"Plug 'airblade/vim-gitgutter', { 'on': [] }

" I'm not going to lie to you; fugitive.vim may very well be the best Git
" wrapper of all time. Fugitive is the premier Vim plugin for Git.
""""Plug 'tpope/vim-fugitive'

" This is the development code of Nvim-R which improves Vim's support to
" edit R code.
""""Plug 'jalvesaq/Nvim-R', { 'on': [] }

" Full path fuzzy file, buffer, mru, tag, ... finder for Vim.
""""Plug 'ctrlpvim/ctrlp.vim'

" TODO: Add https://github.com/junegunn/fzf.vim

" Neomake is a plugin for Vim/Neovim to asynchronously run programs.
""""Plug 'neomake/neomake'

" A collection of language packs for Vim.
""""Plug 'sheerun/vim-polyglot'

" MUcomplete is a minimalist autocompletion plugin for Vim.
""""Plug 'lifepillar/vim-mucomplete', {'on' : []}

" Syntastic is a syntax checking plugin for Vim created by Martin Grenfell. It
" runs files through external syntax checkers and displays any resulting errors
" to the user. This can be done on demand, or automatically as files are saved.
" If syntax errors are detected, the user is notified and is happy because they
" didn't have to compile their code or execute their script to find them.
""""Plug 'vim-syntastic/syntastic'

" A simple, easy-to-use Vim alignment plugin.
""""Plug 'junegunn/vim-easy-align'

" EasyMotion provides a much simpler way to use some motions in vim.
""""Plug 'easymotion/vim-easymotion'

" This plugin provides a start screen for Vim and Neovim.
""""Plug 'mhinz/vim-startify'

" Ripgrep for Vim.
""""Plug 'jremmen/vim-ripgrep'

" The plug-in visualizes undo history and makes it easier to browse and switch
" between different undo branches.
""""Plug 'mbbill/undotree'

" Rainbow Parentheses Improved. Help you read complex code by showing different
" level of parentheses in different color.
""""Plug 'luochen1990/rainbow'


" Initialize plugin system.
"call plug#end()

" All of your Plugins must be added before the following line
"call vundle#end()            " required
"filetype plugin indent on    " required

" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just
"                   :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to
"                   auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line


"source $HOME/.config/vim/plugins/mucomplete.vim
"source $HOME/.config/vim/plugins/lightline.vim
"source $HOME/.config/vim/plugins/gruvbox.vim
"source $HOME/.config/vim/plugins/rg.vim
"source $HOME/.config/vim/plugins/ctrlp.vim
"source $HOME/.config/vim/plugins/undotree.vim
"source $HOME/.config/vim/plugins/rainbow.vim


" vim: set et fdm=marker sts=4 sw=4:
