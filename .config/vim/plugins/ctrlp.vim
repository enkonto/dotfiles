" CtrlP settings.
" Don't search for some certain stuff.
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-stardard']
let g:ctrlp_use_caching = 0

" Change the default mapping and the default command to invoke CtrlP:
let g:ctrlp_map = '<Leader><c-p>'
let g:ctrlp_cmd = 'CtrlP'
" When invoked without an explicit starting directory, CtrlP will set its local
" working directory according to this variable.
let g:ctrlp_working_path_mode = 'ra'
" If a file is already open, open it again in a new pane instead of switching to
" the existing pane.
let g:ctrlp_switch_buffer = 'et'
" Exclude files and directories using Vim's wildignore and CtrlP's own
" g:ctrlp_custom_ignore. If a custom listing command is being used, exclusions
" are ignored.
set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux
set wildignore+=*\\tmp\\*,*.swp,*.zip,*.exe  " Windows
let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/]\.(git|hg|svn)$',
  \ 'file': '\v\.(exe|so|dll)$',
  \ 'link': 'some_bad_symbolic_links',
  \ }
" Use a custom file listing command.
let g:ctrlp_user_command = 'find %s -type f'        " MacOSX/Linux
let g:ctrlp_user_command = 'dir %s /-n /b /s /a-d'  " Windows
" Ignore files in .gitignore.
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']
