" Original Gruvbox theme.
"let g:gruvbox_bold=1
"let g:gruvbox_italics=1
"let g:gruvbox_italicize_strings=1
"colorscheme gruvbox

" Gruvbox Hard.
colorscheme gruvbox8_hard

" Material Gruvbox.
"let g:gruvbox_material_better_performance = 1
"let g:gruvbox_material_background = 'hard'
"let g:gruvbox_material_foreground = 'material'
"let g:gruvbox_material_statusline_style = 'default'
"let g:gruvbox_material_enable_bold = 1
"let g:gruvbox_material_enable_italic = 1
"colorscheme gruvbox-material
