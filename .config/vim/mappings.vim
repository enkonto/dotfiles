
" Key maps to emulate the "system clipboard" shortcuts. Not nedeed for `neovim'
" inside `st'.
" Usefull only for `vim' compiled with the +clipboard feature.
"inoremap <C-v> <ESC>"+pa
"vnoremap <C-c> "+y
"vnoremap <C-d> "+d
" `xclip' must be installed.
"vmap <C-c> !xclip -i -sel -c<CR>
"imap <C-v> !xclip -o -sel -c<CR>

" Use alt + hjkl to resize windows.
nnoremap <M-j> :resize -5<CR>
nnoremap <M-k> :resize +5<CR>
nnoremap <M-h> :vertical resize -5<CR>
nnoremap <M-l> :vertical resize +5<CR>

" Move visual blocks up and down. Acts same as <J> with V-LINE.
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Alternate way to save.
"nnoremap <C-s> :w<CR>
" Alternate way to quit.
"nnoremap <C-Q> :wq!<CR>
" Use control-c instead of escape.
"nnoremap <C-c> <Esc>

" Wipe to the black hole.
nnoremap x "_x

" Add empty lines.
nmap <Leader>o o<Esc>^D
nmap <Leader>O O<Esc>^D

" Yank to the clipboard of X if vim doesn't have clipboard support enabled.
" NOTE: `wl-clip' for Wayland, `pbcopy' for MacOS.
nmap <Leader>y :'<,'>w !xclip -selection clipboard

" Preserve yanked part in the buffer after pasting in place of highlighted part.
nmap <leader>p :[["_dP]]

" Some additional leader bindings.
nmap <Leader>r :!"%:p"<CR>
nmap <Leader>t :tabnew<CR>
nmap <Leader>T :terminal<CR>
nmap <Leader>* :nohlsearch<CR>
nmap <Leader>C :call SpellHighlightToggle()<CR>
nmap <Leader>c :set spell!<CR>
nmap <Leader>u :UndotreeToggle<CR>
nmap <Leader>e :Ex<CR>
nmap <Leader>g :Rg<SPACE>
map <Leader>h <Plug>(easymotion-prefix)


" Start interactive EasyAlign in visual mode (e.g. vipga)
xnoremap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nnoremap ga <Plug>(EasyAlign)


" You can't stop me
cmap w!! w !sudo tee %
